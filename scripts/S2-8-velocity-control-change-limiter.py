class LimiteurAcceleration:
    def __init__(self, acceleration_maximale, valeur_initiale=0):
        self.valeur_precedente = valeur_initiale
        self.change_maximal = acceleration_maximale * PERIODE

    def __call__(self, valeur):
        change = (valeur - self.valeur_precedente)

        # Mettre à jour la valeur précédente (valeur à retourner)
        self.valeur_precedente = valeur

        # Limiter la valeur préceinte si l'accélération dépasse
        # la limite
        if abs(change) > self.change_maximal and \
                sign(change) == sign(value):

            self.valeur_precedente + \
                math.copysign(self.change_maximal, change)

        return self.valeur_precedente
