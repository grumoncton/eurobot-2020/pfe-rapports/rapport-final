% vim: ft=tex spelllang=fr

\subsubsection{Filtrage des faux-positifs} \label{S2-5-concept-final-filtre}

La section~\ref{S2-5-à-faire-faux-positifs} explique l'importance de filtrer les obstacles détectés entre les robot adverses et des éléments de jeu.

Le LiDAR donne des points en coordonnées polaires $(r, \phi)$ relatives au centre du robot. Il faut d'abord convertir les coordonnées polaires relatives au robot en coordonnées rectangulaires relatives au référentiel absolu de la table (ces coordonnées sont donc dites \textquote{absolues}). Il est possible de calculer les coordonnées $(x_p, y_p)$ absolues du point détecté en fonction des coordonnées $(x_r, y_r, \theta)$ du robot. Ce calcul est montré analytiquement à l'équation~\ref{eq:S2-5-filtre-rect} et graphiquement à la figure~\ref{figure:S2-5-conversion-polaire-rectangulaire}.

\begin{align}
	x_p &= x_r + r \cdot \cos(\phi + \theta) & y_p &= y_r + r \cdot \sin(\phi + \theta)
	\label{eq:S2-5-filtre-rect}
\end{align}

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.8]
		\coordinate (origine) at (0,0);
		\coordinate (robot) at (5,-3);
		\coordinate (robot-heading) at (6.5,-5);
		\coordinate (robot-below) at (5,-5);
		\coordinate (point) at (9,-4);

		\draw[->] (origine) -- (0,-8) node[black,below] {$x$};
		\draw[->] (origine) -- (15,0) node[black,right] {$y$};

		\node [draw, shape=rectangle, minimum width=1cm, minimum height=1cm, anchor=center, rotate=36.87] at (robot) {};
		\draw[->] (robot) -- (robot-heading);

		\fill (point) circle (0.05);

		\draw (robot) -- node[above] {$r$} (point);


		\draw[dotted] (robot) -- (robot -| origine) node[left] {$x_r$};
		\draw[dotted] (robot) -- (robot |- origine) node[above] {$y_r$};

		\draw[dotted] (point) -- (point -| origine) node[left] {$x_p$};
		\draw[dotted] (point) -- (point |- origine) node[above] {$y_p$};

		\pic [draw, "$\theta$", angle eccentricity=1.7] {angle = robot-below--robot--robot-heading};
		\draw[dotted] (robot) -- (robot-below);

		\pic [draw, "$\phi$", angle eccentricity=1.7] {angle = robot-heading--robot--point};
	\end{tikzpicture}
	\caption{Représentation graphique de la conversion polaire-rectangulaire dans le système de coordonnées de la table}
	\label{figure:S2-5-conversion-polaire-rectangulaire}
\end{figure}

Ayant les coordonnées absolues du point détecté, il est simple de déterminer s'il se trouve sur la table, comme le montre l'équation~\ref{eq:S2-5-filre-verification}.

\begin{align}
	\left( \SI{0}{\mm} < x_p < \SI{2000}{\mm} \right) \land \left( \SI{0}{mm} < y_p < \SI{3000}{\mm} \right) \label{eq:S2-5-filre-verification}
\end{align}

Il est possible que le robot détecte un obstacle directement sur la bordure de la table (une balise fixe ou le mât central, par exemple). En raison d'une petite erreur sur son positionnement, le robot pourrait déterminer que le point se trouve sur la table. Il serait ainsi prudent d'ajouter une petite marge d'erreur, comme le montre l'équation~\ref{eq:S2-5-filre-verification-marge}. Cette marge ne causera pas des problèmes, puisque le LiDAR détecte les supports de balises embarquées des robots adverses. Ce support doit être centré sur le robot et ne peut pas prendre une forme plus petite qu'un cercle de \SI{70}{\mm} de diamètre. Il est ainsi impossible que ce support se trouve entièrement à moins de \SI{50}{\mm} de la bordure de la table.

\begin{align}
	\epsilon &\approx \SI{50}{\mm} \\
	\epsilon &< x_p < \SI{2000}{\mm} - \epsilon \land \epsilon < y_p < \SI{3000}{\mm} - \epsilon \label{eq:S2-5-filre-verification-marge}
\end{align}

L'algorithme fut implémenté exactement tel que présenté. Il a été exécuté sur le robot de la compétition \textit{Eurobot 2019}. Ce test a rapidement montré que la mise en œuvre est trop lente. En effet, le tampon de données du LiDAR était toujours rempli, ce qui implique que les données n'étaient pas traitées assez rapidement.

Une analyse profonde de l'implémentation a révélé que l'opérateur d'addition ($+$) de l'équation~\ref{eq:S2-5-filtre-rect} ralentissait la fonction par un ordre de grandeur. Il fut impossible de trouver une explication à cela. En temps normal, les opérations multiplicatives ($\times$) et trigonométriques ($\sin$, $\cos$) sont plus coûteuses en temps et en puissance de calcul (dans une notation grand O) que les additions. Peu importe, la solution fut de soustraire la position du robot des limites au lieu de l'additionner à chaque point détecté. De cette façon, l'opération coûteuse est seulement exécutée une fois par révolution du LiDAR. L'équation~\ref{eq:S2-5-filre-verification-marge-rel} montre le nouveau calcul des limites de la table et l'équation~\ref{eq:S2-5-filtre-rect-rel} montre la conversion des coordonnées polaires en coordonnées rectangulaires.

\begin{align}
	\epsilon &\approx \SI{50}{\mm} \\
	x_{\min} &= \epsilon - x_r \\
	y_{\min} &= \epsilon - y_r \\
	x_{\max} &= x_{\min} + \SI{2000}{\mm} \\
	y_{\max} &= y_{\min} + \SI{3000}{\mm} \\
	x_{\min} &< x_p < x_{\max} \land y_{\min} < y_p < y_{\max} \label{eq:S2-5-filre-verification-marge-rel} \\
	\notag \\
	x_p &= r \cdot \cos(\phi + \theta) & y_p &= r \cdot \sin(\phi + \theta)
	\label{eq:S2-5-filtre-rect-rel}
\end{align}

Le problème avec cette approche est qu'elle introduit un troisième système de coordonnées. En plus des coordonnées absolues de la table et des coordonnées relatives du robot, il faut aussi considérer un système d'axe centré sur le robot mais parallèle aux axes de la table. En effet, $x_p$ et $y_p$ utilisent, ici, ce nouveau système d'axes. Le système de coordonnées est illustré à la figure~\ref{figure:S2-5-conversion-polaire-rectangulaire-rel}. L'utilisation d'un troisième système de coordonnées n'est pas un grand problème puisqu'il est confiné à une seule fonction.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.8]
		\coordinate (origine) at (0,0);
		\coordinate (robot) at (5,-3);
		\coordinate (robot-heading) at (6.5,-5);
		\coordinate (robot-below) at (5,-5);
		\coordinate (point) at (9,-4);

		\draw (0,-8) -- (origine) node[black,left] {$x_{\min}$};
		\draw (15,0) -- (origine) node[black,above] {$y_{\min}$};

		\node [draw, shape=rectangle, minimum width=1cm, minimum height=1cm, anchor=center, rotate=36.87] at (robot) {};
		\draw[->] (robot) -- (robot-heading);

		\fill (point) circle (0.05);

		\draw (robot) -- node[above] {$r$} (point);


		\draw[dotted] (robot) -- (robot -| origine) node[left] {$0$};
		\draw[dotted] (robot) -- (robot |- origine) node[above] {$0$};

		\draw[dotted] (point) -- (point -| origine) node[left] {$x_p$};
		\draw[dotted] (point) -- (point |- origine) node[above] {$y_p$};

		\pic [draw, "$\theta$", angle eccentricity=1.7] {angle = robot-below--robot--robot-heading};

		\draw[->] (robot) -- (robot-below) node[below] {$x$};
		\draw[->] (robot) -- ++(3,0) node[right] {$y$};

		\pic [draw, "$\phi$", angle eccentricity=1.7] {angle = robot-heading--robot--point};
	\end{tikzpicture}
	\caption{Systèmes d'axes relatif au robot utilisé pour le filtrage des données du LiDAR}
	\label{figure:S2-5-conversion-polaire-rectangulaire-rel}
\end{figure}

Le code \textit{Python} qui met en œuvre l'algorithme est présenté à la figure~\ref{fig:S2-5-filtre-faux-positifs}. L'algorithme est implémenté par une fonction génératrice (une fonction qui retourne plusieurs valeurs avec le mot clé \code{yield}). La fonction prend comme argument une liste des points détectés par le LiDAR lors d'une rotation complète. Elle \textquote{yield} tous les points qui ne sont pas des faux-positifs. Prime abord, elle établit les limites de la table ($x_{\min}$, $y_{\min}$, $x_{\max}$, et $y_{\max}$). Par la suite, elle traîte chaque point itérativement. Le point est ignoré s'il est plus proche que l'acrylique (le capteur LiDAR sera protégé par un tube translucide en acrylique, pour éviter que des particules endommagent la fenêtre optique). Par la suite, les coordonnées polaires sont converties en coordonnées rectangulaires relatives au centre du robot (le système d'axe présenté à la figure~\ref{figure:S2-5-conversion-polaire-rectangulaire-rel}). Finalement, le point est considéré s'il tombe dans les limites établies.

\begin{figure}[H]
	\pythonscript{./scripts/S2-5-filtre-faux-positifs.py}
	\mintedspace
	\caption{Fonction génératrice filtrant des faux-positifs parmi les points détectés par le LiDAR}
	\label{fig:S2-5-filtre-faux-positifs}
\end{figure}
