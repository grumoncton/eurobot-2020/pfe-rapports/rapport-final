def sans_faux_positifs(points):
    """
    Retourner que les points qui ne sont pas les faux-positifs. Les
    faux positifs sont les éléments de jeu, les balises fixes, le
    tube acrylique qui entoure le LiDAR ou n'importe quoi qui n'est
    pas sur la table.

    Cette année, il n'y a pas des éléments de jeu sur la table à la
    hauteur du LiDAR. Il suffit de vérifier que les mesures sont
    sur la table.
    """

    DISTANCE_ACRYLIC = 2
    ERREUR = 50

    min_x = ERREUR - pos.x
    max_x = min_x + TABLE_HAUTEUR
    min_y = ERREUR - pos.y
    max_y = min_y + TABLE_LARGEUR

    for point in points:
        _, phi, r = point

        # Faux-positif si le point est sur l'acrylique qui entoure
        # le LiDAR
        if r < DISTANCE_ACRYLIC:
            continue

        # Trouver angle relatif à la table
        angle_absolue = phi + pos.a

        # Trouver coordonnées du point relatives au centre du robot
        # mais dans les axes parallèles aux axes de la table
        x = r * math.cos(angle_absolue)
        y = r * math.sin(angle_absolue)

        if min_x < x < max_x and min_y < y < max_y:
            yield point
