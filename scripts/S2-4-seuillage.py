import cv2
import matplotlib.pyplot as plt
import numpy as np


def main():
    img = cv2.imread("image.png", cv2.IMREAD_COLOR)

    # Seuillage à 127
    seuil = 127
    max_val = 255

    ret, res0 = cv2.threshold(img, seuil, max_val, cv2.THRESH_BINARY)
    ret, res1 = cv2.threshold(img, seuil, max_val, cv2.THRESH_BINARY_INV)
    ret, res2 = cv2.threshold(img, seuil, max_val, cv2.THRESH_TOZERO)
    ret, res3 = cv2.threshold(img, seuil, max_val, cv2.THRESH_TOZERO_INV)
    ret, res4 = cv2.threshold(img, seuil, max_val, cv2.THRESH_TRUNC)

    titres = ["Original", "Binary", "Binary Inv", "To 0", "To 0 Inv", "Trunc"]
    images = [img, res0, res1, res2, res3, res4]

    for i in range(len(titres)):
        plt.subplot(2, 3, i + 1)
        plt.imshow(images[i])
        plt.title(titres[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()


if __name__ == "__main__":
    main()
