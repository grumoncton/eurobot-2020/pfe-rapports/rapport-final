import cv2
import numpy as np
import matplotlib.pyplot as plt


def main():
    # Récupération de l'image
    nom_fichier = "image.png"
    img = cv2.imread(nom_fichier, cv2.IMREAD_COLOR)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # TRANSFORMATION AFFINE
    rows, columns, channels = img.shape

    # POINTS D'ORIGINE
    pts_orig = np.float32(
        [[x0, y0],
         [x1, y1],
         [x2, y2]])
    pts_trsf = np.float32(
        [[xprime_0, yprime_0],
         [xprime_1, yprime_1],
         [xprime_2, yprime_2]])

    # Matrice de transformation
    A = cv2.getAffineTransform(pts_orig, pts_trsf)
    transforme = cv2.warpAffine(img, A, (columns, rows))

    titres = ['Original', 'Transformé']
    images = [img, transforme]

    for i in range(len(titres)):
        plt.subplot(1, len(titres), i + 1)
        plt.imshow(images[i])
        plt.title(titres[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()


if __name__ == "__main__":
    main()
