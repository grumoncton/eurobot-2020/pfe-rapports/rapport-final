import time
from smbus2 import SMBus

# Ouvrir un bus de communication
bus = SMBus(1)

# Définir l'adresse du périphérique et le numéro de la commande
ADRESSE_SLAVE = 0x2d
CMD = 0x27

while True:

    # Envoyer la commande au périphérique avec les numéros de
    # 0 à 30 comme argument
    bus.write_i2c_block_data(ADRESSE_SLAVE, CMD, list(range(31)))

    # Lire 32 octets du périphérique
    print(bus.read_i2c_block_data(ADRESSE_SLAVE, CMD, 32))

    time.sleep(0.1)
