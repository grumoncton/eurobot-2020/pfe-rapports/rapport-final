def main():
    dimensions = (hauteur, largeur)
    # Deux séries de figures
    # Chaque série contient `largeur` images
    # Chaque image est définie par une colonne de pixels
    # représentant la limite entre les deux moitiés
    for i in range(2 * dimensions[1]):
        # Première moitié
        if i < dimensions[1]:
            # Première série de figures
            # Le blanc à gauche
            img = creer_girouette(i, dimensions)

        # Deuxième moitité
        else:
            # Deuxième série de figures
            # Inversion : noir à gauche
            img = creer_girouette(i, dimensions, inv=True)

        # Sauvegarde de l'image générée à partir de l'indice `i`
        cv2.imwrite(f"girouette-{str(i).zfill(3)}.png", img)


def creer_girouette(idx, dimensions, inv=False):
    """Création d'une image de girouete"""

    # Image en couleur de dimensions hauteur x largeur
    # Initialisée à zéro :
    img = np.zeros((*dimensions, 3), np.uint8)

    # Couleur blanche
    white = 255 * np.ones(3, np.uint8)

    # Détermination de la colonne de pixels délimitant
    # le Nord et le Sud
    pixel = np.mod(idx, dimensions[1])
    img[:, :pixel] = white

    # Inversion des couleurs de l'image si `inv` est vrai
    return cv2.bitwise_not(img) if inv else img


if __name__ == "__main__":
    main()
