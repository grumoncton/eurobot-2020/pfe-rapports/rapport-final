% vim: ft=tex spelllang=fr

\subsubsection{Communication} \label{S2-1-comms}

La Pi est l'ordinateur centrale des robots et doit alors communiquer avec un grand nombre de périphériques. Certains périphériques utilisent des protocoles de communication différents (UART, \ItwoC, socket). Une hiérarchie a été créée afin de réutiliser au maximum le code. Chaque protocole utilise ainsi le même format de trame, les mêmes sommes de contrôle et échéances. Le système de communication de cette section ne s'applique pas lorsque le destinataire implémente déjà un système de communication (par exemple, le LiDAR, le \textit{Decawave}, etc.). Une liste de caractéristiques voulues pour le système de communication est donnée au tableau~\ref{table:S2-1-travail-caractéristiques-comms}.

\setcounter{magicrownumbers}{0}
\begin{table}[H]
	\caption{Contraintes pour le gestionnaire des tâches}
	\label{table:S2-1-travail-caractéristiques-comms}
	\begin{tabularx}{\linewidth}{ @{\makebox[3em][c]{\rownumber\space}} X }
		\toprule
		\multicolumn{1}{@{\makebox[3em][c]{\textbf{N°}}} l}{\textbf{Contrainte}} \\
		\midrule
		Le format des trames devrait être consistant entre les protocoles de communication. \\
		La requête devrait être renvoyée si le destinataire ne répond pas après un certain délai. \\
		Une retransmission devrait être demandée si la somme de contrôle n'est pas correcte. \\
		\bottomrule
	\end{tabularx}
\end{table}

\paragraph{Hiérarchie des classes}

La structure des systèmes de communication est mise en œuvre avec une hiérarchie de classes. La classe de base, \code{Comms}, est responsable de la logique abstraite de la communication, de l'ajout de sommes de contrôle et de la gestion des erreurs.  Puisque le robot recourt tant à des protocoles asynchrones (UART et socket) que des protocoles synchrones (\ItwoC), les classes \code{AsyncComms} et \code{SyncComms} correspondantes furent développées afin d'inclure des fonctionnalités spécifiques à chaque type de protocole. Par exemple, dans une communication asynchrone, il est possible d'attendre un message du destinataire. Finalement, les classes \code{UARTComms}, \code{SocketComms} et \code{I2CComms} sont définies. Ces classes sont responsables de la communication bas niveau. Ces classes font le lien avec le port du protocole (ou avec une librairie appropriée). Cette structure est donnée graphiquement à la figure~\ref{figure:S2-1-travail-communication-hierarchie-classe}.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[auto, align=center, node distance=2cm]
		\node [block] (comms) {\code[]{Comms}};

		\node [block, below of=comms, xshift=-2cm] (asynccomms) {\code[]{AsyncComms}};
		\draw [->] (comms) -- (asynccomms);

		\node [block, below of=comms, xshift=2cm] (synccomms) {\code[]{SyncComms}};
		\draw [->] (comms) -- (synccomms);

		\node [block, below of=asynccomms, xshift=-2cm] (uart) {\code[]{UARTComms}};
		\draw [->] (asynccomms) -- (uart);

		\node [block, below of=asynccomms, xshift=2cm] (socket) {\code[]{SocketComms}};
		\draw [->] (asynccomms) -- (socket);

		\node [block, below of=synccomms, xshift=2cm] (i2c) {\code[]{I2CComms}};
		\draw [->] (synccomms) -- (i2c);
	\end{tikzpicture}
	\caption{Hiérarchie des classes utilisées pour la communication}
	\label{figure:S2-1-travail-communication-hierarchie-classe}
\end{figure}

\paragraph{Format des trames}

Les trames sont composées de quatre sections: la longueur, la somme de contrôle, l'instruction et les données. La longueur spécifie la taille de la trame. Elle inclut la somme de contrôle et l'instruction, même si ces deux sont de taille fixe, parce que de cette façon elle représente exactement le nombre d'octets à lire.

La somme de contrôle est incluse afin de gérer les erreurs de communication. En effet, si un bit arrive erroné, la somme de contrôle de la trame sera différente de celle incluse dans la trame. Il serait possible de faire une requête pour une retransmission. La somme de contrôle est calculée sur l'instruction et les données, mais pas la longueur.

L'instruction spécifie l'action à prendre ou les données à récupérer. Finalement, les données sont, en quelque sorte, les paramètres de l'instruction. Par exemple, si l'instruction commande la rotation d'un servomoteur, les données définiraient l'angle cible. Le format des trames est donné au tableau~\ref{table:S2-1-travail-format-trame}.

\begin{table}[H]
	\caption{Format des trames utilisées pour la communication}
	\label{table:S2-1-travail-format-trame}
	\centering
	\begin{tabular}{cccc}
		\toprule
		Longueur & Somme de contrôle & Instruction & Données \\
		\midrule
		1 octet & 2 octets & 1 octet & Variable \\
		\bottomrule
	\end{tabular}
\end{table}

Un exemple de trame qui sert à actionner un servomoteur est donné au tableau~\ref{table:S2-1-travail-format-trame-exemple}. Puisque la somme de contrôle prend 2 octets, l'instruction prend 1 octet et les données prennent 1 octet, la longueur est de 4 octets. L'instruction \code{0x37} représente \textquote{actionner} un certain servomoteur et \code{0x40} représente \textquote{\SI{90}{\degree}}. Les octets \code{0xc6 0xd4} représentent la somme de contrôle petit-boutienne de \code{0x37 0x40}.

\begin{table}[H]
	\caption{Exemple de trame pour commander l'angle d'un servomoteur}
	\label{table:S2-1-travail-format-trame-exemple}
	\centering
	\begin{tabular}{cccc}
		\toprule
		Longueur & Somme de contrôle & Instruction & Données \\
		\midrule
		\code{0x04} & \code{0xc6 0xd4} & \code{0x37} & \code{0x40} \\
		\bottomrule
	\end{tabular}
\end{table}

\paragraph{Sommes de contrôle}

La somme de contrôle utilisée est le contrôle de redondance cyclique (CRC)~\cite[Chapitre~10]{IntroductionToDigitalCommunications}. Le CRC est répandu en télécommunications et en stockage des données. L'utilisation d'une somme de contrôle est très importante. Sinon, un bit erroné dans la transmission pourrait changer complètement le sens de la trame et changer l'action que le robot entreprend.

Il fut décidé d'utiliser une somme de contrôle sur deux octets (16 bits). Il a fallu faire un compromis entre le nombre de bits ajoutés à la trame et la probabilité qu'une erreur ne soit pas détectée. Le choix d'utiliser 16 bits permet de détecter un maximum d'erreurs.

La somme de contrôle est mise en œuvre avec une fonction \code{crc16} qui prend comme argument un \textquote{iterable} (un terme \textit{Python} qui signifie un objet qui peut être représenté en tant que séquence) et qui retourne un nombre entier dans la gamme $\left[0, 2^{16}\right[$. Le code complet pour le calcul du CRC est donné à l'annexe~\ref{appendix:S2-1-python-crc16}.

\paragraph{Procédure pour envoyer une instruction}

La classe de base (\code{Comms}) possède une seule méthode publique: \code{envoyer_instruction}. Cette méthode doit être utilisée pour effectuer des actions et pour demander des informations. Les étapes à suivre pour envoyer une instruction dans le cas optimal (sans gestion d'erreurs) sont données à l'organigramme de la figure~\ref{fig:S2-1-travail-comms-flow-optimal}.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.8, every node/.style={transform shape}, node distance=2cm]
		\node (start) [startstop] {Début};

		\node (call) [io, below of=start] {Arrivée d'une instruction à envoyer};
		\draw [arrow] (start) -- (call);

		\node (crc) [process, below of=call] {Calculer somme de contrôle};
		\draw [arrow] (call) -- (crc);

		\node (conv) [process, below of=crc] {Convertir CRC, instruction et données en octets};
		\draw [arrow] (crc) -- (conv);

		\node (send) [io, below of=conv] {Envoyer la séquence d'octets};
		\draw [arrow] (conv) -- (send);

		\node (wait) [process, below of=send] {Attendre la réponse};
		\draw [arrow] (send) -- (wait);

		\node (read) [io, below of=wait] {Lire la trame réponse};
		\draw [arrow] (wait) -- (read);

		\node (extract) [process, below of=read] {Extraire le CRC, instruction et données};
		\draw [arrow] (read) -- (extract);

		\node (return) [io, below of=extract] {Retourner l'instruction et les données};
		\draw [arrow] (extract) -- (return);

		\node (end) [startstop, below of=return] {Fin};
		\draw [arrow] (return) -- (end);

	\end{tikzpicture}

	\caption{Procédure pour l'envoi d'une instruction dans le cas optimal}
	\label{fig:S2-1-travail-comms-flow-optimal}
\end{figure}

Le système doit cependant être en mesure de gérer trois formes d'erreurs. La figure~\ref{fig:S2-1-travail-comms-flow-erreurs} montre la procédure avec la gestion des erreurs.

Le premier type d'erreur est un problème d'expiration de requête, en attente d'une réponse du destinataire. Dans ce cas, cette étape produira une exception et la trame sera renvoyée. La flèche pointillée signifie que ce n'est pas le résultat d'une décision mais la gestion d'une exception.

La deuxième forme d'erreur se produit lorsque le CRC dans la trame reçue ne correspond pas au résultat du calcul de CRC sur la trame reçue. Dans ce cas, l'instruction est remplacée par l'instruction \code{NACK} (\textquote{negative-acknowledgement}).

Le dernier type d'erreur consiste dans une instruction \code{NACK} reçue du destinataire. Dans ce cas, le périphérique demande que la trame soit renvoyée. En effet, la séquence d'octets est envoyée une deuxième fois.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.8, every node/.style={transform shape}, node distance=2cm]
		\node (start) [startstop] {Début};

		\node (call) [io, below of=start] {Arrivée d'une instruction à envoyer};
		\draw [arrow] (start) -- (call);

		\node (crc) [process, below of=call] {Calculer somme de contrôle};
		\draw [arrow] (call) -- (crc);

		\node (conv) [process, below of=crc] {Convertir CRC, instruction et données en octets};
		\draw [arrow] (crc) -- (conv);

		\node (send) [io, below of=conv] {Envoyer la séquence d'octets};
		\draw [arrow] (conv) -- (send);

		\node (wait) [process, below of=send] {Attendre la réponse};
		\draw [arrow] (send) -- (wait);

		\draw [arrow, dotted] (wait) -- node [anchor=north] {Expiration} ++(-5, 0) |- (send);

		\node (read) [io, below of=wait] {Lire la trame réponse};
		\draw [arrow] (wait) -- (read);

		\node (extract) [process, below of=read] {Extraire le CRC, instruction et données};
		\draw [arrow] (read) -- (extract);

		\node (crcmatch) [decision, below of=extract, text width=2.8cm, yshift=-1.8cm] {CRC reçu correspond au CRC calculé?};
		\draw [arrow] (extract) -- (crcmatch);

		\node (change) [process, left of=crcmatch, text width=4.2cm, node distance=6cm] {Remplacer instruction par \code{NACK}};
		\draw [arrow] (crcmatch) -- node [anchor=south] {Non} (change);
		\draw [arrow] (change) |- (crc);

		\node (nack) [decision, right of=crcmatch, text width=2.5cm, node distance=6cm] {Instruction reçue est \code{NACK}?};
		\draw [arrow] (crcmatch) -- node [anchor=south] {Oui} (nack);

		\node (return) [io, below of=nack, node distance=4cm, text width=6.6cm] {Retourner l'instruction et les données};
		\draw [arrow] (nack) -- node [anchor=west] {Non} (return);
		\draw [arrow] (nack) -- node [anchor=west] {Oui} ++(0, 3) |- (send);

		\node (end) [startstop, below of=return] {Fin};
		\draw [arrow] (return) -- (end);

	\end{tikzpicture}

	\caption{Procédure pour l'envoie d'une instruction avec la gestion d'erreur}
	\label{fig:S2-1-travail-comms-flow-erreurs}
\end{figure}

Une fois le format des trames et l'organigramme déterminés, il fut possible de mettre en œuvre la classe \code{Comms} en \textit{Python}. En raison de sa longueur, le code est donné à l'annexe~\ref{appendix:S2-1-python-comms}. La méthode publique de la classe (\code{envoyer_instruction}) fonctionne de façon identique à l'organigramme de la figure~\ref{fig:S2-1-travail-comms-flow-erreurs}.

\paragraph{Procédure pour l'attente d'une réponse}

La classe \code{AsyncComms} ajoute la méthode publique \code{attendre_instruction}. Cette méthode est très similaire à \code{envoyer_instruction}, à l'exception qu'aucune instruction n'est envoyée. Cette méthode est utile pour savoir quand une action chronophage se termine (comme par exemple, monter un système sur une vis-mère). Une instruction sera envoyée pour commencer la tâche et une confirmation sera retournée. Par la suite, la Pi attendra un message indiquant la fin de la tâche.

Dans ce cas, puisqu'il n'y a pas de trame reçue, il est impossible que le destinataire envoie \code{NACK}. De plus, puisque l'objectif est d'attendre, les expirations ne sont pas utilisées. Ainsi, seules des erreurs correspondant à des sommes de contrôle non exactes seront à gérer. Les étapes à suivre pour attendre l'arrivée d'une trame sont données à l'organigramme de la figure~\ref{fig:S2-1-travail-asynccomms-flow}. Le code \textit{Python} qui met en œuvre la classe \code{AsyncComms} est donné à l'annexe~\ref{appendix:S2-1-python-asynccomms}.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.8, every node/.style={transform shape}, node distance=2cm]
		\node (start) [startstop] {Début};

		\node (wait) [process, below of=start] {Attendre la réponse};
		\draw [arrow] (start) -- (wait);

		\node (read) [io, below of=wait] {Lire la trame réponse};
		\draw [arrow] (wait) -- (read);

		\node (extract) [process, below of=read] {Extraire le CRC, instruction et données};
		\draw [arrow] (read) -- (extract);

		\node (crcmatch) [decision, below of=extract, text width=2.8cm, yshift=-1.8cm] {CRC reçu correspond au CRC calculé?};
		\draw [arrow] (extract) -- (crcmatch);

		\node (change) [io, right of=crcmatch, text width=4.2cm, node distance=7cm] {Envoyer une trame où l'instruction est \code{NACK}};
		\draw [arrow] (crcmatch) -- node [anchor=south] {Non} (change);
		\draw [arrow] (change) |- (wait);

		\node (return) [io, below of=crcmatch, node distance=4cm] {Retourner l'instruction et les données};
		\draw [arrow] (crcmatch) -- node [anchor=east] {Oui} (return);

		\node (end) [startstop, below of=return] {Fin};
		\draw [arrow] (return) -- (end);

	\end{tikzpicture}

	\caption{Procédure pour l'attente d'une instruction du destinataire de la classe \code{AsyncComms}}
	\label{fig:S2-1-travail-asynccomms-flow}
\end{figure}
