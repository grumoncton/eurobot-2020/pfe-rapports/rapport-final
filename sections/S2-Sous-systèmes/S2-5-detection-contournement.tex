% vim: spelllang=fr

\section{Détection / Contournement} \label{S2-5-detection-contournement}

Toute collision étant proscrite par les règlements (voir Annexe~\ref{appendix:regles}), les robots doivent être pourvus d'un système de détection des adversaires qui doit être conforme aux règlements de la compétition. La liste de contraintes du tableau~\ref{table:S2-5-contraintes-regles} fut établie à partir de ces règlements (annexe~\ref{appendix:regles}).

\begin{table}[H]
	\caption{Contraintes tirées des règles de la compétition et reliées au système de détection des robots adverses (annexe~\ref{appendix:regles})}
	\label{table:S2-5-contraintes-regles}
	\centering
	\begin{tabularx}{\textwidth}{lX}
		\toprule
		\textbf{Section} & \textbf{Contrainte} \\
		\midrule
		F.4. & Les équipes sont tenues d’équiper leur(s) robot(s) d’un système de détection des robots adverses. \\
		F.5.b. & Les lasers de classe: \\
					 & \tabitem 1 et 1M sont acceptés sans restriction \\
					 & \tabitem 2 sont tolérés si le rayon laser n'est jamais projeté en dehors de l'aire de jeu \\
					 & \tabitem 2M, 3R, 3B et 4 sont formellement interdits \\
		G.6. & Au-delà des bordures de l'aire de jeu, il peut y avoir des éléments pouvant perturber la détection des couleurs ou des signaux de communications. En aucun cas, il n'est possible de demander aux personnes de s'écarter ou de bouger des éléments de décors autour de l'aire de jeu. \\
		\bottomrule
	\end{tabularx}
\end{table}

En somme, les robots doivent être en mesure de détecter les robots adverses sur leur chemin et de s'arrêter avant d'entrer en collision avec l'adversaire. Puisqu'il est interdit de bouger les éléments au-delà de l'aire de jeu, les robots doivent être en mesure de filtrer les adversaires des arbitres de la compétition. Ensuite, un système de détection à base de laser doit seulement comporter des lasers de classe 1 ou 1M.

Si le robot s'arrête face à un adversaire, il respecte déjà aux contraintes de la compétition. Cependant, si le robot adverse ne se déplace pas, le robot serait bloqué et ne pourrait pas accumuler plus de points. Afin d'éviter cette situation, les robots devraient implémenter un système de contournement capable de générer un nouveau trajet sans obstacles. Le robot pourrait alors suivre ce nouveau chemin et se sortir de la zone.


\subsection{Choix de la méthode de détection}

\subsubsection{Cahier des charges}

La performance du système de détection est très importante; il doit être en mesure de répondre aux contraintes du tableau~\ref{table:S2-5-contraintes-regles}. Afin de permettre au système d'entraînement de réagir rapidement à l'apparition d'un adversaire, il doit se réactualiser fréquemment. Sa portée doit être suffisante afin de détecter des robots de loin. En effet, plus loin seront les adversaires lors de leur détection, plus facile sera le contournement. Le positionnement des obstacles détectés doit être précis en termes de distance et d'angle. Finalement, l'espace couvert par les capteurs devrait être maximisé. Ainsi, il faut minimiser les angles morts.

Plusieurs alternatives existent pour le système de détection : certaines sont plus complexes que d'autres. Vu la durée du projet et la quantité de tâches à faire, les alternatives simples seront favorisées. Même si la simplicité est difficile à quantifier, elle sera incluse dans les analyses, vu son importance.

Finalement, puisque le budget du projet est limité, le système doit être abordable. Cependant, puisque le système de détection est important pour se conformer aux règles de la compétition, mais aussi pour contourner des obstacles et pour se sortir d'un blocage à proximité d'adversaires, le prix est moins important que la performance.

Tous ces critères furent résumés dans le cahier des charges du tableau~\ref{table:S2-5-cahier-des-charges}.

\begin{table}[H]
	\caption{Cahier des charges utilisé lors du choix de système de détection}
	\label{table:S2-5-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xcccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		\rowgroup{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{500}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{700}{\$}: \SI{75}{\percent}$ \\
			$> \SI{700}{\$}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Performance} & $\SI{50}{\percent}$ \\

		\midrule

		Fréquence d'actualisation & $\SI{10}{\percent}$ & \makecell{
			$\geq \SI{10}{\hertz}: \SI{100}{\percent}$ \\
			$\geq \SI{5}{\hertz}: \SI{75}{\percent}$ \\
			$\geq \SI{1}{\hertz}: \SI{50}{\percent}$ \\
			$< \SI{1}{\hertz}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Portée & $\SI{10}{\percent}$ & \makecell{
			$\geq \SI{3.6}{\meter}: \SI{100}{\percent}$ \\
			$\geq \SI{1}{\meter}: \SI{80}{\percent}$ \\
			$\geq \SI{200}{\centi\meter}: \SI{60}{\percent}$ \\
			$< \SI{200}{\centi\meter}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Précision sur la distance & $\SI{10}{\percent}$ & \makecell{
			$\leq \SI{1}{\milli\meter}: \SI{100}{\percent}$ \\
			$\leq \SI{5}{\milli\meter}: \SI{60}{\percent}$ \\
			$> \SI{5}{\milli\meter}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Précision angulaire & $\SI{10}{\percent}$ & \makecell{
			$\leq \SI{1}{\degree}: \SI{100}{\percent}$ \\
			$\leq \SI{10}{\degree}: \SI{75}{\percent}$ \\
			$\leq \SI{15}{\degree}: \SI{50}{\percent}$ \\
			$> \SI{15}{\degree}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Espace couvert & $\SI{10}{\percent}$ & \makecell{
			$\geq \SI{95}{\percent}: \SI{100}{\percent}$ \\
			$\geq \SI{75}{\percent}: \SI{75}{\percent}$ \\
			$\geq \SI{50}{\percent}: \SI{50}{\percent}$ \\
			$< \SI{50}{\percent}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Simplicité} & $\SI{30}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{60}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}


\subsubsection{Alternatives considérées}

Afin de maximiser la réutilisation des composantes, les seules alternatives considérées sont celles pour lesquelles le GRUM possède déjà les composantes. Les alternatives sont le capteur LiDAR, les capteurs à ultrason et les capteurs de temps de vol.

\paragraph{LiDAR}

Les capteurs LiDAR projettent un faisceau laser et évaluent le temps nécessaire à la réflexion de ce faisceau sur un obstacle. En connaissant la vitesse du faisceau, il est alors possible de déterminer la distance entre le capteur et l'obstacle.

Ce type de capteur de détection fut utilisé par le GRUM l'année passée. Plus précisément, les robots du GRUM pour la compétition \textit{Eurobot 2019} étaient munis de capteurs LiDAR RPLIDAR A2M8-R4 de \textit{Slamtec}. Un aperçu du capteur est montré à la figure~\ref{fig:S2-5-rplidar}, alors qu'un survol de ses caractéristiques est donné au tableau~\ref{fig:S2-5-rplidar-caractéristiques}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-5-rplidar.jpg}
	\caption{Capteur LiDAR RPLIDAR A2M8-R4 de \textit{Slamtec}~\cite{RPLIDAR-home}}
	\label{fig:S2-5-rplidar}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques du capteur LiDAR RPLIDAR A2M8-R4 de \textit{Slamtec}~\cite{RPLIDAR-datasheet}}
	\label{fig:S2-5-rplidar-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Classe du laser & Classe 1 \\
		Portée & $\SI{12}{\meter}$ \\
		Intervalle angulaire & $\SI{360}{\degree}$ \\
		Planéité de la surface couverte & $\pm\SI{1.5}{\degree}$ \\
		Précision sur la distance & \makecell[l]{
			$< \SI{0.5}{\milli\meter}$ (pour des distances $< \SI{1.5}{\meter}$) \\
			$< \SI{1}{\percent}$ (pour toutes autres distances) \\
		} \\
		Précision angulaire typique & $\SI{0.9}{\degree}$ \\
		Fréquence d'actualisation maximale & $\SI{15}{\hertz}$ \\
		Prix & $\SI{425}{\$}$~\cite{RPLIDAR-robotshop} \\
		\bottomrule
	\end{tabularx}
\end{table}

\paragraph{Capteurs à ultrason}

Les capteurs à ultrason émettent une impulsion sonore à basse fréquence (ultrasons) et utilisent le temps de vol entre l'émission, la réflexion et la réception de l'impulsion, pour déterminer la distance entre le capteur et l'obstacle. Le GRUM a utilisé de tels capteurs pour la détection pour les compétitions \textit{Eurobot 2017} et \textit{Eurobot 2018}. Contrairement au LiDAR, le capteur ultrason ne tourne pas sur lui même. Il faudra donc utiliser plusieurs capteurs afin de se rapprocher des $\SI{360}{\degree}$ de détection du LiDAR.

La figure~\ref{fig:S2-5-ultras-bob} montre l'ensemble de capteurs à ultrason constituant le système de détection du robot secondaire de la compétition de 2018. Ce système emploie huit capteurs afin de couvrir $\SI{360}{\degree}$.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-5-ultras-bob.jpg}
	\caption{Ensemble de huit capteurs ultrason utilisé pour la détection en 2018}
	\label{fig:S2-5-ultras-bob}
\end{figure}

Les capteurs utilisés dans ce système sont des HC-SR04 de \textit{Elec Freaks}. Un aperçu de ce capteur est présenté à la figure~\ref{fig:S2-5-ultra}, tandis qu'un survol de ses caractéristiques est donné au tableau~\ref{fig:S2-5-ultra-caractéristiques}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-5-ultra.jpg}
	\caption{Capteur HC-SR04 de \textit{Elec Freaks}~\cite{ultra-sparkfun}}
	\label{fig:S2-5-ultra}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques du capteur ultrason HC-SR04 \textit{Elec Freaks}~\cite{ultra-datasheet}}
	\label{fig:S2-5-ultra-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Portée & $\SI{80}{\centi\meter}$ \footnotemark \\
		Intervalle angulaire & $\pm\SI{15}{\degree}$ \\
		Précision sur la distance & $\SI{3}{\milli\meter}$ \\
		Fréquence de fonctionnement & $\SI{16.67}{\hertz}$ \\
		Prix & $\SI{3.95}{\$}$~\cite{ultra-sparkfun} \\
		\bottomrule
	\end{tabularx}
\end{table}

\footnotetext{La fiche de données donne une portée de $\SI{4}{\meter}$~\cite{ultra-datasheet}. Pourtant, P. Marian donne une portée effective de $\SI{80}{\centi\meter}$~\cite{ultra}. En effet, il est seulement possible de détecter des obstacles sur une portée maximale dans des conditions idéales. En pratique, les expériences du GRUM correspondent à la portée effective de P. Marian.}

Avec les données du tableau~\ref{fig:S2-5-ultra-caractéristiques}, il est possible de déterminer les caractéristiques du système à huit capteurs présenté à la figure~\ref{fig:S2-5-ultras-bob}. Vu l'intervalle angulaire du capteur, la précision angulaire du système sera également de $\pm\SI{15}{\degree}$. Puisque huit capteurs sont utilisés et l'angle couvert par chacun est de $\SI{30}{\degree}$, l'espace aura un rendement de $\SI{66.67}{\percent}$, comme le montre l'équation~\ref{eq:S2-5-ultra-espace}. Ce calcul est optimiste. Les capteurs détectent principalement au centre. Le tableau~\ref{fig:S2-5-systemeultra-caractéristiques} montre les propriétés du système au complet.

\begin{align}
	\eta &= \frac{\SI{30}{\degree} \cdot 8}{\SI{360}{\degree}} = \SI{66.67}{\percent}
	\label{eq:S2-5-ultra-espace}
\end{align}

\begin{table}[H]
	\caption{Caractéristiques du système de détection utilisant huit capteurs ultrason HC-SR04}
	\label{fig:S2-5-systemeultra-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Portée & $\SI{80}{\centi\meter}$ \\
		Planéité de la surface couverte & $\pm\SI{15}{\degree}$ \\
		Précision sur la distance & $\SI{3}{\milli\meter}$ \\
		Précision angulaire & $\pm\SI{15}{\degree}$ \\
		Espace couvert & $\SI{66.67}{\percent}$ \\
		Fréquence d'actualisation maximale & $\SI{16.67}{\hertz}$ \\
		Prix & $\SI{31.60}{\$}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

\paragraph{Capteurs de temps de vol}

Les capteurs de temps de vol sont très similaires aux capteurs à ultrason. Contrairement aux capteurs à ultrason qui envoient des impulsions sonores, les capteurs de temps de vol envoient des impulsions lumineuses (voire \guillemets{photoniques}). À la compétition \textit{Eurobot 2019}, le GRUM a utilisé des capteurs de temps de vol VL53L0X de \textit{Pololu} (basé sur le circuit intégré VL53L0X de \textit{STMicroelectronics}). Ces mêmes capteurs furent choisis comme une des solutions finales pour le positionnement à la section~\ref{S2-2-choix-tof}, notamment vu qu'ils ont été utilisés pour le positionnement (et non la détection) l'année passée. Un aperçu du capteur est donné à la figure~\ref{fig:S2-5-tof}. De plus, une liste de ses caractéristiques pertinentes pour la détection est présentée au tableau~\ref{table:S2-5-tof-caractéristiques}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-5-VL53L0X.jpg}
	\caption{Capteur temps de vol VL53L0X de \textit{Pololu}~\cite{VL53L0X}}
	\label{fig:S2-5-tof}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques pertinentes du capteur temps de vol VL53L0X en mode d'opération \textquote{standard}~\cite{VL53L0X}~\cite{VL53L0X-datasheet}}
	\label{table:S2-5-tof-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Classe du laser & Classe 1 \\
		Portée & $\SI{1.2}{\meter}$ \\
		Intervalle angulaire & $\pm\SI{12.5}{\degree}$ \\
		Précision sur la distance & $\SI{1}{\milli\meter}$ \\
		Fréquence d'actualisation maximale & $\SI{33.33}{\hertz}$ \\
		Prix & $\SI{9.95}{\$}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

Il serait possible d'utiliser ces capteurs dans une configuration similaire à celle des capteurs à ultrason de la figure~\ref{fig:S2-5-ultras-bob}. Le système résultant aura les caractéristiques du tableau~\ref{fig:S2-5-systeme-tof-caractéristiques}.

\begin{table}[H]
	\caption{Caractéristiques du système de détection utilisant huit capteurs temps de vol VL53L0X}
	\label{fig:S2-5-systeme-tof-caractéristiques}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Portée & $\SI{1.2}{\meter}$ \\
		Planéité de la surface couverte & $\pm\SI{12.5}{\degree}$ \\
		Précision sur la distance & $\SI{1}{\milli\meter}$ \\
		Précision angulaire & $\pm\SI{12.5}{\degree}$ \\
		Surface couverte & $\SI{55.56}{\percent}$ \\
		Fréquence d'actualisation maximale & $\SI{33.33}{\hertz}$ \footnotemark \\
		Prix & $\SI{79.60}{\$}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

\footnotetext{Cette valeur est basée sur la période d'échantillonnage d'un capteur. Techniquement, il serait possible de lire chacun simultanément. Cependant, en pratique, tous les capteurs seraient probablement sur le même bus \ItwoC, ce qui réduirait la fréquence d'actualisation.}

Il est possible de voir que le système à base de capteurs de temps de vol est plus précis et a une meilleure portée que le système à base de capteurs à ultrason. Il faut tout de même faire un compromis, vu que les capteurs de temps de vol couvrent moins de surface, minimisant par le fait-même les angles morts. De plus, cs capteurs coûtent deux fois plus cher qu'un système à base d'ultrasons.


\subsubsection{Étude de faisabilité}

Avant de faire la sélection de la méthode de détection avec une matrice de décision, une étude de faisabilité fut effectuée sur chacune des alternatives. Les options furent évaluées selon leur faisabilité physique, leur efficacité, leur simplicité et leur faisabilité économique. Les résultats de cette étude sont présentés au tableau~\ref{table:S2-5-detection-faisabilité}.

Le capteur LiDAR fut reconnu comme faisable sur les quatres aspects. En effet, ce capteur a été très efficace, l'année dernière, lors de son utilisation par le GRUM.

Le système à base d'HC-SR04 fut déterminé comme satisfaisant, malgré certains problèmes d'efficacité. En effet, les angles morts que présente le système sont problématiques. De plus, l'alternative a été jugée a priori complexe. Le système exige des supports pour les capteurs et il faudra fort probablement un microcontrôleur pour lire les distances des capteurs et pour les transmettre à l'ordinateur central. Malgré cela, l'alternative est acceptée.

Le système exploitant les capteurs VL53L0X, tout comme celui utilisant les HC-SR04, fut déterminé comme satisfaisant tous les aspects, malgré quelques ajustements pour l'efficacité. En effet, les angles morts de ce système sont encore plus étendus. Concernant la simplicité, le système a est a priori simple avec ajustements nécessaires. Puisque les huit capteurs sont tous sur le même bus \ItwoC, la lecture des valeurs est légèrement plus simple. Pourtant, il faudrait encore un microcontrôleur pour transmettre les valeurs au microcontrôleur central. En somme, l'alternative est acceptée.

\begin{table}[H]
	\caption{Étude de la faisabilité des trois alternatives pour le système de détection}
	\label{table:S2-5-detection-faisabilité}
	\begin{tabularx}{\textwidth}{Xllllll}
		\toprule
		\textbf{Alternative} & \textbf{Physique} & \textbf{Efficacité} & \textbf{Simplicité} & \textbf{Économique} & \textbf{Décision} \\
		\midrule
		LiDAR & Oui & Oui & Oui & Oui & Acceptée \\
		Capteurs à ultrason & Oui & Oui, mais & Non, mais & Oui & Acceptée \\
		Temps de vol & Oui & Oui, mais & Oui, mais & Oui & Acceptée \\
		\bottomrule
	\end{tabularx}
\end{table}


\subsubsection{Choix final de la méthode de détection}

La matrice de décision du tableau~\ref{table:S2-5-detection-matrice} fut utilisée afin de choisir la méthode de détection en fonction du cahier des charges du tableau~\ref{table:S2-5-cahier-des-charges} et des trois alternatives considérées. Le RPLIDAR A2M8 fut choisi comme système de détection, vu son score parfait.

\begin{table}[H]
	\caption{Matrice de décision utilisée pour choisir le système de détection}
	\label{table:S2-5-detection-matrice}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xcccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pondération} & \textbf{LiDAR} & \textbf{Ultrason} & \textbf{Temps de vol} \\

		\midrule

		\rowgroup{Prix} & $\SI{20}{\percent}$ & $\SI{100}{\percent}$ & $\SI{100}{\percent}$ & $\SI{100}{\percent}$ \\

		\midrule

		\rowgroup{Performance} & $\SI{50}{\percent}$ \\

		\midrule

		Fréquence d'actualisation & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{100}{\percent}$ & $\SI{100}{\percent}$ \\

		\midrule

		Portée & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{0}{\percent}$ & $\SI{80}{\percent}$ \\

		\midrule

		Précision sur la distance & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{60}{\percent}$ & $\SI{100}{\percent}$ \\

		\midrule

		Précision angulaire & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{50}{\percent}$ & $\SI{50}{\percent}$ \\

		\midrule

		Surface couverte & $\SI{10}{\percent}$ & $\SI{100}{\percent}$ & $\SI{50}{\percent}$ & $\SI{50}{\percent}$ \\

		\midrule

		\rowgroup{Simplicité} & $\SI{30}{\percent}$ & $\SI{100}{\percent}$ & $\SI{60}{\percent}$ & $\SI{60}{\percent}$ \\

		\toprule

		\rowgroup{Total} & - & $\SI{100}{\percent}$ & $\SI{64}{\percent}$ & $\SI{76}{\percent}$ \\

		\bottomrule
	\end{tabularx}
\end{table}


\subsection{Choix de la stratégie de contournement}

\subsubsection{Cahier des charges}

Pour ne pas être bloqué face aux adversaires, le système de contournement doit être en mesure de trouver un chemin qui permet d'atteindre la destination sans entrer en collision avec les obstacles. Puisque le système de positionnement et le système de détection ne seront pas parfaits, des facteurs de sécurité doivent être ajoutés.

Les alternatives seront évaluées selon leur temps de traitement, la génération de trajets optimaux et leur simplicité d'implémentation. Le temps de traitement est difficile à quantifier, mais est un critère important, vu la durée des matchs et la complexité potentielle de l'algorithme utilisé. La simplicité est aussi difficile à quantifier, mais elle est quand même incluse dans le cahier des charges, compte tenu des échéances du projet et la quantité de tâches à faire. Ces critères furent résumés dans le cahier des charges du tableau~\ref{table:S2-5-contournement-cahier-des-charges}.

\begin{table}[H]
	\caption{Cahier des charges utilisé lors du choix de système de contournement}
	\label{table:S2-5-contournement-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{Xcccc}
		\toprule
		\textbf{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		Temps de traitement & $\SI{30}{\percent}$ & \makecell{
			$\leq \SI{0.1}{\second}: \SI{100}{\percent}$ \\
			$\leq \SI{1}{\second}: \SI{75}{\percent}$ \\
			$\leq \SI{2}{\second}: \SI{50}{\percent}$ \\
			$> \SI{2}{\second}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Génération de trajets optimaux & $\SI{40}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Simplicité & $\SI{30}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{60}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}


\subsubsection{Alternatives considérées}

Les deux méthodes de contournement considérées sont une approche discrète et une approche continue.

\paragraph{Méthode discrète}

La méthode discrète de contournement consiste à \textit{discrétiser} le terrain de jeu, en divisant la surface continue du terrain pour former un quadrillage de points discrets. Le terrain discrétisé pourrait alors être représenté en tant que graphe (dans le sens informatique du terme) pour pouvoir y appliquer des méthodes de recherche de chemin discrètes (telles que le fameux algorithme de Dijkstra~\cite{Dijkstra1959}).

Cette approche est celle qui fut implémentée par le GRUM pour les trois dernières compétitions. En effet, l'aire de jeu fut divisée en grille de points et l'algorithme de Dijkstra fut appliqué. Un exemple de recherche de trajets sur le terrain de jeu d'\textit{Eurobot 2019} est présenté à la figure~\ref{fig:S2-5-chemin-dijkstra}, où les points en rouge montre les sommets d'un trajet défini entre l'origine à ($\SI{100}{\milli\meter}$, $\SI{100}{\milli\meter}$) et la destination à ($\SI{1750}{\milli\meter}$, $\SI{2750}{\milli\meter}$). Dans cet exemple, les points en bleus font partie du quadrillage discret, alors que les formes en noir sont les obstacles.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-5-chemin-dijkstra.png}
	\caption{Chemin entre le point ($\SI{100}{\milli\meter}$, $\SI{100}{\milli\meter}$) et le point ($\SI{1750}{\milli\meter}$, $\SI{2750}{\milli\meter}$) sur une grille discrète calculé avec l'algorithme de Dijkstra}
	\label{fig:S2-5-chemin-dijkstra}
\end{figure}

Cette approche nécessite de faire un compromis entre le temps d'exécution et la qualité du trajet. En effet, afin d'optimiser le temps, pas tous les points sont connectés. Par conséquent, le chemin résultant n'est pas optimal. Un autre moyen d'augmenter la qualité du trajet est de diminuer l'espace entre les points, ce qui risque de ralentir le temps d'exécution.

Vu les désavantages de cette approche, la recherche de chemin n'a jamais été fortement implémentée par le GRUM et n'a jamais été utilisée pour le contournement de robots adverses.

\paragraph{Méthode continue}

La méthode continue de la recherche de chemin tente de trouver un trajet entre l'origine et la destination, sans discrétiser l'aire de jeu. L'algorithme peut définir un chemin de manière itérative, jusqu'au moment où il ne contient plus d'obstacles. Des formules géométriques seraient utilisées afin de calculer les distances et les obstacles pouvant se dresser sur un chemin. Ces calculs seront légèrement plus complexes que ceux de l'algorithme de Dijkstra. Cependant, il faut significativement moins de calculs, vu que le nombre de calculs sera, au plus, proportionnel au nombre d'obstacles et non au nombre de points. En fin de compte, l'algorithme devrait trouver le chemin globalement optimal.

\subsubsection{Décision finale de la stratégie de contournement} \label{S2-5-choix-contournement}

Puisque la méthode discrète n'est pas capable de calculer des chemins globalement optimaux, il est constaté qu'elle ne répond pas au cahier des charges du tableau~\ref{table:S2-5-contournement-cahier-des-charges}. Ainsi, la méthode continue est choisie comme méthode de contournement.

\subsection{Tâches à faire}

Les capteurs et la méthode de contournement étant maintenant déterminés, les tâches restantes consistent majoritairement à faire de la programmation. Ces tâches sont détaillées dans les sous-sections suivantes.

\subsubsection{Filtrage des faux-positifs} \label{S2-5-à-faire-faux-positifs}

Le système de détection localisera des obstacles dans les $\SI{360}{\degree}$ autour de lui. Puisque ces obstacles pourraient être des robots adverses, des éléments de jeu ou des obstacles au-delà de l'aire de jeu, le système doit être en mesure de filtrer les obstacles détectés et de ne retenir que ceux qui sont véritablement sur le chemin.

Pour faire ce tri, le système devra convertir les coordonnées relatives des obstacles détectés (par rapport au robot), en coordonnées absolues (par rapport au terrain de jeu). Cela exige évidemment un système de positionnement qui permet de calculer très précisément l'angle du robot. Sinon, toute petite erreur sera propagée sur les coordonnées de la position de l'obstacle par rapport à la table. Par la suite, il est possible de déterminer si l'obstacle est un élément de jeu en fonction d'un plan de terrain prédéfini dans la mémoire du robot.


\subsubsection{Implémentation de divers formes de la zone de détection} \label{S2-5-à-faire-zones}

Jusqu'à présent, la zone d'arrêt (zone où la présence d'un obstacle produira un arrêt immédiat du robot) prenait la forme d'un quart de cercle devant le robot, comme le montre la figure~\ref{table:S2-5-zone-detection-ancien}. Cette forme est loin d'être optimale, car elle met trop d'importance sur les obstacles latéraux éloignés et pas assez sur des obstacles rapprochés sur les côtés. Ce problème est illustré par l'exemple de la figure~\ref{table:S2-5-zone-detection-ancien-probleme}, où le robot rouge est beaucoup plus proche et présente beaucoup plus de danger de collisions que le robot vert. Pourtant, contrairement au robot rouge, le robot vert se trouve dans la zone d'arrêt et sa présence produira un freinage du robot. Il est à remarquer que cette erreur peut être désastreuse si les dimensions du robot rouge sont un peu plus grandes que celles du robot bleu : une situation qui pourrait endommager ce dernier.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-5-zone-detection-ancien.png}
	\caption{Zone d'arrêt utilisée pour la compétition \textit{Eurobot 2019}}
	\label{table:S2-5-zone-detection-ancien}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-5-zone-detection-ancien-probleme.png}
	\caption{Problème avec la zone d'arrêt utilisée pour la compétition \textit{Eurobot 2019}}
	\label{table:S2-5-zone-detection-ancien-probleme}
\end{figure}

Lors des séances de remue-méninges de détection, il fut proposé d'avoir une zone pour arrête le robot et une zone plus large pour ralentir le robot. La zone d'arrêt pourrait prendre la forme d'un rectangle. Puisque le robot se déplace lentement, le rectangle pourrait être seulement légèrement plus large que lui. Un aperçu de la forme d'arrêt proposée est présentée à la figure~\ref{table:S2-5-zone-detection-arreter}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{figures/S2-sous-systèmes/S2-5-zone-detection-arreter.png}
	\caption{Zone d'arrêt rectangulaire proposée}
	\label{table:S2-5-zone-detection-arreter}
\end{figure}

La zone de ralentissement pourrait prendre la forme d'une ellipse. Elle serait plus large et plus longue que la zone rectangulaire, afin de ralentir le robot si un adversaire arrive par le côté. Un aperçu de la zone elliptique proposée est présenté à la figure~\ref{table:S2-5-zone-detection-ralentir}. L'exemple de la figure~\ref{table:S2-5-zone-detection-exemple} montre que le même adversaire rouge ferait ralentir le robot, mais ne le ferait pas arrêter, vu qu'il ne se trouve pas dans son chemin.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.4\linewidth}
		\includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-5-zone-detection-rallentir.png}
		\caption{Zone elliptique}
		\vspace{12pt}
	\end{subfigure}\hfill
	\begin{subfigure}[b]{0.4\linewidth}
		\includegraphics[width=\linewidth]{figures/S2-sous-systèmes/S2-5-zone-detection-superposition.png}
		\caption{Superposition de la zone elliptique et de la zone rectangulaire}
	\end{subfigure}
	\caption{Zone de ralentissement elliptique proposée}
	\label{table:S2-5-zone-detection-ralentir}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-5-zone-detection-exemple.png}
	\caption{Zone d'arrêt rectangulaire proposée}
	\label{table:S2-5-zone-detection-exemple}
\end{figure}

Il faudrait expérimenter avec la largeur et la longueur de ces zones afin de trouver des formes qui réussissent à arrêter le robot avant qu'une collusion ne se produise, mais qui ne sont pas trop sensibles de telle sorte que le robot ne s'arrête que dans des situations qui sont dangereuses. Tout cela dépendra aussi évidement des dimensions du robot et de sa vitesse de déplacement.

\subsubsection{Implémentation de l'algorithme de recherche de chemin}

Cette tâche consiste à programmer un algorithme continu de recherche de chemin, comme décidé à la section~\ref{S2-5-choix-contournement}. L'algorithme doit prendre comme entrée une coordonnée d'origine et une coordonnée de destination et doit trouver le chemin le plus court en fonction des obstacles. Le temps de traitement devrait être minime. Idéalement, le traitement devrait prendre une fraction de seconde.

\subsection{Échéancier}

Les tâches présentées aux sous-sections précédentes furent insérées dans le diagramme de Gantt de la figure~\ref{table:S2-5-gantt}. Il est possible de voir que toutes les tâches de programmation sont complétées. Le travail effectué relativement à ces tâches et présenté à la section~\ref{S2-5-travail-accompli}. Le diagramme montre également que la tâche \textquote{Tests} a dû être abandonnée. En effet, la pandémie COVID-19 a empêché la tenue des tests prévus pour le système. Cela sera plus amplement expliqué à la section~\ref{S2-5-tests}.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-5-echeance.png}
	\caption{Diagramme de Gantt représentant l'échéance des tâches pour les systèmes de détection et de contournement}
	\label{table:S2-5-gantt}
\end{figure}

\subsection{Travail accompli} \label{S2-5-travail-accompli}

\input{sections/S2-Sous-systèmes/S2-5-detection-contournement/contournement.tex}

\input{sections/S2-Sous-systèmes/S2-5-detection-contournement/filtre.tex}

\input{sections/S2-Sous-systèmes/S2-5-detection-contournement/zones.tex}

\input{sections/S2-Sous-systèmes/S2-5-detection-contournement/communication.tex}

\input{sections/S2-Sous-systèmes/S2-5-detection-contournement/interface.tex}

\subsubsection{Tests} \label{S2-5-tests}

Des tests étaient prévus pour le système de détection et de contournement. Il était planifié de mettre un obstacle fixe dans le chemin du robot et vérifier qu'il est en mesure de trouver un trajet pour contourner l'obstacle. Malheureusement, en raison de la pandémie actuelle, ces tests n'ont pas pu être tenus. En effet, ces tests nécessitaient l'accès au terrain de jeu et à des anciens robots pour utiliser en guise d'obstacles. Vu les mesures de confinement et la fermeture des bâtiments du campus pour éviter la propagation de la COVID-19, il était impossible d'effectuer les tests. Plus de renseignements concernant les mesures d'adaptation prises face à la pandémie sont disponibles à la section~\ref{S5-COVID}.

Les tests complets du système entier ont été annulés. Par contre, il était quand même possible de tester chaque composante du système individuellement afin de s'assurer que les fonctionnalités de base sont présentes. En effet, le filtrage, les zones de détection ainsi que l'algorithme de contournement ont chacun été testés de façon isolée.
