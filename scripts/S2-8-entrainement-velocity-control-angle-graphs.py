import json
import math
from os import path

import matplotlib
from matplotlib import rcParams
import matplotlib.pyplot as plt


matplotlib.rcParams['text.usetex'] = True
rcParams.update({'figure.autolayout': True})


def degrees(gen):
    return [math.degrees(x) for x in gen]


filename = path.join(path.dirname(__file__), '../data/S2-8-velocity-control-data/data-2020-04-07T22:10:41-w(1, 0.0, 0.07)-max=1.5-max_dot=2-v(0.6, 0.015, 0.1)-max=120-max_dot=200.json')

print(filename)
with open(filename, 'r') as f:
    data = json.load(f)

    NUM = 400

    times = data['times']
    times = [t - times[0] for t in times]
    x = math.degrees(data['omegas'][0])

    fig, ax1 = plt.subplots(figsize=(8, 5))
    plt.grid()
    color = 'tab:blue'
    ax1.set_xlim([0, times[-1]])
    ax1.set_xlabel(r'Temps ($s$)', fontsize=16)
    ax1.set_ylabel(
        r'Erreur sur l\'angle ($^{\circ}$)',
        color=color,
        fontsize=16,
    )
    ax1.set_ylim([-20, 200])
    ax1.tick_params(axis='x', labelsize=16)
    ax1.tick_params(axis='y', labelcolor=color, labelsize=16)
    ax1.plot(
        times,
        degrees(data['heading_errors'])[:NUM],
        color=color,
    )

    ax2 = ax1.twinx()
    color = 'tab:red'
    ax2.set_ylabel(
        r'Vitesse angulaire ($^{\circ}/s$)',
        color=color,
        fontsize=16,
    )
    ax2.tick_params(axis='y', labelcolor=color, labelsize=16)
    ax2.set_ylim([-20 / 180 * x, 200 / 180 * x])
    ax2.plot(
        times,
        degrees(data['omegas'][:NUM]),
        color=color,
    )

    fig.savefig(path.join(
        path.dirname(__file__),
        '../figures/S2-sous-systèmes/S2-8-velocity-control-heading-angle.png',
    ))
