def lire_girouette(rdi):
    """
    Détermination de l'orientation de la girouette à partir
    d'une région d'image `rdi`
    """

    # Seuillage binaire
    # Valeur maximaile : 255
    # Seuil : 127 = 255 / 2
    seuil = 127
    max_val = 255
    ret, res = cv2.threshold(
        rdi,
        seuil,
        max_val,
        cv2.THRESH_BINARY
    )

    # Binarisation
    # Conversion de la matrice en booléens
    bin_res = (res > seuil).astype(int)

    # Pixels de la colonne du milieu
    col_fleche = np.size(bin_res, 1) // 2
    coul_fleche = np.all(bin_res[:, col_fleche])

    # Détermination de la direction
    # "S" si les pixels sont blancs, "N" sinon
    direction = "S" if coul_fleche else "N"

    return direction
