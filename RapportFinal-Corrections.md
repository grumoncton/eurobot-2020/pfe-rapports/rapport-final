# Rapport final

## Résumé

* 3e paragraphe
* 4e paragraphe
* Achèvement des robots,malgré la pandémie de COVID-19


## Figures

2.3.6 --> Références



## Introduction

### Description du problème

#### Tâches à accomplir

##### 1.1.4.4 Arriver à bon port

<!-- 
Moitié ==> Pôle

Pôle indiqué par la flèche
 -->

##### 1.1.4.5 Hisser vos pavillons

<!-- Durant les 5 dernières secondes du match,  -->


### 1.2 Objectifs

#### 1.2.1 Objetifs de conception

<!-- * Obejctifs -->
<!-- * homoliguables -->

<!-- Tableau : -->

<!-- * Il doivent être -->

#### 1.2.2 Objectifs d'apprentissage

<!-- Taxonomie  -->

<!-- Tableau : -->

<!-- * systèmes entiers (complets) -->


### 1.3 Contraintes générales

<!-- Tableau :  -->

<!-- * tension_ supérieures -->

### 1.4 Planification

<!-- * Première phrase -->

<!-- Tableau :  -->

<!-- * ramasser les gobelets, effectuer des tâches d'agilité -->


## 2 Sous Systèmes

### 2.1 Ordinateur de bord

<!-- Micontrôleurs gérant -->

#### 2.1.1 Cahier des charges

<!-- étant difficile à quantifier ==> Il est difficile à quantifier, mais il sera quand même considéré -->


#### 2.1.2 Choix

<!-- * possède ==> a plusieurs Raspberry Pi à sa disposition / détient -->
<!-- * ce contrôleur fut choisi pour faire office d'ordinateur central -->

#### 2.1.3 Tâches

<!-- consiste__ à programmer -->

##### 2.1.3.1 Gestion des tâches

<!-- * séquence d'étapes -->
<!-- * qui maximisera ==> qui maximiseront -->
<!-- * l'activation d'une pompe ==> une activation de pompe à air -->
<!-- * pompe à air ==> pompe à vide -->
<!-- * séquence_s d'étapes -->
<!-- * pour optimiser le temps ==> pour mieux gérer son temps -->

#### 2.1.3.2

<!-- Début et fin des matchs (titre) -->

<!-- * lorsque du commencement ==> au commencement -->

<!-- * Changer pour "début et fin" -->
<!-- * Avant le tableau 3.3 : regelèrenet ==> règlements -->
<!-- * le protocole, le porte et ainsi de suite -->


##### 2.1.3.4

<!-- * envoi de commandes -->
<!-- * doit être en mesure ==> doit pouvoir envoyer -->
<!-- * ("timeout") ==> (en anglais, "timeout") -->
<!-- * , ce qui engendrerait plusieurs problèmes -->

<!-- * commander le servomoteur X à Y dégrées -->


#### 2.1.3.5 Estimation du ~score~ --> Pointage

Score --> Pointage

<!-- * affichages à cristaux liquides (ACL) -->


### 2.1.5 Travail accompli

#### 2.1.5.1 Gestion des tâches

<!-- * Ainsi, La seule fonction -->
<!-- * assigner un temps limite  -->
<!-- * tâche pour continuer après un certain délai ==> tâche, pour continuer avec la prochaine, après un temps imparti -->


#### 2.1.5.2 Début et arrêt des matchs

<!-- Début et fin des matchs  -->



#### 2.1.5.3 

<!-- * format des trames ==> format de trame -->
<!-- * expîrations échéances -->

Tableau :

<!-- * protocole des communication ==> protocole de communication -->


##### 2.1.5.3.1 Hiérarchie des classes

<!-- * Des protocoles asynchrones (UART et socket) et synchrones (I²C) sont utilisés ==> Tant des protocoles asynchrones que synchrones seront utilisés -->

##### 2.1.5.3.2 Format des trames

<!-- * composées de quatre valeurs ==> composées de quatre sections -->
<!-- * paramètres pour l'instruction ==> paramètres de l'instruction -->
<!-- * est de faire tourner ==> l'instruction commande la rotation d'un servomoteur -->
<!-- * les données contiendraient l'angle cible ==> les données définiraient l'angle cible -->

##### 2.1.5.3.4 Sommes de contrôle

<!-- * Le premier type d’erreur consiste dans une expiration (« timeout ») en attendant une réponse de l’interlocuteur. --> ==>
<!-- * L'erreur la plus fréquente est un problème d'expiration de requêtes, en attente d'une réponse de l'interlocuteur -->

Figure 2.1.4 :

<!-- * Arrivée  -->



##### 2.1.5.3.5 Procédure pour l'atteinte d'une réponse

<!-- * à l'exception que l'étape d'envoyer une instruction est sauté -->
<!-- * longe action ==> si une action chronophage -->
<!-- * monter un système vis-noix -->
<!-- * Ainsi seules des erreurs correspondant à des sommes de contrôle non exactes seront à gérer -->


#### 2.1.5.4

<!-- * Une fois la preuve de concept complétée -->
* (section ??)


#### 2.1.5.5 Communication avec les sous-systèmes

##### 2.1.5.5.1 Choix de protocole de communication 

<!-- * dernières deux années ==> deux dernières -->
<!-- * Avec le protocole de communication sélective ==> Une fois le protocole de communication choisi -->
* section (??)

##### 2.1.5.5.2 Choix de protocole de communication pour la carte fille de la Pi

<!-- * les pins d’entrées / sorties ==> pins d'entrée / sortie -->

Figure 2.1.1.10

<!-- * Preuve de la communication  ==> Preuve de communication -->


### 2.2 Positionnement

#### 2.2.2 Étude préliminaire

##### 2.2.2.1 Capteurs de temps de vol

* Dans le cas des robots du GRUM, ils ont déjà été utilisés pour détecter la distance entre le robot et la paroi de la surface de jeu
* Cette méthode pourrait être utilisée dans le robot primaire, vu qu’il est chargé de ramasser les gobelets des écueils
* La précision requise pour cette tâche est critique, le robot pourrait donc utiliser des capteurs de temps de vol pour corriger sa position
* Pour les cas des robots du GRUM, des capteurs pourraient être utilisés

##### 2.2.2.2 Encodeurs

* catégories --> types

Figure 2.2.3 :

<!-- * Intérieur d'un encodeur rotatif ==> Composantes d'un encodeur rotatif -->

* Pargraphe : Move

###### 2.2.2.2.1

<!-- Figure 2.2.4 : Fonctionnement de l'encodeur rotatif -->

###### 2.2.2.2.2

<!-- Figure 2.2.5 : Fonctionnement de l'encodeur rotatif -->

###### 2.2.2.3.1

<!-- * Le sens de mouvement de l'objet ou du gyromètre -->

<!-- Figure 2.2.7 -->

<!-- L'effet Sagnac -->

###### 2.2.2.3.2

<!-- * Souvent, trois accéléromètres ==> Dans la plupart des cas -->

##### 2.2.2.3 Trilatération

<!-- * distance au carrée ==> distance au carré -->

#### 2.2.4 Choix final

* matrice de décision n'est pa<!-- s nécessaire
* Chacune des composantes décrites à la section "Études préliminaire" fut choisie pour faire partie du système de positionnement. Ces  -->

##### 2.2.4.2 Encodeurs

<!-- * Les encodeurs sont _nécessaires pour assurer une bonne marche_ des robots -->
<!-- * Les signaux A, B et X provienent des encodeurs et doivent être branchés sur les pins numériques d'un microcontrôleur -->
<!-- * Ces capteurs seront alimentés avec une tension de 5 V par la carte fille de la Pi à et communiquera avec le microcontrôleur choisi pour cette carte. -->


##### 2.2.4.4 Trilatération

<!-- * Changer le titre -->
<!-- * Les « DWM1001‐DEV » peuvent avoir diverses sortes d’application. ==> Les « DWM1001‐DEV » peuvent être utilisées dans une variété d'applications.
* Les systèmes de localisation en temps réel à ondes décamétriques, telles que le Decawave, sont composés de noeuds fixes (appelés "Ancres") ayant un positonnement absolu et de noeuds mobiles (appelés "Étiquettes") pour laquelle la position est calculé à partir des ancres.
* Les ancres seront placés dans des balises placées sur les supports de balises fixes, alors que les étiquettes seront intégrés dans les robots.
* Une communication UART sera établie entre les Decawave et l'ordinateur de bord. Cette communication alimentera les étiquettes avec une tension de \SI{5}{\volt}. -->


#### 2.2.5 



##### 2.3.1.4 Décision finale du mécanisme pour la manipulation et stockage des gobelets

* La matrice du tableau permet de choisir les concepts qui seront incorporés aux robots de la compétition Eurobot 2020.

* Une fois cette matrice complétée, deux solutions furent retenues, à savoir la pince horizontale fixe ainsi que le système à ventouses. Il peut être constaté que ces solutions sont plus avantageuses que les autres concepts, vu que ces concepts peuvent être implémentés de différentes manières. De même, ils sont tous deux capables de ramasser plus d’un gobelet à la fois.






### Budget

* Trois points 3.1 / 3.2 / 3.3


