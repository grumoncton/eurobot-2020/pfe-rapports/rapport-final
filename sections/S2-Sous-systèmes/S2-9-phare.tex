% vim ft=tex, spelllang=fr


\section{Phare} \label{S2-9-phare}

Le phare est un élément de jeu conçu par les équipes participantes à Eurobot . Il n'est, par contre, pas obligatoire pour participer à la compétition. Cependant, le phare est un élément qui permet de réaliser une action simple permettant d'obtenir des points faciles. Comme mentionné à la section~\ref{S1-1c-allumerPhare}, le phare doit être activé, durant le match, par un contact physique avec un des robots de l'équipe. Une fois activé, le phare doit se déployer et allumer une lumière fixée sur son sommet.

Selon les règlements d'\textit{Eurobot 2020} (annexe~\ref{appendix:regles}), le phare doit être d'une hauteur maximale de \SI{300}{\milli\meter} avant déploiement et doit être d'une hauteur d'au moins \SI{700}{\milli\meter} après déploiement, en excluant la source lumineuse. La source lumineuse, une fois activée, doit effectuer un mouvement de balayage d'au moins \SI{180}{\degree}, du côté de la surface de jeu.


\subsection{Choix de système de levée}

Le système de levée du phare est très important car si celui-ci ne se déploie pas, l'équipe manque une occasion d'obtenir des points faciles. Ceci étant dit, le système de levée doit être le plus simple que possible, afin d'éviter des complications lors du déploiement. De plus, le mécanisme doit se déployer dans un temps convenable afin d'être complètement étendu avant la fin du match.

\subsubsection{Cahier des charges}

Selon la section D.6.b. des règlements de la compétition (annexe~\ref{appendix:regles}), le phare a des dimensions maximales de $\SI{222}{\mm} \times \SI{450}{\mm} \times \SI{300}{\mm}$ avant déploiement. Après déploiement, la lumière doit être perchée à plus de \SI{700}{\mm} et à moins de \SI{900}{\mm} de hauteur. De plus, le système de phare au complet ne doit pas avoir une masse supérieure à \SI{3}{\kilo\gram}. Ces deux règlements sont des contraintes qui doivent être pris en compte pour le cahier des charges. Enfin, il est important de minimiser le coût.

Le tableau~\ref{table:S2-9-levee-cahier-des-charges} ci-dessous représente le cahier des charges du choix de système de levée basé sur les contraintes mentionnées plus haut.

\begin{table}[H]
	\caption{Cahier des charges utilisé lors du choix de système de levée}
	\label{table:S2-9-levee-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{Xcccc}
		\toprule
		\textbf{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		\textbf{Simplicité} & $\SI{20}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\textbf{Temps de montée} & $\SI{10}{\percent}$ & \makecell{
			$\leq \SI{10}{\second}: \SI{100}{\percent}$ \\
			$\leq \SI{15}{\second}: \SI{60}{\percent}$ \\
			$> \SI{15}{\second}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\textbf{Taille avant déploiement} & $\SI{20}{\percent}$ & - & - & \makecell{
			$\SI{222}{\mm} \times$ \\
			$\SI{450}{\mm} \times$ \\
			$\SI{300}{\mm}$ \\
		} \\

		\midrule

		\textbf{Taille après déploiement} & $\SI{20}{\percent}$ & - & - & \makecell{
			$\SI{222}{\mm} \times$ \\
			$\SI{450}{\mm} \times$ \\
			$\SI{900}{\mm}$ \\
		} \\

		\midrule

		\textbf{Masse} & \SI{20}{\percent} & - & - & \SI{3}{\kilo\gram} \\

		\midrule

		\textbf{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{100}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{150}{\$}: \SI{80}{\percent}$ \\
			$\leq \SI{200}{\$}: \SI{60}{\percent}$ \\
			$> \SI{200}{\$}: \SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Étude préliminaire}

\paragraph{Élévateur à ciseaux}

Des élévateurs à ciseaux sont courants sur les chantiers de construction et sont utilisés pour soulever des ouvriers à des hauteurs appropriées pour le travail. Le système est constitué de tiges montées en forme de $X$, comme des ciseaux. La base est attachée à une tige fixe et une tige mobile. Lorsque la tige mobile bouge sur un axe parallèle à la base, le système fait l'action d'un ciseau et les tiges s'éloignent, de telle sorte qu'elles deviennent parallèles. La figure~\ref{fig:S2-9-levee-ciseaux-non-deploye} ci-dessous démontre le système d'élévateur à ciseaux avant et après un déploiement.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.4\linewidth}
		\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-9-levee-ciseaux-non-deploye.png}
		\caption{Non déployé}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.4\linewidth}
		\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-9-levee-ciseaux-deploye.png}
		\caption{Déployé}
	\end{subfigure}
	\caption{Exemple d'élévateur à ciseaux}
	\label{fig:S2-9-levee-ciseaux-non-deploye}
\end{figure}

Plus le système est haut, plus sa base est étroite, ce qui peut affecter la rigidité du système. Ce système est connu, de par son utilisation dans les chantier. Il serait donc facile à reproduire à petite échelle.

\paragraph{Tiges avec moteurs}

Le système à tiges avec moteurs est un concept qui consiste à faire pivoter deux membres à l'aide de servomoteurs, afin d'obtenir une bras droit atteignant une hauteur voulue. Un servomoteur est fixé à la base du phare et commande un membre, le permettant ainsi de pivoter sur une plage de \SI{180}{\degree}, par rapport à la base. Un deuxième servomoteur est fixé au bout de ce membre et contrôle un deuxième membre raccordé à celui-ci. Ce servomoteur est capable de pivoter sur \SI{270}{\degree}. La figure~\ref{fig:S2-9-levee-tiges} montre un exemple de système à tiges avec moteurs.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-9-levee-tiges.png}
	\caption{Exemple de système à tiges avec moteurs}
	\label{fig:S2-9-levee-tiges}
\end{figure}

Lorsque les deux tiges sont parfaitement perpendiculaires à la base, le système est déployé complètement et la hauteur désirée est atteinte. Il faut aussi considérer que le premier servomoteur doit être assez grand pour supporter le poids des deux membres et du deuxième servomoteur.

\paragraph{Tige avec actionneur linéaire}

Le système de tige avec actionneur linéaire est un concept qui consiste à faire monter une tige à l'aide d'un actionneur linéaire. La tige serait fixée à un palier situé au bas de la tige, permettant de créer un point de pivot. Un actionneur est situé au centre de la tige et est attaché à la tige et à la base du système à l'aide de paliers. Lorsque celui-ci est activé, la tige est poussée vers le haut jusqu'à ce que la hauteur voulue soit atteinte. La figure~\ref{fig:S2-9-levee-actionneur-linaire} démontre un exemple de système de tige avec actionneur linéaire.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-9-levee-actionneur-lineaire.png}
	\caption{Exemple de système de tige avec actionneur linéaire}
	\label{fig:S2-9-levee-actionneur-linaire}
\end{figure}

Le système de tige avec actionneur linéaire contient peu de composantes, comparativement aux deux premières alternatives.

\subsubsection{Étude de faisabilité}

Avant de faire la sélection du système de levée du phare avec une matrice de décision, une étude de faisabilité a été réalisée pour les 3 concepts présentés dans la section précédente. Les options furent évaluées selon leur faisabilité physique, leur efficacité, simplicité et faisabilité économique, comme présenté au tableau~\ref{table:S2-9-levee-faisabilité}.

Tous les trois concepts ont été acceptés avec l'étude de faisabilité, mais le système d'élévateur à ciseaux dépend de la simplicité et de la faisabilité économique. Cela est dû au fait que le système est un peu complexe, vu qu'il comporte plusieurs pièces mobiles en même temps et parce qu'autant de pièces augmentent les coûts par rapport aux deux autres concepts.

\begin{table}[H]
	\caption{Étude de la faisabilité des trois alternatives pour le système de levée}
	\label{table:S2-9-levee-faisabilité}
	\begin{tabularx}{\textwidth}{Xllllll}
		\toprule
		\textbf{Alternative} & \textbf{Physique} & \textbf{Efficacité} & \textbf{Simplicité} & \textbf{Économique} & \textbf{Décision} \\
		\midrule
		Élévateur à ciseaux & Oui & Oui & Oui, mais & Oui, mais & Acceptée \\
		Tiges avec moteurs & Oui & Oui & Oui & Oui & Acceptée \\
		Tige avec actionneur linéaire & Oui & Oui & Oui & Oui & Acceptée \\
		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Décision finale du système de levée}

La matrice de décision du tableau~\ref{table:S2-9-levee-matrice-de-decisions} ci-dessous fut utilisée afin de choisir le système de levée du phare en fonction du cahier des charges établi au tableau~\ref{table:S2-9-levee-cahier-des-charges}. Parmi les deux concepts, le système à tiges et moteurs fut choisi comme système de levée du phare avec le meilleur score à \SI{98.8}{\percent}.

\begin{table}[H]
	\caption{Matrice de décision utilisée lors du choix de système de levée}
	\label{table:S2-9-levee-matrice-de-decisions}
	\centering
	\begin{tabularx}{\textwidth}{Xccccc}
		\toprule
	\textbf{Critère} & \textbf{Pondération} & \textbf{Barème} & \makecell{\textbf{Élévateur}\\\textbf{à ciseaux}} & \makecell{\textbf{Tiges avec}\\\textbf{moteurs}} & \makecell{\textbf{Tige avec}\\\textbf{actionneur}\\\textbf{linéaire}} \\

		\midrule

		\textbf{Simplicité} & $\SI{20}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & \SI{65}{\percent} & \SI{95}{\percent} & \SI{80}{\percent} \\

		\midrule

		\textbf{Temps de montée} & $\SI{10}{\percent}$ & \makecell{
			$\leq \SI{10}{\second}: \SI{100}{\percent}$ \\
			$\leq \SI{15}{\second}: \SI{60}{\percent}$ \\
			$> \SI{15}{\second}: \SI{0}{\percent}$ \\
		} & \SI{100}{\percent} & \SI{100}{\percent} & \SI{100}{\percent} \\

		\midrule

		\textbf{Taille avant déploiement} & $\SI{20}{\percent}$ & - & \SI{100}{\percent} & \SI{100}{\percent} & \SI{100}{\percent} \\

		\midrule

		\textbf{Taille après déploiement} & $\SI{20}{\percent}$ & - & \SI{100}{\percent} & \SI{100}{\percent} & \SI{100}{\percent} \\

		\midrule

		\textbf{Masse} & \SI{20}{\percent} & - & \SI{100}{\percent} & \SI{100}{\percent} & \SI{100}{\percent} \\

		\midrule

		\textbf{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{100}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{150}{\$}: \SI{80}{\percent}$ \\
			$\leq \SI{200}{\$}: \SI{60}{\percent}$ \\
			$> \SI{200}{\$}: \SI{0}{\percent}$ \\
		} & \SI{80}{\percent} & \SI{98}{\percent} & \SI{80}{\percent} \\

		\toprule

		\textbf{Total} & - & - & \SI{91}{\percent} & \SI{98.8}{\percent} & \SI{94}{\percent} \\

		\bottomrule
	\end{tabularx}
\end{table}

\subsection{Choix de système de lumière}

Le système de lumière du phare est un mécanisme très simple, mais aussi très important car l'équipe n'accumule pas de points, s'il ne fonctionne pas efficacement. Ceci étant dit, le système de lumière doit être simple pour éviter des problèmes électroniques lors du déploiement.

\subsubsection{Cahier des charges}

Selon la section D.6.b. des règlements de la compétition (annexe~\ref{appendix:regles}), le phare doit avoir une hauteur minimum de \SI{700}{\mm} et ne pas dépasser une hauteur de \SI{900}{\mm} après déploiement. Cela implique que le mécanisme de lumière ne doit pas dépasser une hauteur de \SI{200}{\mm}. De plus, le système de lumière du phare doit être capable d'effectuer un mouvement de balayage, simulant l'effet d'un phare, pour un angle minimum de \SI{180}{\degree}. Ces deux règlements sont des contraintes qui doivent être prises en compte pour le cahier des charges. Enfin, comme toujours, des concepts à bas coût doivent être favorisés.

Le tableau~\ref{table:S2-9-lumiere-cahier-des-charges} ci-dessous représente le cahier des charges du choix de système de lumière, basé sur les contraintes mentionnées ci-dessus.

\begin{table}[H]
	\caption{Cahier des charges utilisé pour le choix de la lumière}
	\label{table:S2-9-lumiere-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{Xcccc}
		\toprule
		\textbf{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		\textbf{Simplicité} & $\SI{30}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\textbf{Balayage} & $\SI{30}{\percent}$ & - & \SI{180}{\degree} & - \\

		\midrule

		\textbf{Taille} & $\SI{20}{\percent}$ & - & - & \SI{200}{\mm} \\

		\midrule

		\textbf{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{50}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{100}{\$}: \SI{80}{\percent}$ \\
			$\leq \SI{200}{\$}: \SI{60}{\percent}$ \\
			$> \SI{200}{\$}: \SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Étude préliminaire}

\paragraph{Lumières DEL}

Les lumières DEL sont des composantes peu dispendieuses et peuvent être adaptés à des circuits électroniques pour créer différents motifs d'éclairage.  Le but de ce concept serait d'agencer une série de DEL pour en forme de cercle et de les contrôler avec un microcontrôleur et un système d'adressage, afin de créer une séquence d'éclairage qui reproduirait l'effet d'une lumière d'un phare. La figure~\ref{fig:S2-9-lumiere-del} ci-dessous montre un exemple de circuit imprimé avec des lumières DEL montées dans un forme d'un cercle.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-9-lumiere-del.jpg}
	\caption{Concept de système à lumière DEL~\cite{neopixel}}
	\label{fig:S2-9-lumiere-del}
\end{figure}

\paragraph{Lumière de hockey}

Les lumières de hockey sont très semblables aux faisceaux des phares, tant dans leur fonctionnement que dans leur construction. La lumière de hockey comporte une lumière fixée au centre de l'appareil qui reste allumée et un miroir concave tournant qui permet de projeter la lumière dans une direction. La figure~\ref{fig:S2-9-lumiere-hockey} ci-dessous démontre un exemple de lumière de hockey.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-9-lumiere-hockey.jpg}
	\caption{Exemple de lumière de hockey~\cite{hockey}}
	\label{fig:S2-9-lumiere-hockey}
\end{figure}

Ce concept consisterait à acheter une lumière de hockey qui serait attachée au sommet du système de levée choisi à la section précédente.

\subsubsection{Étude de faisabilité}

Avant de faire la sélection du système de lumière du phare avec une matrice de décision, une étude de faisabilité a réalisée été pour les 2 concepts présentés dans la section précédente. Les options furent évaluées selon leur faisabilité physique, leur efficacité, leur simplicité et leur faisabilité économique, comme présenté au tableau~\ref{table:S2-9-lumiere-faisabilité} ci-dessous.

Tous les deux concepts ont été acceptés avec l'étude de faisabilité, mais le système à lumière de hockey a quelques restrictions sur le plan physique, vu que les lumière de hockey sont normalement plus grandes en taille que des simples lumières à DEL.

\begin{table}[H]
	\caption{Étude de la faisabilité des deux alternatives pour le système de lumière}
	\label{table:S2-9-lumiere-faisabilité}
	\begin{tabularx}{\textwidth}{Xllllll}
		\toprule
		\textbf{Alternative} & \textbf{Physique} & \textbf{Efficacité} & \textbf{Simplicité} & \textbf{Économique} & \textbf{Décision} \\
		\midrule
		Lumière DEL & Oui & Oui & Oui & Oui & Acceptée \\
		Lumière de hockey & Oui, mais & Oui & Oui & Oui & Acceptée \\
		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Décision finale du système d'éclairage}

La matrice de décision du tableau~\ref{table:S2-9-lumiere-cahier-des-charges} fut utilisée pour choisir le système de lumière du phare, en fonction du cahier des charges établi au tableau~\ref{table:S2-9-lumiere-matrice-de-decision}. Parmi les trois concepts, le système à lumière DEL fut choisi comme système de lumière du phare avec le meilleur score à \SI{99}{\percent}.

\begin{table}[H]
	\caption{Cahier des charges utilisé lors du choix de système de levée}
	\label{table:S2-9-lumiere-matrice-de-decision}
	\centering
	\begin{tabularx}{\textwidth}{Xcccc}
		\toprule
		\textbf{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Lumière DEL} & \textbf{Lumière de hockey} \\

		\midrule

		\textbf{Simplicité} & $\SI{30}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & \SI{100}{\percent} & \SI{100}{\percent} \\

		\midrule

			\textbf{Balayage} & $\SI{30}{\percent}$ & - & \SI{100}{\percent} & \SI{100}{\percent} \\

		\midrule

			\textbf{Taille} & $\SI{20}{\percent}$ & - & \SI{100}{\percent} & \SI{70}{\percent} \\

		\midrule

		\textbf{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{50}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{100}{\$}: \SI{80}{\percent}$ \\
			$\leq \SI{200}{\$}: \SI{60}{\percent}$ \\
			$> \SI{200}{\$}: \SI{0}{\percent}$ \\
		} & \SI{95}{\percent} & \SI{80}{\percent} \\

		\toprule

			\textbf{Total} & - & - & \SI{99}{\percent} & \SI{90}{\percent} \\
		\bottomrule
	\end{tabularx}
\end{table}

\subsection{Échéancier}

Aucun échéancier n'est disponible vu que le travail pour le phare a été délégué à un groupe du cours GELE3700 - \textit{Projet de Génie Électrique I}.

\subsection{Travail accompli}

Une fois le système de levée choisi, la conception des parties mécaniques a été réalisée à l'aide du logiciel \textit{Solid Edge}. La figure~\ref{fig:S2-9-conception-finale} ci-dessous représente le design final du phare produit avec \textit{Solid Edge}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-9-conception-finale.jpg}
	\caption{Concept final du phare}
	\label{fig:S2-9-conception-finale}
\end{figure}

Une fois le design mécanique du phare complété, il a fallu calculer les tailles de servomoteurs nécessaires à la levée des deux tiges. Ces tiges seront imprimées à l'aide d'une imprimante 3D. Le premier servomoteur aura une capacité de \SI{60}{\kilo\gram} car il devra soulever deux tiges et un autre servomoteur. Le deuxième servomoteur aura une capacité de \SI{25}{\kilo\gram}, vu qu'il devra seulement soulever une tige et le système de lumière.

L'assemblage du phare est complété à \SI{75}{\percent}. Le dernier \SI{25}{\percent} consiste à réaliser la partie électronique, tâche qui fut déléguée à une équipe du cours GELE3700 (Projet de génie électrique I). Vu les circonstances exceptionnelles dûes à la pandémie, les composantes électroniques ont été retardées. Ceci étant dit, tous les mécanismes électroniques, ainsi que le code pour faire fonctionner le phare seront développés au cours des prochains mois, après la fin du projet. Plus de renseignements concernant les mesures d'adaptation prises face à la pandémie sont disponibles à la section~\ref{S5-COVID}.
