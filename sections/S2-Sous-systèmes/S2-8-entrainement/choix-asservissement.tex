% vim: ft=tex spelllang=fr

\subsection{Choix de la méthode d'asservissement}

\subsubsection{Cahier des charges}

Le choix de la méthode d'asservissement du système d'entraînement est très important, car si l'entraînement du robot fonctionne très bien, le robot n'aura pas de difficulté à compléter ses tâches sans erreur et dans un temps approprié. La méthode d'asservissement doit être structurellement simple, mais performante et robuste. De plus, si la méthode d'asservissement peut corriger l'erreur au fur et à mesure du déplacement du robot, cela éviterait de cumuler de l'incertitude.

Le tableau~\ref{table:S2-8-methode-cahier-des-charges} ci-dessous représente le cahier des charges du choix de la méthode d'asservissement, en se basant sur les critères mentionnés ci-dessus.

\begin{table}[H]
	\caption{Cahier des charges utilisé pour le choix de la méthode d'asservissement}
	\label{table:S2-8-methode-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{Xcccc}
		\toprule
		\textbf{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		Autocorrection d'erreur & $\SI{50}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Simplicité & $\SI{25}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Changement de destination sans attendre & $\SI{25}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}



\subsubsection{Étude préliminaire}

Le contrôleur de moteur sélectionné, le \textit{ODrive}, supporte plusieurs modes d'asservissement, y compris l'asservissement de position et l'asservissement de vitesse. Ces deux modes sont ainsi considérés pour le robot.

L'asservissement du \textit{ODrive} se fait en boucle fermée. En mode de commande de vitesse, la carte contrôle le courant du moteur et la fréquence des impulsions qui lui sont envoyées, en utilisant un encodeur pour la rétroaction. En mode de commande de position, la positon est contrôlée en cascade avec la vitesse. L'erreur sur la position est utilisée afin de calculer la vitesse cible. Les encodeurs sont encore une fois utilisés pour la régulation.


\paragraph{Contrôle de vitesse}

Un asservissement à contrôle de vitesse pour un robot mobile à entraînement différentiel envoie des signaux de commande de vitesse à ses deux roues. Les vitesses des roues sont contrôlées de façon continue en fonction de l'erreur entre la position actuelle du robot et sa destination. Le contrôle de vitesse se différencie du contrôle de position en ce sens, que les commandes de vitesse sont envoyées aux roues. La figure~\ref{fig:S2-8-asservissement-vc-principe} donne le principe d'une boucle de rétroaction de contrôle de vitesse.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.8, every node/.style={transform shape}, node distance=2cm]
		\node (start) [startstop] {Début};

		\node (read) [io, below of=start] {Lecture de la position actuelle};
		\draw [arrow] (start) -- (read);

		\node (error) [process, below of=read] {Calcul de l'erreur sur la position};
		\draw [arrow] (read) -- (error);

		\node (calc) [process, below of=error, text width=4cm, yshift=-.4cm] {Calcul de la vitesse des deux roues en fonction de l'erreur};
		\draw [arrow] (error) -- (calc);

		\node (send) [io, below of=calc, text width=6cm, yshift=-.4cm] {Envoi des commandes de vitesse aux roues};
		\draw [arrow] (calc) -- (send);

		\draw [arrow] (send) -- ++(4,0) |- (read);

	\end{tikzpicture}

	\caption{Principe de fonctionnement de l'asservissement par contrôle de vitesse}
	\label{fig:S2-8-asservissement-vc-principe}
\end{figure}

La dynamique du robot est donnée par l'équation~\ref{eq:S2-8-vc-modèle-vl-rv}~\cite{ControlOfMobileRobots}. La position du robot est donnée par les coordonnées $(x, y, \theta)$ du point centré entre ses roues. La notation Newtonienne ($\dot{x}$, $\dot{y}$, $\dot{\theta}$) est utilisée pour les dérivées instantanées. La vitesse de la roue droite est notée $v_d$, tandis que celle de la roue gauche est notée $v_g$. Le rayon des roues est noté $R$ et la séparation des roues est notée $L$. Les vitesses des roues sont données en \si{\radian\per\second} et la position $(x, y)$ du robot est en \si{\mm}. Ainsi, les unités de $R$ sont des \si{\mm\per\radian} pour faire la conversion. Les unités de $L$ sont les \si{\mm\per\radian}.

\begin{align}
	\begin{cases}
		\dot{x} = \frac{R}{2} (v_d + v_g) \cos(\theta) \\
		\dot{y} = \frac{R}{2} (v_d + v_g) \sin(\theta) \\
		\dot{\theta} = \frac{R}{L} (v_d - v_g)
	\end{cases}
	\label{eq:S2-8-vc-modèle-vl-rv}
\end{align}

Il est difficile de commander directement les vitesses des roues. Cela nécessite d'exprimer l'erreur pour chaque roue, en fonction de l'erreur sur la distance ainsi que l'erreur angulaire. Un régulateur sera configuré pour chaque roue pour minimiser cette erreur. Il serait très difficile de trouver deux régulateurs qui coopèrent avec ce modèle et qui asservissent le robot de façon performante.

C'est pourquoi, en temps normal, un modèle est utilisé pour convertir les vitesses des roues pour qu'elles soient contrôlables. Le modèle fait office de couche d'abstraction pour des mesures de bas niveau, telles que les vitesses des roues.

Un modèle répandu pour cette tâche est le modèle du monocycle (\techterm{unicycle model}, en anglais)~\cite{ControlOfMobileRobots, TimBowerUnicycle, CollisionFreeTracking, ControlUnicycle}. Ce modèle considère que le robot a une seule roue, où seules les vitesses linéaire $v$ et angulaire $\omega$ peuvent êtres commandées. Les variables de commandes sont donc $v$ et $\omega$. Ces deux quantités peuvent être régulées, respectivement, en fonction de la distance du point cible et de l'angle entre le robot et le point cible.

La dynamique du modèle est donnée à l'équation~\ref{eq:S2-8-vc-modèle-v-omega}~\cite{ControlOfMobileRobots}. Les expressions pour les vitesses des roues en fonction des vitesses du modèle monocycle sont obtenues en substituant $\dot{x}$, $\dot{y}$ et $\dot{\theta}$ par les expressions de l'équation~\ref{eq:S2-8-vc-modèle-vl-rv} et en isolant pour $v_d$ et $v_g$. Ces expressions sont données à l'équation~\ref{eq:S2-8-vc-vr-vl}.

\begin{align}
	\begin{cases}
		\dot{x} = v \cdot \cos(\theta) \\
		\dot{y} = v \cdot \sin(\theta) \\
		\dot{\theta} = \omega
	\end{cases}
	\label{eq:S2-8-vc-modèle-v-omega}
\end{align}

\begin{align}
	\begin{rcases}
		v \cdot \cancel{\cos(\theta)} = \frac{R}{2} (v_d + v_g) \cancel{\cos(\theta)} \\
		v \cdot \cancel{\sin(\theta)} = \frac{R}{2} (v_d + v_g) \cancel{\sin(\theta)}
	\end{rcases} && \longrightarrow && v &= \frac{R}{2} (v_d + v_g) \\
	&&&& \omega &= \frac{R}{L} (v_d - v_g)
\end{align}

\begin{align}
	\frac{2 v}{R} &= v_d + v_g & \frac{\omega L}{R} &= v_d - v_g \\
	%
	\frac{2 v}{R} + \frac{\omega L}{R} &= v_d + \cancel{v_g} + v_d - \cancel{v_g} &
	\frac{2 v}{R} - \frac{\omega L}{R} &= \cancel{v_d} + v_g - \cancel{v_d} + v_g \\
	%
	\Aboxed{v_d &= \frac{2 v + \omega L}{2 R}} &
	\Aboxed{v_g &= \frac{2 v - \omega L}{2 R}}
	\label{eq:S2-8-vc-vr-vl}
\end{align}

Avec ces expressions, il est possible de représenter les mouvements du robot en fonction de la vitesse linéaire et angulaire d'un monocycle et de convertir ces valeurs dans les quantités physiques utilisées par le \textit{ODrive}: $v_d$ et $v_g$. Il faut maintenant contrôler les variables de commande $v$ et $\omega$ afin de minimiser l'erreur entre la position actuelle du robot $(x, y, \theta)$ et un point cible ou une destination $(x_d, y_d, \theta_d)$.

Les variables de commande doivent être régulées de telle façon que l'erreur entre le point $(x_d, y_d, \theta_d)$ et le point $(x, y, \theta)$ converge à zéro. La vitesse linéaire $v$ est contrôlée en fonction de la distance entre les points, $d$. Cette distance est définie à l'équation~\ref{eq:S2-8-vc-d}. La vitesse angulaire $\omega$ est contrôlée en fonction de l'erreur angulaire $\theta_e$ de l'équation~\ref{eq:S2-8-vc-theta_e}. La stratégie de commande est résumée par le schéma bloc de la figure~\ref{fig:S2-8-vc-regulation}.

\begin{align}
	d &= \sqrt{(x_d - x)^2 + (y_d - y)^2} \label{eq:S2-8-vc-d} \\
	\theta_e &= \tan^{-1}\left(\frac{y_d - y}{x_d - x}\right) - \theta \label{eq:S2-8-vc-theta_e}
\end{align}

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[auto, node distance=3cm,>=latex', align=center]

		\node [sum, text width=0.5cm] (somme-v) {};
		\node [above of=somme-v, node distance=0.2cm] {\tiny{$-$}};
		\node [left of=somme-v, node distance=0.2cm] {\tiny{$+$}};

		\coordinate [left of=somme-v, node distance=1cm] (input-v) {};
		\draw [->] (input-v) -- node {$0$} (somme-v);

		\node [block, right of=somme-v, text width=3cm, node distance=2.5cm] (reg-v) {Régulation de vitesse linéaire};
		\draw [->] (somme-v) -- node {} (reg-v);

		\node [block, above of=reg-v, node distance=2cm] (calc-d) {Calcul de la distance};
		\draw [->] (calc-d) -- (calc-d -| somme-v) -- node[anchor=east] {$d$} (somme-v);

		\node [block, below of=reg-v, text width=2.5cm, node distance=2.5cm] (calc) {Calcul des vitesses des roues};
		\draw [->] (reg-v) -- node[anchor=east] {$v$} (calc);

		\node [block, below of=calc, text width=3cm, node distance=2.5cm] (reg-omega) {Régulation de vitesse angulaire};
		\draw [->] (reg-omega) -- node {$\omega$} (calc);

		\node [sum, text width=0.5cm, left of=reg-omega, node distance=2.5cm] (somme-omega) {};
		\node [below of=somme-omega, node distance=0.2cm] {\tiny{$-$}};
		\node [left of=somme-omega, node distance=0.2cm] {\tiny{$+$}};
		\draw [->] (somme-omega) -- (reg-omega);

		\coordinate [left of=somme-omega, node distance=1cm] (input-omega) {};
		\draw [->] (input-omega) -- node {$0$} (somme-omega);

		\node [block, below of=reg-omega, text width=3cm, node distance=2cm] (calc-theta) {Calcul de l'erreur angulaire};
		\draw [->] (calc-theta) -- (calc-theta -| somme-omega) -- node[anchor=east] {$\theta_e$} (somme-omega);

		\node [block, right of=calc, text width=2.5cm, node distance=4cm] (odrive) {Signaux de commande au \textit{ODrive}};
		\draw [->] (calc) -- node[anchor=south] {$v_d$} node[anchor=north] {$v_g$} (odrive);

		\node [block, right of=odrive, text width=2.3cm, node distance=3.8cm] (lire) {Lecture de la position};
		\draw [->] (odrive) -- node {} (lire);

		\draw [->] (lire) |- (calc-d);
		\draw [->] (lire) |- (calc-theta);

	\end{tikzpicture}
	\caption{Stratégie de commande en contrôle de vitesse}
	\label{fig:S2-8-vc-regulation}
\end{figure}

\paragraph{Contrôle de position}

Le contrôleur de moteurs utilisé (l'\textit{ODrive}) accepte également des commandes de position. Ces instructions déplacent les moteurs d'un certain angle à une vitesse constante. L'\textit{ODrive} utilise un régulateur de position et un régulateur de vitesse en cascade afin d'atteindre sa cible à la vitesse donnée.

Le principe de la méthode de commande est de calculer l'erreur entre la position cible et la position actuelle et de déterminer les rotations des roues nécessaires pour atteindre la destination. Puisque les vitesses des roues sont constantes, il faut tourner et se déplacer indépendamment. Le principe de commande est résumé à l'organigramme de la figure~\ref{fig:S2-8-asservissement-vp-principe}.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.8, every node/.style={transform shape}, node distance=2cm]
		\node (start) [startstop] {Début};

		\node (read) [io, below of=start] {Lire la position actuelle};
		\draw [arrow] (start) -- (read);

		\node (error-a) [process, below of=read] {Calculer l'erreur sur l'angle};
		\draw [arrow] (read) -- (error-a);

		\node (calc-a) [process, below of=error-a, text width=4cm, yshift=-.4cm] {Calculer les rotations des roues nécessaires pour corriger l'angle};
		\draw [arrow] (error-a) -- (calc-a);

		\node (send-a) [io, below of=calc-a, text width=6cm, yshift=-.4cm] {Envoyer les commandes de position aux roues};
		\draw [arrow] (calc-a) -- (send-a);

		\node (error-d) [process, below of=send-a] {Calculer l'erreur sur la distance};
		\draw [arrow] (send-a) -- (error-d);

		\node (calc-d) [process, below of=error-d, text width=5cm, yshift=-.4cm] {Calculer les rotations des roues nécessaires pour corriger la distance};
		\draw [arrow] (error-d) -- (calc-d);

		\node (send-d) [io, below of=calc-d, text width=6cm, yshift=-.4cm] {Envoyer les commandes de position aux roues};
		\draw [arrow] (calc-d) -- (send-d);

		\node (end) [startstop, below of=send-d] {Fin};
		\draw [arrow] (send-d) -- (end);
	\end{tikzpicture}

	\caption{Principe de fonctionnement de l'asservissement par contrôle de position}
	\label{fig:S2-8-asservissement-vp-principe}
\end{figure}

Le désavantage majeur de cette approche d'asservissement est qu'il n'y a aucune rétroaction sur la position. En effet, contrairement au contrôle de vitesse, où les vitesses de commandes sont calculées en fonction de l'erreur sur la position, le contrôle de position calcule les déplacements à l'avance. Le robot ne pourra ainsi pas se corriger s'il s'éloigne de son chemin. La méthode se fie à l'\textit{ODrive} pour accélérer les roues de façon symétrique.

Les avantages de cette approche sont la simplicité de la commande et le fait qu'il serait possible de réutiliser le code de la compétition \textit{Eurobot 2019}.

\subsubsection{Étude de faisabilité}

Avant de faire la sélection du système d'asservissement avec une matrice de décision, une étude de faisabilité a été réalisée pour les 2 méthodes présentées dans la section précédente. Les options furent évaluées selon leur faisabilité physique, leur efficacité, leur simplicité et leur faisabilité économique comme présenté au tableau~\ref{table:S2-8-asservissement-faisabilité} ci-dessous.

Les deux méthodes ont été acceptées avec l'étude de faisabilité. Par contre, puisque le contrôle de position ne permet pas de corriger la trajectoire au fur et à mesure, cette approche reçoit un \textquote{Oui, mais} pour l'efficacité.

\begin{table}[H]
	\caption{Étude de la faisabilité des deux alternatives de la méthode d'asservissement}
	\label{table:S2-8-asservissement-faisabilité}
	\begin{tabularx}{\textwidth}{Xllllll}
		\toprule
		\textbf{Alternative} & \textbf{Physique} & \textbf{Efficacité} & \textbf{Simplicité} & \textbf{Économique} & \textbf{Décision} \\
		\midrule
		Contrôle de vitesse & Oui & Oui, mais & Oui & Oui & Acceptée \\
		Contrôle de position & Oui & Oui & Oui & Oui & Acceptée \\
		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Décision finale de la méthode d'asservissement}

La matrice de décision du tableau~\ref{table:S2-8-methode-cahier-des-charges} ci-dessous fut utilisée afin de choisir la méthode d'asservissement du système d'entraînement en fonction du cahier des charges établi au tableau~\ref{table:S2-8-methode-matrice-de-decision}. Entre les deux méthodes, la méthode par contrôle de vitesse fut choisie comme méthode d'asservissement avec le meilleur score à \SI{87.5}{\percent}.

\begin{table}[H]
	\caption{Matrice de décision utilisée pour choisir la méthode d'asservissement}
	\label{table:S2-8-methode-matrice-de-decision}
	\centering
	\begin{tabularx}{\textwidth}{Xcccc}
		\toprule
		\textbf{Critère} & \textbf{Pondération} & \textbf{Barème} & \makecell{\textbf{Contrôle de}\\\textbf{vitesse}} & \makecell{\textbf{Contrôle de}\\\textbf{position}} \\

		\midrule

		Autocorrection d'erreur & $\SI{50}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & \SI{100}{\percent} & \SI{0}{\percent} \\

		\midrule

		Simplicité & $\SI{25}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & \SI{50}{\percent} & \SI{100}{\percent} \\

		\midrule

		Changement de destination sans pause & $\SI{25}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & \SI{100}{\percent} & \SI{0}{\percent} \\

		\toprule

			\textbf{Total} & - & - & \SI{87.5}{\percent} & \SI{25}{\percent} \\

		\bottomrule
	\end{tabularx}
\end{table}
