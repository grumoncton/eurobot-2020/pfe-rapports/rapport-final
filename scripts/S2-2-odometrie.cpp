#include "main.h"

QEI encDroite(ENC_RIGHT_A_PIN, ENC_RIGHT_B_PIN, ENC_RIGHT_X_PIN, PULSES_PER_REVOLUTION);
QEI encGauche(ENC_LEFT_A_PIN, ENC_LEFT_B_PIN, ENC_LEFT_X_PIN, PULSES_PER_REVOLUTION);

Ticker ticker;


/*
 * Actualiser position
 *
 * Utilise la distance parcourue par encodeur droite et gauche afin
 * d'approximer le déplacement x, y.
 */

void actualiserPosition() {

  double deltaDroite = PULSES_TO_MM(encDroite.getPulses());
  double deltaGauche = PULSES_TO_MM(encGauche.getPulses());

  encDroite.reset();
  encGauche.reset();

  // Distance parcourue par le centre du robot
  double deltaCentre = (deltaDroite + deltaGauche) / 2;

  // Changement de l'angle
  double aDelta = (deltaGauche - deltaDroite) / WHEEL_SEPARATION;

  // Polar à rectangulaire
  double xDelta, yDelta;
  if (aDelta != 0) {
    xDelta = (deltaCentre / aDelta) * (sin(pos.a + aDelta) - sin(pos.a));
    yDelta = (deltaCentre / aDelta) * (cos(pos.a) - cos(pos.a + aDelta));
  } else {
    xDelta = deltaCentre * cos(pos.a);
    yDelta = deltaCentre * sin(pos.a);
  }

  // Actualiser position
  pos.x += xDelta;
  pos.y += yDelta;
  pos.a += aDelta;

  // Normaliser l'angle
  pos.a = fmod(pos.a, 2 * M_PI);
}

int main() {

  // Mettre à jour la position tous les 10 ms
  ticker.attach(&actualiserPosition, 10e-3);

  while (true) {

     printf(
       "x: %d y: %d a: %d\n",
       (int) pos.x,
       (int) pos.y,
       (int) (pos.a * 180 / M_PI));

     wait(0.5);
  }

  return 1;
}
