% vim: spelllang=fr

\section{Alimentation} \label{S2-6-alimentation}

Étant des systèmes électriques, les robots requièrent un sous-système responsable de l'alimentation électriques. Le système d'alimentation doit être autosuffisant et doit être conforme aux règlements de la compétition. La liste de contraintes du tableau~\ref{table:S2-6-contraintes-regles} fut établie en fonction de ces règlements (annexe~\ref{appendix:regles}).

\begin{table}[H]
	\caption{Contraintes provenant des règles de la compétition relatives au système d'alimentation (annexe~\ref{appendix:regles})}
	\label{table:S2-6-contraintes-regles}
	\centering
	\begin{tabularx}{\textwidth}{lX}
		\toprule
		\textbf{Section} & \textbf{Contrainte} \\
		\midrule
		F.3. & Seules des batteries non modifiées peuvent être utilisées. \\
		F.3. & Les équipes doivent être en mesure de jouer trois matchs de suite. \\
		F.3. & Les batteries doivent être en permanence dans des sacs ignifuges certifiés et non modifiés. \\
		F.4. & Bouton d'arrêt d'urgence de robots autonomes. L'appui sur ce bouton doit provoquer l'arrêt immédiat de tous les actionneurs du robot. \\
		F.5.a & Les tensions embarquées ne doivent  pas dépasser $\SI{48}{\volt}$. \\
		\bottomrule
	\end{tabularx}
\end{table}

En somme, les robots devraient être alimentés par des batteries de tension inférieure à $\SI{48}{\volt}$ non modifiées dans des sacs ignifuges. Le système d'alimentation doit être en mesure d'alimenter le robot pour trois matchs consécutifs. Le bouton d'arrêt d'urgence doit être en mesure d'arrêter tous actionneurs du robot.

\subsection{Cahier des charges}

Avant de concevoir un circuit d'alimentation, il faut d'abord déterminer les exigences énergétiques des robots. Le tableau~\ref{table:S2-6-liste-composantes-batterie} liste les composantes qui seront alimentées directement de la batterie. Étant donné la large gamme de tensions d'alimentation du \textit{ODrive} (contrôleur de moteurs sélectionné à la section~\ref{S2-8-choix-moteurs}), il sera probablement possible de l'alimenter directement, selon le choix de batterie.

\begin{table}[H]
	\caption{Composantes électriques alimentées directement par la batterie}
	\label{table:S2-6-liste-composantes-batterie}
	\centering
	\begin{tabularx}{\textwidth}{Xlll}
		\toprule
		\textbf{Nom de la composante} & \textbf{Courant ($\si{\ampere}$)} & \textbf{Quantité} & \textbf{Courant total ($\si{\ampere}$)} \\
		\midrule
		\textit{ODrive} & $\num{20}$ \cite{odrive-docs} & 1 & $\num{20}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

Le tableau~\ref{table:S2-6-liste-composantes-12v} présente les composantes électriques qui seront alimentées à $\SI{12}{\volt}$. Ces composantes doivent alors être alimentées via un régulateur de tension (hacheur C.C.). Afin d'utiliser le même circuit d'alimentation pour les deux robots, il faut que la sortie $\SI{12}{\volt}$ puisse fournir au moins le courant que prends les composantes communes plus le courant maximal entre les composantes exclusives à un tel robot. Avec un facteur de sécurité de quinze pour cent, la capacité minimale du régulateur $\SI{12}{\volt}$ est alors $\SI{4.054}{\ampere}$.

\begin{table}[H]
	\caption{Composantes électriques alimentées à $\SI{12}{\volt}$}
	\label{table:S2-6-liste-composantes-12v}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}llll}
		\toprule
		\rowgroup{Nom de la composante} & \textbf{Courant ($\si{\ampere}$)} & \textbf{Quantité} & \textbf{Courant total ($\si{\ampere}$)} \\
		\midrule

		\rowgroup{Commun entre les deux robots} \\

		Raspberry Pi + Carte Fille & $\num{1.325}$ (section~\ref{S2-2-carte-fille}) & 1 & $\num{1.325}$ \\

		\midrule
		\rowgroup{Exclusif au robot primaire} \\

		Moteur C.C. avec balais & $\num{1.1}$ \cite{moteur} & 2 & $\num{2.2}$ \\

		\midrule
		\rowgroup{Exclusif au robot secondaire} \\

		Pompes à vide & $\num{0.25}$ \cite{pompe} & 8 & $\num{2}$ \\

		\midrule
		\hspace{-1em}Total & - & - & $\num{3.525}$ \\
		\rowgroup{Total + facteur de sécurité ($\SI{15}{\percent}$)} & - & - & $\num{4.054}$ \\

		\bottomrule
	\end{tabularx}
\end{table}

Le tableau~\ref{table:S2-6-liste-composantes-6v} présente les composantes alimentées à $\SI{6}{\volt}$. Encore une fois, un régulateur de tension doit être utilisé afin de fournir cette tension constante. Puisque le robot secondaire exige plus de courant à cette tension, il dicte la capacité minimale du régulateur. Avec un facteur de sécurité de quinze pour cent, le régulateur $\SI{6}{\volt}$ doit alors être en mesure de fournir au moins $\SI{10.35}{\ampere}$.

\begin{table}[H]
	\caption{Composantes électriques alimentées à $\SI{6}{\volt}$}
	\label{table:S2-6-liste-composantes-6v}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xlll}
		\toprule
		\rowgroup{Nom de la composante} & \textbf{Courant ($\si{\ampere}$)} & \textbf{Quantité} & \textbf{Courant total ($\si{\ampere}$)} \\

		\midrule
		\rowgroup{Exclusif au robot primaire} \\

		Servomoteur \textquote{micro} & $\num{1}$ \cite{micro-servo} & 2 & $\num{2}$ \\

		\midrule
		\rowgroup{Exclusif au robot secondaire} \\

		Servomoteur \textquote{micro} & $\num{1}$ \cite{micro-servo} & 4 & $\num{4}$ \\
		Servomoteur & $\num{1}$ & 5 & $\num{5}$ \\

		\midrule
		\hspace{-1em}Total & - & - & $\num{9}$ \\
		\rowgroup{Total + facteur de sécurité ($\SI{15}{\percent}$)} & - & - & $\num{10.35}$ \\

		\bottomrule
	\end{tabularx}
\end{table}

Avec ces courants totaux calculés, il fut possible d'établir le cahier des charges pour le système d'alimentation du tableau~\ref{table:S2-6-cahier-des-charges}. Vu que la fonction principale du système d'alimentation est de fournir les sources d'énergies à des tensions précises, une emphase est mise sur la section \textquote{régulation de tension}. Cette section spécifie que les sorties $\SI{12}{\volt}$ et $\SI{6}{\volt}$ devraient être précises et efficaces et doivent fournir les courants requis par les systèmes du robot.

Une autre grande section est la protection et la vérification. La carte doit se protéger contre une surtension, un surcourant et une polarisation inverse. De plus, elle devrait vérifier la santé de la batterie. C'est-à-dire, elle assure que les cellules sont chargées de façon balancée. Le circuit devrait aussi afficher la tension de la batterie afin de montrer quand elle doit être échangée. Des indicateurs tels que des DELs seront incorporés dans le circuit afin de montrer l'état du système. Il serait possible d'utiliser une DEL pour l'état de l'arrêt d'urgence, une autre pour l'état de la batterie et une dernière pour l'état des régulateurs.

\begin{table}[H]
	\caption{Cahier des charges utilisé lors du choix de système d'alimentation}
	\label{table:S2-6-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xcccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		\rowgroup{Prix} & $\SI{15}{\percent}$ & \makecell{
			$\leq \SI{250}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{300}{\$}: \SI{50}{\percent}$ \\
			$> \SI{300}{\$}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Régulation de tension} & $\SI{50}{\percent}$ \\
		\midrule

		Erreur sur la sortie & $\SI{12.5}{\percent}$ & \makecell{
			$\leq \SI{1}{\percent}: \SI{100}{\percent}$ \\
			$\leq \SI{5}{\percent}: \SI{75}{\percent}$ \\
			$> \SI{5}{\percent}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Rendement du hachage & $\SI{12.5}{\percent}$ & \makecell{
			$\geq \SI{90}{\percent}: \SI{100}{\percent}$ \\
			$\geq \SI{75}{\percent}: \SI{75}{\percent}$ \\
			$< \SI{75}{\percent}: \SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Courant de la sortie $\SI{12}{\volt}$ & $\SI{12.5}{\percent}$ & - & $\SI{4.054}{\ampere}$ & - \\

		\midrule

		Courant de la sortie $\SI{6}{\volt}$ & $\SI{12.5}{\percent}$ & - & $\SI{10.35}{\ampere}$ & - \\

		\midrule
		\rowgroup{Protection / Vérification} & $\SI{20}{\percent}$ \\
		\midrule

		Protection surtension & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Protection surcourant & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Protection polarité inverse & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Vérification de la batterie & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Vérification des régulateurs & $\SI{4}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule
		\rowgroup{Rétroaction} & $\SI{10}{\percent}$ \\
		\midrule

		\makecell[l]{
			Affichage de la tension \\
			de la batterie \\
		} & $\SI{5}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Indicateurs de l'état du système & $\SI{5}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}

\subsection{Choix des composantes du système d'alimentation}

Avec le cahier des charges établie, les sous-sections suivantes justifieront les choix des composantes électriques utilisées dans le système d'alimentation.

\subsubsection{Choix de la batterie}

Au vu de la gamme de tension d'alimentation du \textit{ODrive} de $\SI{12}{\volt}$ à $\SI{24}{\volt}$ \cite{odrive-docs}, la tension de la batterie devrait tomber dans cette gamme. De plus, vu le régulateur de $\SI{12}{\volt}$, la tension de la batterie devrait être au moins quelques volts supérieur à cette tension afin de combattre la chute de tension du hacheur série. En utilisant une chute typique d'environ $\SI{2}{\volt}$, la tension de la batterie devrait être entre $\SI{14}{\volt}$ et $\SI{24}{\volt}$. Ainsi, la contrainte de $\SI{48}{\volt}$ du tableau~\ref{table:S2-6-contraintes-regles} n'est pas un problème.

En plus de ces contraintes sur la tension, la batterie devrait avoir une capacité suffisante pour alimenter le robot pour un match plein. Idéalement, elle alimenterait le robot pour jusqu'à trois matchs consécutifs sans exiger un changement de batterie.

La batterie utilisée dans les robots l'année dernière, le \textit{TURNIGY} 2650 mAh 4S, réponds au cahier des charges. Afin de réutiliser au maximum les composantes, aucunes alternatives furent considérées et cette batterie fut choisie comme source d'alimentation pour les robots. Un aperçu de la batterie est donné à la figure~\ref{fig:S2-6-lipo} et ses caractéristiques sont données au tableau~\ref{table:S2-6-lipo-caractéristiques}. La batterie a une capacité de $\SI{2.65}{\ampere\hour}$. De plus, sa tension nominale est de $\SI{14.8}{\volt}$, puisque sa configuration est de quatre cellules de $\SI{3.7}{\volt}$ chacune en série. Elle répond parfaitement au cahier des charges du tableau~\ref{table:S2-6-cahier-des-charges}.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{./figures/S2-sous-systèmes/S2-6-lipo.jpg}
	\caption{Batterie LiPo \textit{TURNIGY} 2650 mAh 4S \cite{lipo}}
	\label{fig:S2-6-lipo}
\end{figure}

\begin{table}[H]
	\caption{Caractéristiques de la \textit{TURNIGY} 2650 mAh 4S \cite{lipo}}
	\label{table:S2-6-lipo-caractéristiques}

	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Propriété} & \textbf{Valeur} \\
		\midrule
		Capacité & $\SI{2650}{\milli\ampere\hour}$ \\
		Configuration & 4S1P \\
		Tension nominale & $\SI{14.8}{\volt}$ \\
		Décharge constante & 20C \\
		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Choix des régulateurs de tension}

La batterie étant choisie, il est possible de calculer le courant d'entrée des régulateurs grâce à la formule de l'équation~\ref{eq:S2-6-calcul-courant-entrée}, où $I_{entr\acute{e}e}$ représente le courant entrant dans le régulateur, $I_{sortie}$ représente le courant sortant du régulateur, $V_{r\acute{e}g}$ la tension du régulateur et $V_{nom}$ la tension nominale de la batterie.

\begin{align}
	I_{entr\acute{e}e} &= \frac{V_{r\acute{e}g}}{V_{nom}} \cdot I_{sortie}
	\label{eq:S2-6-calcul-courant-entrée}
\end{align}

L'équation~\ref{eq:S2-6-calcul-courant-entrée} est utilisée pour calculer les courants d'entrée des régulateurs de $\SI{12}{\volt}$ et de $\SI{6}{\volt}$ en fonction de leurs courants de sortie. Ces valeurs sont données au tableau~\ref{table:S2-6-courants-sortant}. Il est à noter que ce calcul suppose naïvement que le régulateur a un rendement unitaire. Cependant, ces valeurs donnent l'ordre des courants requis et permettront de choisir un régulateur. Une fois cette composante déterminé, il serait possible de calculer des courant réalistes en utilisant le rendement donné.

\begin{table}[H]
	\caption{Courants entrants en fonction des courants sortants des régulateurs de tension}
	\label{table:S2-6-courants-sortant}
	\begin{tabularx}{\textwidth}{lXX}
		\toprule
		\textbf{Tension ($\si{\volt}$)} & \textbf{Courant à tension sortie ($\si{\ampere}$)} & \textbf{Courant à tension d'entrée ($\si{\ampere}$)} \\
		\midrule
		12 & $\num{4.054}$ & $\num{3.287}$ \\
		6 & $\num{10.35}$ & $\num{4.196}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

L'année passée, trois régulateurs furent utilisés: le \textit{D24V50F5}, le \textit{D24V150F6} et le \textit{D24V150F12}. Tous les trois sont des produits de \textit{Pololu}. Les propriétés de ces régulateurs sont donnés au tableau~\ref{table:S2-6-regulateurs-Pololu}.

\begin{table}[H]
	\caption{Caractéristiques des régulateurs de tension \textit{Pololu} \cite{Pololu-d24v50f5} \cite{Pololu-d24v150f6} \cite{Pololu-d24v150f12}}
	\label{table:S2-6-regulateurs-Pololu}
	\begin{tabularx}{\textwidth}{Xlll}
		\toprule
		\textbf{Régulateur} & \textbf{Tension de sortie} & \textbf{Courant de sortie} & \textbf{Rendement typique} \\
		\midrule
		\textit{D24V50F5} & $\SI{5}{\volt} \pm \SI{4}{\percent}$ & $\SI{5}{\ampere}$ & $\SI{85}{\percent}$ à $\SI{95}{\percent}$ \\
		\textit{D24V150F6} & $\SI{6}{\volt} \pm \SI{4}{\percent}$ & $\SI{15}{\ampere}$ & $\SI{80}{\percent}$ à $\SI{95}{\percent}$ \\
		\textit{D24V150F12} & $\SI{12}{\volt} \pm \SI{4}{\percent}$ & $\SI{15}{\ampere}$ & $\SI{80}{\percent}$ à $\SI{95}{\percent}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

Cette année, il fut décidé de ne pas utiliser le régulateur de $\SI{5}{\volt}$. Au contraire, les microcontrôleurs à $\SI{5}{\volt}$ seront alimentés via un régulateur sur leur carte mère qui sera branché à la source d'alimentation de $\SI{12}{\volt}$. Cette approche permet de simplifier le système et de positionner les composantes protectrices (fusibles, diodes) avant la régulation de $\SI{5}{\volt}$, ce qui enlève la chute de tension associée à la sortie. Ce fut un problème l'an dernier: le fait d'avoir des éléments protecteurs entre le régulateur et la Pi alimentait ce dernier sous sa tension nominale.

Puisque les régulateurs \textit{D24V150F6} et \textit{D24V150F12} répondent au cahier des charges du tableau~\ref{table:S2-6-cahier-des-charges}, ces deux furent sélectionnés respectivement pour le régulateur $\SI{6}{\volt}$ et $\SI{12}{\volt}$ sans la nécessité de considérer des alternatives. Ce choix maximisera la réutilisation des composantes. Un aperçu du régulateur \textit{D24V150F12} est présenté à la figure~\ref{fig:S2-6-D24V150F12}. Le régulateur \textit{D24V150F6} n'est pas montré parce que sa forme est presque identique.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-6-D24V150F12.jpg}
	\caption{Régulateur de tension \textit{Pololu D24V150F12}~\cite{Pololu-d24v150f12}}
	\label{fig:S2-6-D24V150F12}
\end{figure}

Avec les régulateurs déterminés, il est possible de refaire les calculs du tableau~\ref{table:S2-6-courants-sortant} avec les rendements réels. Les résultats de ces calculs sont présentés au tableau~\ref{table:S2-6-courant-max}. Ces valeurs sont calculées en utilisant le pire cas pour le rendement: $\SI{80}{\percent}$. En ajoutant le courant que l'\textit{ODrive} prend directement de la batterie, il est possible de calculer un courant maximal de $\SI{29.35}{\ampere}$.

\begin{table}[H]
	\caption{Courant maximal que la batterie doit être en mesure de fournir}
	\label{table:S2-6-courant-max}
	\begin{tabularx}{\textwidth}{XX}
		\toprule
		\textbf{Tension} & \textbf{Courant requis de la batterie} \\
		\midrule
		12 & $\SI{4.109}{\ampere}$ \\
		6 & $\SI{5.245}{\ampere}$ \\
		Batterie & $\SI{20}{\ampere}$ \\
		\midrule
		\textbf{Total} & $\SI{29.35}{\ampere}$ \\
		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Choix de la stratégie d'alimentation} \label{S2-6-choix-stratégie}

Avec les composantes critiques (la batterie et les régulateurs) choisies, il fut possible d'établir une stratégie pour le circuit d'alimentation. Vu les fonctionnalités voulues pour le système (tableau~\ref{table:S2-6-cahier-des-charges}), il fut décidé d'implémenter le système d'alimentation en tant que circuit imprimé personnalisé. Les régulateurs seront inclus comme cartes filles à ce circuit.

Vu le grand nombre de fonctions voulues, il fut décidé lors d'un remue méninge de concevoir une carte numérique (c'est-à-dire, basée sur un microcontrôleur) plutôt qu'analogique.

La carte sera responsable de la protection surtension, surcourant et polarité inverse de la batterie. Elle sera également responsable de raccorder les régulateurs à la batterie ainsi que les sorties aux régulateurs appropriés. Le microcontrôleur sera en mesure d'interrompre les sorties dans le cas où la tension de la batterie est trop basse ou qu'un des régulateurs ne fonctionne pas correctement.

Le système gèrera aussi l'arrêt d'urgence, puisque la méthode le plus simple et le plus sure d'arrêter tous actionneurs est de couper leur alimentation. L'arrêt d'urgence sera l'entrée d'un autre étage de régulateurs, capable d'interrompre l'alimentation en cas d'urgence. Des sorties seront disponible entre les deux étages d'interrupteurs pour des systèmes qui ne sont pas dangereux et où des coupures abruptes de tension sont problématique. Notamment, pour la Pi. Finalement, le circuit aura un afficheur permettant l'indication de la tension de la batterie, ainsi que des DELs permettant l'indication de l'état du système.

\subsection{Échéancier}

Les tâches à faire pour le système d'alimentation sont principalement la conception du circuit imprimé et la programmation de son microcontrôleur. La tâche de conception se divise dans la conception des schémas, la conception de la carte, la commande des circuits et des composantes et l'assemblage (soudage) des circuits. Ces tâches sont représentées dans la forme d'un diagramme de Gantt à la figure~\ref{fig:S2-6-échéance}.

Il est possible de voir que la conception des schémas ainsi que de la carte sont complétées. L'arrivée des composantes électriques est toujours attendue. En effet, la livraison a été retardée par la pandémie COVID-19. Cela est détaillé à la section~\ref{S2-6-travail-commande-assemblage}.

Puisque l'arrivée des composantes électriques est prévue après la remise du projet, il est impossible que les cartes soient assemblées avant la date limite. La tâche d'assemblage fut ainsi annulée. Si les cartes ne sont pas complétées, il n'y a rien à programmer. La tâche de programmation n'était pas alors une priorité, surtout en raison de la difficulté de compléter la tâche avec les mesures de confinement et la fermeture du campus. La tâche de programmation fut également annulée. La justification est donnée à la section~\ref{S2-6-travail-programmation}.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-echeance.png}
	\caption{Diagramme de Gantt représentant l'échéancier des tâches pour le système d'alimentation}
	\label{fig:S2-6-échéance}
\end{figure}

\subsection{Travail accompli}

\input{sections/S2-Sous-systèmes/S2-6-alimentation/travail-schemas.tex}

\input{sections/S2-Sous-systèmes/S2-6-alimentation/travail-carte.tex}

\subsubsection{Commande des composantes et assemblage} \label{S2-6-travail-commande-assemblage}

Les circuits imprimés conçus à la section~\ref{S2-6-travail-carte}  sont arrivés. Un aperçu des cartes est donné à la figure~\ref{fig:S2-6-travail-pcbway}. Tout comme pour la carte fille de la Pi (section~\ref{S2-2-carte-fille}), elles étaient commandées auprès de \textit{PCBWay}, une compagnie de circuits imprimés qui commandite le Groupe de Robotique de l'Université de Moncton.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-6-pcbway.jpg}
	\caption{Circuit imprimé du sous-système d'alimentation réalisé par \textit{PCBWay}}
	\label{fig:S2-6-travail-pcbway}
\end{figure}

Pour les mêmes raisons que pour la carte fille de la Pi (section~\ref{S2-2-travail-fille-assemblage}), les composantes électriques commandées de \textit{Digi-Key} n'ont jamais arrivées. La livraison était retardée en raison de la pandémie COVID-19. Il fut ainsi impossible d'assembler et de souder la carte. Plus de renseignements concernant les mesures d'adaptation prises face à la pandémie sont disponibles à la section~\ref{S5-COVID}.

\subsubsection{Programmation} \label{S2-6-travail-programmation}

Avec l'impossibilité d'assembler la carte d'alimentation, il n'y a rien à programmer. Il serait donc inutile de développer un micrologiciel qui ne pourrait jamais être exécuté avant la limite du projet. De plus, les mesures de confinement et la fermeture du campus afin d'éviter la propagation de la COVID-19, la collaboration est limitée et le partage des composantes électriques nécessaire pour le développement du programme est dissuadé. La création d'un tel programme serait difficile en isolation. Vu la basse priorité de la tâche et la difficulté de sa mise en œuvre, la tâche programmation est aussi annulée. Des détails supplémentaires concernant les mesures d'adaptation prises face à la pandémie sont disponibles à la section~\ref{S5-COVID}.
