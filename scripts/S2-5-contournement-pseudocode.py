def developper_chemin(chemin, obstacles):

    # Itérer chaque paire de points dans le chemin
    for A, B in en_tant_que_paires_de_points(chemin):

        # Vérifier tous les obstacles
        for obstacle in obstacles:

            # Si l'obstacle bloque le segment de chemin A à B,
            # retourner le résultat de l'appel récursif qui
            # contourne au dessus l'obstacle ainsi que celui
            # qui contourne en dessous
            if obstacle.bloque(A, B):
                return [
                    developper_chemin(
                        # Ajouter un point pour contourner
                        # l'obstacle au dessus
                        chemin + contourner_au_dessus(obstacle),
                        # Enlever l'obstacle
                        # Il est déjà considéré
                        obstacles - obstacle,
                    ),
                    developper_chemin(
                        # Ajouter un point pour contourner
                        # l'obstacle en dessous
                        chemin + contourner_en_dessous(obstacle),
                        # Enlever l'obstacle
                        # Il est déjà considéré
                        obstacles - obstacle,
                    ),
                ]

    # Si aucun obstacle bloquant n'est trouvé, le chemin n'est pas
    # bloqué
    return chemin


developper_chemin([origine, destination], obstacles)
