def _actualiser_controle_vitesse(self):

    # Mettre à jour le positionnement
    pos.update()

    # Calculer l'erreur sur l'angle
    erreur_angle = self._normaliser_angle(
        pos.coord.angle_to(self._destination) - pos.a,
    )

    # Calculer l'erreur sur la distance
    erruer_distance = (self._destination - pos).hypot()

    # Inverser l'angle et la distance si la destination se
    # trouve en arrière du robot
    if abs(erreur_angle) > math.pi / 2:
        erreur_angle = \
            self._normaliser_angle(erreur_angle - math.pi)
        erruer_distance *= -1

    # Régulation de la vitesse angulaire
    omega = self._pid_omega(-erreur_angle)

    # Rotation "pure" si erreur sur l'angle trop grande
    v = 0 if erreur_angle > SEUIL_ROTATION_PURE else \
        self._pid_v(-erruer_distance)

    # Calcul des vitesses des roues
    v_d = (2 * v + L * omega) / (2 * R)
    v_g = (2 * v - L * omega) / (2 * R)

    # Modification de la vitesse de l'ODrive
    odrive.axis0.controller.vel_setpoint = v_d * \
        self._IMPULSIONS_PAR_RADIAN

    odrive.axis1.controller.vel_setpoint = v_g * \
        self._IMPULSIONS_PAR_RADIAN
