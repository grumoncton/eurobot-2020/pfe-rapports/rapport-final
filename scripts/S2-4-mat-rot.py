import cv2
import numpy as np
import matplotlib.pyplot as plt


def main():
    # Récupération de l'image
    nom_fichier = "image.png"
    img = cv2.imread(nom_fichier, cv2.IMREAD_COLOR)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # ROTATION
    # Taille de l'image
    rows, columns, channels = img.shape

    # Matrice de transformation
    # Rotation de theta degrés autour du pt (0,0)
    # Échelle : 1
    R = cv2.getRotationMatrix2D((0, 0), theta, 1)
    rotation = cv2.warpAffine(img, R, (columns, rows))

    titres = ['Original', 'Rotation']
    images = [img, rotation]

    for i in range(len(titres)):
        plt.subplot(1, len(titres), i + 1)
        plt.imshow(images[i])
        plt.title(titres[i])
        plt.xticks([])
        plt.yticks([])

    plt.show()


if __name__ == "__main__":
    main()
