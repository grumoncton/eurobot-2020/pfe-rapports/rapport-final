with open(__file__.replace('.py', '.dat'), 'w') as f:
    v_0 = 1000
    a = -1000
    T = -v_0 / a
    d = v_0 * T + a * T**2 / 2
    E = 120
    D = 100
    L = 246.35
    l = 180

    x_e = 0
    y_e = d / 2
    r_y = d / 2 + E
    r_x = L / 2 + E
    r_y_slow = r_y + D
    r_x_slow = r_x + D

    for name in ('L', 'l', 'x_e', 'y_e', 'r_y', 'r_x', 'r_x_slow', 'r_y_slow'):
        print(f'{name} = {locals()[name]/50}', file=f)
