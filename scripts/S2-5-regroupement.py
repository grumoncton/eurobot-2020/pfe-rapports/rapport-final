def regrouper(points):

    # Initialement, nous avons un groupe avec seulement le
    # premier point
    groupes = [[points[0]]]

    # Distance maximale où les points seront regroupés
    # Diamètre des supports de balises embarquées
    DIST_REGROUPEMENT = 70

    # Les angles entre les points sont contigus
    # Il suffit de vérifier les points adjacents
    for precedent, actuel in en_tant_que_pairs(points):

        # Ajouter un groupe si la distance est trop grande
        if math.hypot(
            precedent.x - actuel.x,
            precedent.y - actuel.y,
        ) < DIST_REGROUPEMENT:
            groupes.append([])

        # Ajouter le point actuel au dernier groupe
        groupes[-1].append(actuel)

    # Prendre la moyenne des valeurs de x et de y
    for groupe in groupes:
        x = 0
        y = 0

        for point in groupe:
            x += point.x
            y += point.y

        yield Cercle(
            x=x / len(groupe),
            y=y / len(groupe),
            r=RAYON_ADVERSAIRE,
        )
