% vim: ft=tex spelllang=fr

\subsubsection{Contrôle de vitesse}

La stratégie de régulation de $v$ et $\omega$ de la figure~\ref{fig:S2-8-vc-regulation} a été mise en œuvre dans une méthode de la classe \textit{Python} du système d'entraînement. Le contenu de la méthode est donné à la figure~\ref{fig:S2-8-velocity-control-update-initial}. La méthode est périodiquement exécutée, grâce à la librairie \code{sched}, une librairie d'ordonnancement \textit{Python}~\cite{sched}.

Prime abord, la méthode actualise la position du robot avec un appel à la classe du sous système de positionnement. Par après, l'erreur sur l'angle est calculée avec la l'équation~\ref{eq:S2-8-vc-theta_e}. Une méthode \code{_normaliser_angle} est utilisée afin d'assurer que l'angle soit dans la gamme $\left[\pi, -\pi\right[$. L'erreur sur la distance est aussi calculée grâce à l'expression de l'équation~\ref{eq:S2-8-vc-d}.

Si la destination se trouve derrière le robot (si l'erreur sur l'angle est supérieure à \SI{90}{\degree}), l'erreur sur l'angle ainsi que l'erreur sur la distance sont inversées.

Ensuite, les erreurs sont utilisées pour calculer les vitesses du modèle de monocycle: $v$ et $\omega$. La librairie \code{simple_pid}~\cite{simple-pid} est utilisée pour les régulateurs PID. Une rotation \textquote{pure} (sans vitesse linéaire) est effectuée si l'erreur sur l'angle est supérieure à un certain seuil, afin d'éviter que le robot avance quand il est orienté dans la mauvaise direction. Il est à noter que les vitesses sont limitées grâce à un paramètre de la librairie PID.

Finalement, les vitesses des roues sont calculées grâce à l'équation~\ref{eq:S2-8-vc-vr-vl}. Ces valeurs sont envoyées à l'\textit{ODrive}.

\begin{figure}[H]
	\pythonscript{./scripts/S2-8-velocity-control-update-initial.py}
	\mintedspace
	\caption{Code \textit{Python} de la méthode d'actualisation de contrôle de vitesse}
	\label{fig:S2-8-velocity-control-update-initial}
\end{figure}

\paragraph{Limite sur l'accélération}

Puisque les robots ne sont pas achevés, l'asservissement fut testé sur le robot secondaire de la compétition \textit{Eurobot 2019}. En effet, ce robot possède les mêmes moteurs, les mêmes encodeurs et le même contrôleur de moteur. Il faudrait évidemment changer les configurations légèrement sur les robots finaux, mais ce robot présente un bon modèle pour les tests.

Il fut remarqué que lorsque le robot accélère trop rapidement, il a tendance à dévier de sa cible. Ainsi, l'accélération linéaire et angulaire ont été limitées avec la classe \textit{Python} de la figure~\ref{fig:S2-8-velocity-control-change-limiter}. Seulement les accélérations positives (c'est-à-dire, pas les décélérations) sont limitées. Si les décélérations sont limitées, le robot dépasse sa cible.

\begin{figure}[H]
	\pythonscript{./scripts/S2-8-velocity-control-change-limiter.py}
	\mintedspace
	\caption{Classe \textit{Python} qui sert à limiter une accélération}
	\label{fig:S2-8-velocity-control-change-limiter}
\end{figure}

Maintenant que le robot débute son trajet plus lentement, l'angle ne s'éloigne pas autant et la réponse est drastiquement améliorée.

\paragraph{Interpolation entre les angles pour terminer de façon stable}

Asservi de cette façon, le robot était en mesure de se déplacer à un point destination quelconque. Par contre, lorsque la position actuelle et la position cible se rapprochent, l'erreur sur l'angle augmente rapidement. Par exemple, si l'erreur latérale est seulement \SI{0.1}{\mm}, lorsque l'erreur sur la distance tend vers zéro, l'erreur sur l'angle tend vers \SI{90}{\degree}, tel que montré à l'équation~\ref{eq:S2-1-8-travail-vc-erreur-angle}.

\begin{align}
	\theta_e &= \tan^{-1}\left(\frac{y_d - y}{x_d - x}\right) - \theta \\
	\theta_e &= \tan^{-1}\left(\frac{\SI{0.1}{\mm}}{x_d - x}\right) - 0 \\
	\lim_{x \to x_d} \theta_e &= \SI{90}{\degree} \label{eq:S2-1-8-travail-vc-erreur-angle}
\end{align}

La conséquence est qu'arrive à sa destination, le robot tourne continuellement en essayant d'atteindre le point directement à son côté. La solution mise en œuvre fut d'interpoler entre deux angles: la position actuelle à la position cible (équation~\ref{eq:S2-8-vc-d}) et la position originale à la position cible. L'expression de cette deuxième erreur angulaire est donnée à l'équation~\ref{eq:S2-8-travail-vc-erreur-angle-prime}, où l'origine se trouve au point $(x_o, y_o)$. En échangeant $\theta_e$ par $\theta_e^\prime$ en proximité de la destination, l'angle entre l'origine et la destination devient l'angle cible final, ce qui est logique. Cette erreur angulaire stable (indépendante de $x$ et $y$) permet d'atteindre la destination sans tourner en rond.

\begin{align}
	\theta_e^\prime &= \tan^{-1}\left(\frac{y_d - y_o}{x_d - x_o}\right) - \theta \label{eq:S2-8-travail-vc-erreur-angle-prime}
\end{align}

\paragraph{Essais et résultats}

Avec un asservissement qui atteint un point final stable, il fut possible d'ajuster les paramètres des régulateurs afin d'améliorer la réponse et de réduire le dépassement. Il serait possible de déterminer les fonctions de transfert pour chaque composante et de concevoir un régulateur de façon analytique. Par contre, il serait très difficile de trouver les modèles pour la majorité des systèmes. En effet, le positionnement prend place sur un microcontrôleur à part qui intercepte les impulsions des encodeurs, fait des calculs d'odométrie et envoie des données sur le bus \ItwoC. Il faudrait analyser chaque étape, ainsi que la réponse de l'\textit{ODrive}. Calculer les paramètres des régulateurs de façon analytique serait chronophage et ne produirait pas forcément un asservissement plus performant que la méthode par essais et erreurs.

Il est impossible de configurer un régulateur aveugle. Ainsi, un système a été développé pour donner une rétroaction sur plusieurs variables de contrôle. Il fut alors possible de créer des graphiques des erreurs, des variables de commande et de la position. Ces figures, donnant une rétroaction immédiate aux changements au code, ont permis d'augmenter la rapidité de la réponse et de minimiser le dépassement.

\subparagraph{Asservissement de l'angle}

Avant de tester le système entier, l'asservissement de l'angle a d'abord été testé. En effet, plusieurs tests ont été effectués où le robot était chargé d'effectuer une rotation sur lui même. Ces tests ont permis de trouver des paramètres satisfaisants du régulateur de vitesse angulaire.

La figure~\ref{fig:S2-8-travail-vc-essai-angle} montre la réponse lorsque le robot est chargé d'une rotation de \SI{180}{\degree}. Il est possible de voir que l'erreur décroît rapidement et se stabilise à zéro après environ une seconde. Seulement un petit dépassement est présent. Il est aussi visible que la vitesse angulaire du robot est initialement limitée. En effet, des contraintes sont en place afin d'assurer que le robot est commandé à des vitesses sécuritaires et stables.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{./figures/S2-sous-systèmes/S2-8-velocity-control-heading-angle.png}
	\caption{Réponse de l'angle pour un essai rotatif du système d'entraînement}
	\label{fig:S2-8-travail-vc-essai-angle}
\end{figure}

\subparagraph{Asservissement de la position}

L'asservissement de l'angle étant satisfaisant, il fut possible d'ajouter la vitesse linéaire. En effet, suivant les essais de rotation, un grand nombre de tests de déplacement ont été effectués. Un déplacement linéaire dépend des deux régulateurs: le régulateur de vitesse linéaire est utilisé afin de se déplacer vers la cible et le régulateur de la vitesse angulaire est utilisé afin de corriger l'angle du robot en chemin.

Les résultats d'un essai de déplacement sont donnés aux figures suivantes. Le robot était chargé d'un déplacement linéaire de \SI{600}{\mm}. La figure~\ref{fig:S2-8-travail-vc-essai-lineaire} présente les variables de sorties des régulateurs en fonction des variables d'entrées tandis que la figure~\ref{fig:S2-8-travail-vc-essai-lineaire-xy} montre la position deux-dimensionnelle du robot au cours de son déplacement.

La figure~\ref{fig:S2-8-travail-vc-essai-distance} montre comment la distance du point cible décroît au cours du temps et comment la vitesse de commande est affectée par cette distance. Initialement, l'erreur sur la distance est de \SI{600}{\mm}, ce qui est logique pour un déplacement de cette distance. La vitesse croît linéairement (accélération constante limitée de \SI{200}{\mm\per\second\squared}) jusqu'à une vitesse maximale de \SI{120}{\mm\per\second}. À cette vitesse limitée constante, la distance décroît de façon linéaire. Enfin, lorsque la distance s'approche de zéro, la sortie du régulateur n'est plus limitée et la vitesse décroît afin de décélérer le robot jusqu'à sa cible sans dépassement. Dans cette région, la vitesse est environ proportionnelle à la distance (pas toute à fait proportionnelle puisque la constante $k_d$ du régulateur est non nulle).

La vitesse angulaire et l'erreur angulaire sont présentées à la figure~\ref{fig:S2-8-travail-vc-essai-heading}. Il est possible de voir des oscillations sur l'erreur angulaire. Par contre, ce n'est pas un problème puisque les oscillations n'apparaissent pas dans le graphe de la position de la figure~\ref{fig:S2-8-travail-vc-essai-lineaire-xy} et puisqu'elles sont petites. En effet, l'erreur sur l'angle ne dépasse jamais $\pm\SI{1}{\degree}$.

La figure~\ref{fig:S2-8-travail-vc-essai-lineaire-xy} montre le trajet du robot dans le plan x-y. Il est possible de remarquer que le robot s'éloigne de son chemin d'environ \SI{1.2}{\mm} avant que l'asservissement de l'angle le corrige. De plus, il atteint sa position cible sur l'axe des $x$ parfaitement. Son erreur latérale (axe des $y$) est très petite aussi ($<\SI{0.1}{\mm}$). Ces résultats sont très satisfaisants.

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.9\linewidth}
		\includegraphics[width=\linewidth]{./figures/S2-sous-systèmes/S2-8-velocity-control-distance.png}
		\caption{Erreur sur la distance et vitesse linéaire}
		\label{fig:S2-8-travail-vc-essai-distance}
	\end{subfigure}
	\begin{subfigure}[b]{0.9\linewidth}
		\includegraphics[width=\linewidth]{./figures/S2-sous-systèmes/S2-8-velocity-control-heading.png}
		\caption{Erreur sur l'angle et vitesse angulaire}
		\label{fig:S2-8-travail-vc-essai-heading}
	\end{subfigure}
	\caption{Réponse de la distance et de l'angle pour un essai de déplacement linéaire du système d'entraînement}
	\label{fig:S2-8-travail-vc-essai-lineaire}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{./figures/S2-sous-systèmes/S2-8-velocity-control-xy.png}
	\mintedspace
	\caption{Position deux-dimensionnelle du robot lors de l'essai de déplacement linéaire du système d'entraînement}
	\label{fig:S2-8-travail-vc-essai-lineaire-xy}
\end{figure}
