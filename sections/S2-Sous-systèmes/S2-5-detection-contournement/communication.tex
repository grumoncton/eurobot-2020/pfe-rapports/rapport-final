% vim: ft=tex spelllang=fr

\subsubsection{Communication avec les autres systèmes}

Le système de détection est maintenant en mesure de déterminer si un obstacle est dans le chemin. Par contre, présentement, cette information n'est pas utilisée ailleurs dans le robot. En effet, le système d'entraînement doit être mis au courant de la présence d'adversaires dans le chemin, afin de freiner à temps.

Le système de détection, ainsi que le système d'entraînement, sont implémentés dans deux boucles infinies dans deux files d'exécution indépendantes. La détection communique ses blocages via un \code{threading.Event}, une classe représentant des évènements dans la librairie \code{threading} de \textit{Python}~\cite{threading}. La classe qui met en œuvre la détection possède un attribut \code{event}, une instance de \code{threading.Event}. L'évènement est activé (avec la méthode \code{.set()}) lorsque le robot est bloqué. Cette condition est lue continuellement dans la boucle principale du système d'entraînement, avec la méthode \code{.is_set()}.

Les organigrammes qui modélisent les deux systèmes sont donnés à la figure~\ref{fig:S2-5-detection-communication}. Ce sont des diagrammes simplifiés qui montrent uniquement les aspects qui concernent la communication. La figure~\ref{fig:S2-5-detection-communication-lidar} montre comment le système de détection traite continuellement les données du LiDAR. Si un obstacle est présent, il exécute \code{.set()} sur l'évènement et ajoute les obstacles détectés au système de recherche de chemin. L'entraînement, modélisé par l'organigramme de la figure~\ref{fig:S2-5-detection-communication-entrainement}, vérifie continuellement si l'évènement est activé. Si oui, le robot freine immédiatement. Il reste immobile jusqu'au moment où l'évènement n'est plus activé (le robot n'est plus bloqué). Il exécute l'algorithme de recherche de chemin avec les nouveaux obstacles et suit le chemin résultant. Il est à noter que le robot ne restera pas immobile jusqu'au moment où le robot adverse se déplace. En effet, les zones de détection sont proportionnelles à la vitesse du robot. S'il est immobile, l'obstacle qui l'a fait freiner ne sera plus dans sa zone de détection. Le test est simplement pour assurer que la détection a eu le temps de traiter tous les points bloquants et à ajouter les obstacles au chercheur de chemin.

\begin{figure}[H]
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\begin{tikzpicture}[scale=0.8, every node/.style={transform shape}, node distance=2cm]
			\node (start) [startstop] {Début};

			\node (scan) [process, below of=start, text width=5cm] {Lire les points d'une révolution du LiDAR};
			\draw [arrow] (start) -- (scan);

			\node (blocked) [decision, below of=scan, yshift=-2cm] {Obstacle dans l'ellipse?};
			\draw [arrow] (scan) -- (blocked);

			\draw [arrow] (blocked) -- node [anchor=south] {Non} ++(-4,0) |- (scan);

			\node (event-set) [process, below of=blocked, yshift=-2cm] {\code[]{event.set()}};
			\draw [arrow] (blocked) -- node [anchor=east] {Oui} (event-set);

			\node (add) [process, below of=event-set, text width=5cm] {Ajouter les obstacles détectés au chercheur de chemin};
			\draw [arrow] (event-set) -- (add);

			\draw [arrow] (add) -- ++(4,0) |- (scan);

		\end{tikzpicture}

		\caption{Système de détection}
		\label{fig:S2-5-detection-communication-lidar}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\centering
		\begin{tikzpicture}[scale=0.8, every node/.style={transform shape}, node distance=2cm]
			\node (start) [startstop] {Début};

			\node (isset) [decision, below of=start, yshift=-2cm] {\code[]{event.is_set()}};
			\draw [arrow] (start) -- (isset);

			\draw [arrow] (isset) -- node [anchor=south] {Faux} ++(-4,0) -- ++(0,3) -| (isset);

			\node (stop) [process, below of=isset, yshift=-2cm] {Arrêter le mouvement};
			\draw [arrow] (isset) -- node [anchor=east] {Vrai} (stop);

			\node (stillset) [decision, below of=stop, yshift=-2cm] {\code[]{event.is_set()}};
			\draw [arrow] (stop) -- (stillset);

			\draw [arrow] (stillset) -- node [anchor=south] {Vrai} ++(-4,0) -- ++(0,3) -| (stillset);

			\node (pathfinding) [process, below of=stillset, yshift=-2cm, text width=4.5cm] {Exécuter le recherche de chemin avec les nouveaux obstacles};

			\draw [arrow] (stillset) -- node [anchor=east] {Faux} (pathfinding);

			\node (chemin) [process, below of=pathfinding] {Prendre le nouveau chemin};
			\draw [arrow] (pathfinding) -- (chemin);

			\draw [arrow] (chemin) -- ++(4,0) |- (isset);

		\end{tikzpicture}

		\caption{Système d'entraînement}
		\label{fig:S2-5-detection-communication-entrainement}
	\end{subfigure}

	\caption{Organigrammes de l'interaction entre les systèmes de détection et d'entraînement}
	\label{fig:S2-5-detection-communication}
\end{figure}

Le système de détection communique également avec l'algorithme de recherche de chemin. En effet, ce dernier doit être au courant des obstacles qui pourraient être dans le chemin. L'étape \textquote{Ajouter les obstacles détectés au chercheur de chemin} consiste à regrouper les points qui proviennent du même obstacle et à ajouter un cercle centré à la position moyenne des points et de rayon du robot adverse. Puisque le LiDAR ne peut pas distinguer entre les robots, le plus grand rayon entre les deux robots de l'équipe adverse doit être utilisé.

Le regroupement des points et la génération des obstacles sont mis en œuvre par une fonction génératrice (ces fonctions sont expliquées à la section~\ref{S2-5-concept-final-filtre}). La fonction prend comme argument une liste de points. Elle traite les points en paires (précèdent, actuel) de manière itérative. Si la distance entre le point actuel et le point précèdent est plus qu'un certain seuil, le point actuel est mis dans le même groupe. Sinon, un nouveau groupe est créé.

La prochaine étape est de trouver les moyennes des positions de tous les points dans chaque groupe. Pour chaque groupe, la somme des valeurs de $x$ et de $y$ est calculée. Cette valeur est divisée par la taille du groupe afin de trouver la moyenne. Une instance de la classe \code{Cercle} est créée avec cette position moyenne et le rayon du plus gros robot adverse. Le cercle est retourné avec le mot clé \code{yield}, un mot clé spécial pour les fonctions génératrices qui permet de retourner plusieurs valeurs.

\begin{figure}[H]
	\pythonscript{./scripts/S2-5-regroupement.py}
	\mintedspace
	\caption{Fonction génératrice regroupant les points en obstacles}
	\label{fig:S2-5-regroupement}
\end{figure}
