% vim: ft=tex spelllang=fr

\subsubsection{Nouvelles zones de détection elliptiques} \label{S2-5-concept-final-zones}

\DTLloaddb[noheader, keys={thekey,thevalue}]{S2-5-ellipse}{./data/S2-5-ellipse.dat}

La section~\ref{S2-5-à-faire-zones} clarifie pourquoi la zone de détection en forme de secteur circulaire n'est pas adéquate. Cette section propose d'utiliser une zone rectangulaire pour arrêter le robot et une zone elliptique plus large pour le ralentir. En effet, une version intermédiaire du code utilise une zone rectangulaire et une zone elliptique. Par contre, les avantages de l'ellipse pour la zone de ralentissement (plus d'importance aux extrémités gauche et droite pour des obstacles plus proches) s'appliquent également pour le freinage. Ainsi, la conception finale utilise deux zones elliptiques similaires.

L'utilisation de zones elliptiques exige un moyen de déterminer si un point se trouve dans la forme. L'inégalité de l'équation~\ref{eq:S2-5-test-ellipse} teste si le point $(x_p, y_p)$ se trouve dans l'ellipse centrée au point $(x_e, y_e)$ ayant les rayons $r_x$ et $r_y$. Si les deux termes sont égaux, le point se trouve sur la bordure de l'ellipse.

\begin{align}
	\frac{(x_p - x_e)^2}{r_x^2} + \frac{(y_p - y_e)^2}{r_y^2} \leq 1 \label{eq:S2-5-test-ellipse}
\end{align}

Dans le cas des robots, l'ellipse sera centrée horizontalement ($x_e = 0$) et sera devant le robot afin de compenser pour sa vitesse ($y_e > 0$). Encore en raison de la vitesse, l'ellipse sera plus longue que large ($r_y > r_x$). Toutes ces coordonnées sont relatives au centre du robot. La figure~\ref{figure:S2-5-zones-ellipse} montre toutes ces valeurs graphiquement et à des dimensions réalistes. L'origine $(0, 0)$ correspond au centre du robot.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\coordinate (e) at (0, 3);
		\draw (e) node[right] {$(x_e, y_e)$};

		\draw (e) ellipse (3cm and 6cm);
		\fill (e) circle (0.05);

		\draw (0, 0) node[left] {$(0, 0)$};
		\fill (0, 0) circle (0.05);
		\draw (0, -0.2) node[below] {Centre du robot};
		\draw[->] (0, 0) -- (1, 0) node[right] {$x$};
		\draw[->] (0, 0) -- (0, 1) node[right] {$y$};

		\draw[->] (e) -- node[above] {$r_x$} ++(-3cm,0);
		\draw[->] (e) -- node[left] {$r_y$} ++(0,6cm);

	\end{tikzpicture}
	\caption{Paramètres de l'ellipse utilisée comme zone de détection}
	\label{figure:S2-5-zones-ellipse}
\end{figure}

Pour une ellipse donnée, l'équation~\ref{eq:S2-5-test-ellipse} prend en entrée la position du point détecté, en coordonnées rectangulaires relatives au centre du robot. Il faut ainsi effectuer cette conversion. Les coordonnées polaires $(r, \phi)$ du point peuvent être converties en coordonnées rectangulaires grâce à l'équation~\ref{eq:S2-5-zones-pol-rect}. Cette équation est très similaire à l'équation~\ref{eq:S2-5-filtre-rect-rel}, mis à part l'angle. En effet, ici l'angle est relatif au robot tandis que pour le filtrage des faux positifs, il était relatif à la table.

\begin{align}
	x_p &= r \cdot \cos(\phi) & y_p &= r \cdot \sin(\phi)
	\label{eq:S2-5-zones-pol-rect}
\end{align}

Il est maintenant possible de déterminer si un point $(r, \phi)$ se situe dans une ellipse donnée. Cependant, il faut déterminer les paramètres des ellipses. La taille des ellipses devrait croître avec la vitesse du robot. En effet, se déplacer à des vitesses élevées implique des distances de freinage plus grandes. Il serait alors prudent de ralentir lorsqu'un obstacle est détecté à une distance plus éloignée. La taille de l'ellipse doit donc être proportionnelle à la distance de freinage.

La distance de freinage se calcule à partir de la vitesse linéaire initiale $v_0$ et de la décélération de freinage $a$. La vitesse instantanée durant le freinage est donnée par l'équation~\ref{S2-5-freinage-v(t)}. Le robot commence à freiner à $t=0$. En substituant $v(t)=0$ et en isolant $t$, il est possible de trouver le temps de freinage $T$ de l'équation~\ref{S2-5-freinage-T}. Finalement, la distance de freinage ($d$) est l'intégrale de la vitesse sur le temps de freinage, donnée à l'équation~\ref{S2-5-freinage-d}.

\begin{align}
	v(t) &= v_0 + a \cdot t \label{S2-5-freinage-v(t)} \\
	T &= \frac{-v_0}{a} \label{S2-5-freinage-T} \\
	d &= \int_0^T v(t) \cdot dt \\
	d &= \left[v_0 \cdot t - \frac{a}{2} \cdot t^2\right]_0^T \\
	d &= v_0 \cdot T - \frac{a}{2} \cdot T^2 \label{S2-5-freinage-d}
\end{align}

Il serait prudent de centrer l'ellipse sur le rectangle tracé par le robot lors d'un freinage (ce qui implique $y_e = d / 2$). Ainsi, l'importance des obstacles est symétrique par rapport à la distance de freinage. Ensuite, le rayon vertical devrait être le restant de la distance de freinage, plus un certain facteur de sécurité ($\epsilon$), alors que le rayon horizontal devrait être la moitié de la largeur du robot ($L$), plus ce même facteur. Les paramètres de l'ellipse de freinage sont donnés aux équations~\ref{eq:S2-5-ellipse-xe} à \ref{eq:S2-5-ellipse-ry}.

\begin{align}
	x_e &= 0 \label{eq:S2-5-ellipse-xe} \\
	y_e &= \frac{d}{2} \label{eq:S2-5-ellipse-ye} \\
	r_x &= \frac{L}{2} + \epsilon \label{eq:S2-5-ellipse-rx} \\
	r_y &= \frac{d}{2} + \epsilon \label{eq:S2-5-ellipse-ry}
\end{align}

La zone de ralentissement sera plus large d'un écart $D$. Les rayons de cette ellipse sont calculés à l'équation~\ref{eq:S2-5-zones-ellipse-ralentissement}.

\begin{align}
	r_{x_{ralentissement}} &= r_x + D & r_{y_{ralentissement}} &= r_y + D
	\label{eq:S2-5-zones-ellipse-ralentissement}
\end{align}

Pour une vitesse initiale de $\SI{1000}{\mm\per\second}$ et une décélération de freinage de $\SI{1000}{\mm\per\second^2}$, un aperçu de la zone de freinage est donnée à la figure~\ref{figure:S2-5-zones-ellipse-parametres}. Le robot est représenté par un rectangle centré à l'origine.

\DTLgetvalueforkey{\xe}{thevalue}{S2-5-ellipse}{thekey}{x_e}
\DTLgetvalueforkey{\ye}{thevalue}{S2-5-ellipse}{thekey}{y_e}
\DTLgetvalueforkey{\rx}{thevalue}{S2-5-ellipse}{thekey}{r_x}
\DTLgetvalueforkey{\ry}{thevalue}{S2-5-ellipse}{thekey}{r_y}
\DTLgetvalueforkey{\rxSlow}{thevalue}{S2-5-ellipse}{thekey}{r_x_slow}
\DTLgetvalueforkey{\rySlow}{thevalue}{S2-5-ellipse}{thekey}{r_y_slow}
\DTLgetvalueforkey{\L}{thevalue}{S2-5-ellipse}{thekey}{L}
\DTLgetvalueforkey{\l}{thevalue}{S2-5-ellipse}{thekey}{l}

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\coordinate (e) at (\xe cm, \ye);
		\draw (e) node[right] {$(x_e = \xe, y_e = \ye)$};

		\draw (e) ellipse ({\rx} and {\ry});
		\draw (e) ellipse ({\rxSlow} and {\rySlow});
		\fill (e) circle (0.05);

		% \draw (0, 0) node[left] {$(0, 0)$};
		\fill (0, 0) circle (0.05);
		% \draw (0, -0.2) node[below] {Centre du robot};
		\draw[->] (0, 0) -- (1, 0) node[right] {$x$};
		\draw[->] (0, 0) -- (0, 1) node[right] {$y$};

		\draw[->] (e) -- node[above] {$r_x$} ++(-\rx,0);
		\draw[->] (e) -- node[left] {$r_y$} ++(0,\ry);
		\draw[<->] (-\rx,\ye) -- node[above] {$D$} ++(-2,0);

		\node [draw, shape=rectangle, minimum width=\L cm, minimum height=\l cm, anchor=center] at (0,0) {};

	\end{tikzpicture}
	\caption{Paramètres de l'ellipse utilisée comme zone de détection}
	\label{figure:S2-5-zones-ellipse-parametres}
\end{figure}
