import socket


PORT = 8080


# Ouvrir un "socket"
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:

    # Écouter pour des connexions
    sock.bind(('0.0.0.0', PORT))
    sock.listen(5)

    # Continuellement accepter des clients
    while True:
        conn, addr = sock.accept()

        # Continuellement lire des données du client
        with conn:
            while True:
                data = conn.recv(4096)

                if not data:
                    break

                # Imprimer le message du client
                print(data)

                # Envoyer une reponse
                conn.send('Message du serveur')
