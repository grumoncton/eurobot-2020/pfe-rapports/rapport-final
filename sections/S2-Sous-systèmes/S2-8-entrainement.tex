% vim: ft=tex, spelllang=fr
% !TeX root = ../../scratchpadDamien.tex


\section{Entraînement} \label{S2-8-entrainement}

Le système d'entraînement joue un rôle très important dans le fonctionnement et l'efficacité du robot. En effet, c'est le système qui permet au robot de se déplacer pour se rendre aux tâches qui lui sont allouées.


\subsection{Choix du système d'entraînement}

Plusieurs systèmes d'entraînement existent et chacun utilise un type de roue différent. Cette section tente à déterminer le système d'entraînement le plus approprié pour déplacer les robots du GRUM de manière efficace.

\subsubsection{Cahier des charges}

Le tableau~\ref{table:S2-8-roues-cahier-des-charges} ci-dessous représente le cahier des charges du choix de système d'entraînement.

Pour la compétition \textit{Eurobot 2019}, le GRUM a utilisé un système d'entraînement complexe qui était difficile à configurer. Il fut ainsi décider de favoriser la simplicité du système et la facilité du contrôle, au lieu de sa performance potentielle. De cette façon, il serait possible de perfectionner l'entraînement et de le tester amplement.

Le coût est également un critère important pour le GRUM, vu les limitations de budget.

\begin{table}[H]
	\caption{Cahier des charges utilisé pour le choix des roues sur le système d'entraînement}
	\label{table:S2-8-roues-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xcccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		\rowgroup{Performance} & $\SI{40}{\percent}$ \\

		\midrule

		Polyvalence & $\SI{35}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Multidirectionnalité & $\SI{5}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Efficacité} & $\SI{40}{\percent}$ \\

		\midrule

		Simplicité & $\SI{25}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Contrôle & $\SI{15}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{50}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{100}{\$}: \SI{80}{\percent}$ \\
			$\leq \SI{200}{\$}: \SI{60}{\percent}$ \\
			$> \SI{200}{\$}: \SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Étude préliminaire}~\label{S2-8-systeme-entrainement-etude-preliminaire}

\paragraph{Entraînement mecanum}

La roue mecanum, aussi nommée roue omnidirectionnelle, est une roue qui permet à un véhicule terrestre de se déplacer dans n'importe quelle direction, que ce soit vers l'avant, vers l'arrière, vers le côté et en diagonale. Au lieu de pneus, cette roue est ceinte d'une série de petits rouleaux en caoutchouc fixés obliquement a \SI{45}{\degree} par rapport à la jante. Les rouleaux sont situés sur la circonférence externe de la jante, tel que présenté à la figure~\ref{fig:S2-8-roue-mecanum} ci-dessous.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-8-roue-mecanum.jpg}
	\caption{Exemple de roue mecanum~\cite{mecanum}}
	\label{fig:S2-8-roue-mecanum}
\end{figure}

Pour avoir un bon fonctionnement de ces roues, le véhicule doit avoir un minimum de 4 roues mecanums placées en position rectangulaire. Chacune des roues du véhicule est non-directionnelle et chaque roue est contrôlée par un moteur différent. Les roues mecanum étant indépendantes les unes des autres, un bon contrôle des moteurs permet de déplacer les robots dans une multitude de directions, comme démontré à la figure~\ref{fig:S2-8-roue-mecanum-directions}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/S2-sous-systèmes/S2-8-roues-mecanum-directions.jpg}
	\caption{Mouvements possibles avec des roues mecanum~\cite{spangler1988method}}
	\label{fig:S2-8-roue-mecanum-directions}
\end{figure}

La figure~\ref{fig:S2-8-roue-mecanum-directions} montre que plusieurs mouvements sont possibles en commandant les roues à des vitesses précises. Des mouvements diagonaux à des angles quelconques sont même possibles. De plus, il est possible de faire des déplacements linéaires tout en complétant une rotation du robot sur soi-même. Ces mouvements complexes servent à maximiser l'efficacité des déplacements et à économiser du temps.

Les avantages de ces mouvements complexes sont évidents. Par contre, un système d'entraînement mecanum est très difficile à programmer et à contrôler relativement à un système plus traditionnel. De plus, ce type de roue a tendance à glisser. Cela pose un grand problème vu qu'il devient difficile de récupérer des données de rétroaction sur la position. En effet, il est alors impossible d'utiliser des encodeurs sur des roues externes et de recourir à l'odométrie pour calculer la position du robot. Le positionnement était le plus gros problème l'année passée, lorsque le GRUM a utilisé un système d'entraînement mecanum sur un de ses robots.


\paragraph{Entraînement holonome}

La roue holonome, tout comme la roue mecanum, permet à un véhicule à se déplacer dans des directions quelconques. La roue holonome est physiquement semblable à la roue mecanum dans le sens qu'elle est composée d'une série de petits rouleaux en caoutchouc ou en plastique. Contrairement à la roue mecanum, les rouleaux de celle-ci sont fixés pour être perpendiculaires à la rotation de la jante, tel que montré à la figure~\ref{fig:S2-8-roue-holonome}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-8-roue-holonome.jpg}
	\caption{Exemple de roue holonome~\cite{holonomic}}
	\label{fig:S2-8-roue-holonome}
\end{figure}

Avec des rouleaux fixés de cette manière, la roue peut tourner à plein régime mais peut également glisser latéralement avec peu de friction. Les systèmes d'entraînement holonomes utilisent typiquement trois roues dans une configuration triangulaire, tel que présenté à la figure~\ref{fig:S2-8-roue-holonome-directions} ci-dessous. Cette configuration est appelée \textquote{Kiwi Drive}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-8-roue-holonome-directions.jpg}
	\caption{Mouvements possibles avec des roues holonomes en configuration \textquote{Kiwi Drive}}
	\label{fig:S2-8-roue-holonome-directions}
\end{figure}

Les mouvements permis par le \textquote{Kiwi Drive} sont identiques à ceux du système d'entraînement mecanum. En effet, en commandant les roues à des vitesses bien calculées, il est possible de déplacer le robot dans n'importe quelle direction et avec une rotation de vitesse quelconque.

Par contre, les roues holonomes en configuration de \textquote{Kiwi Drive} permettent d'économiser de l'espace comparativement au système mecanum, permettant ainsi de concevoir un robot plus compact.


\paragraph{Entraînement différentiel}
% FIXME : I was here
Le mouvement d'un système d'entraînement différentiel est basé sur la différence de vitesse entre deux roues de format standard disposées en parallèle. Un robot à entraînement différentiel est similaire au différentiel utilisés dans les automobiles, car il est alors possible d'avoir des roues pouvant tourner à des vitesses de rotation indépendantes. Contrairement au système d'engrenages différentiel, chaque roue est contrôlée par son propre moteur, dans un robot à entraînement différentiel.

Ce type d'entraînement est largement utilisé en robotique, car il permet des mouvements faciles à programmer et peut être bien contrôlé. La majorité des robots mobiles sur le marché utilisent la direction différentielle principalement pour son faible coût et sa simplicité.

Il est possible de changer la direction d'un robot à entraînement différentiel en faisant varier la vitesse relative des roues. Les roues sont très simples et sont constitués de pneus en caoutchouc montés sur des jantes en métal ou plastique. Le pneu en caoutchouc n'est pas à négliger, car c'est ce qui permet d'avoir une certaine adhérence entre le système d'entraînement et le surface sur laquelle se déplace le robot. Pour garantir un meilleur équilibre, des roues pivotantes peuvent être ajoutées. La figure~\ref{fig:S2-8-roues-diffrentielles-movements} représente un système d'entraînement différentiel de base avec deux roues motrices et une roue pivotante.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-8-roue-differentielles-movements.png}
	\caption{Exemple de système d'entraînement différentiel~\cite{differential-drive}}
	\label{fig:S2-8-roues-diffrentielles-movements}
\end{figure}

Afin que le robot puisse se déplacer sur une ligne droite, les deux roues motrices doivent tourner dans la même direction et à la même vitesse. Il est possible d'effectuer une rotation sur soi-même en commandant les roues à des vitesses égales et opposées. Si les deux roues sont commandées à deux vitesses différentes dans la même direction, le robot tracera un arc, tel que montre la figure~\ref{fig:S2-8-roues-diffrentielles-arc}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-8-roues-differentielles-arc.png}
	\caption{Système d'entraînement différentiel qui trace un arc~\cite{differential-steering}}
	\label{fig:S2-8-roues-diffrentielles-arc}
\end{figure}

En faisant varier la vitesse de rotation des roues, il est possible de faire bouger le centre du robot le long d'une courbe quelconque. La direction du robot dépend uniquement de la vitesse et du sens de rotation des deux roues motrices. Ces deux vitesses doivent alors être captées et contrôlées avec précision afin de garantir un système efficace.

\subsubsection{Étude de faisabilité}

Avant de faire la sélection du système de roues pour l'entraînement avec une matrice de décision, une étude de faisabilité a été réalisée pour les 3 concepts présentés précédemment. Les options furent évaluées sur les conditions de faisabilité physique, efficacité, simplicité et faisabilité économique comme présenté au tableau~\ref{table:S2-8-entrainement-faisabilité} ci-dessous.

Tous les trois concepts ont été acceptés avec l'étude de faisabilité, mais le système à roues mecanum ainsi que le système à roues holonomes ont des restrictions concernant la simplicité. Cela est dû à la complexité de leur mise en œuvre, notamment en raison du nombre de degrés de liberté.

\begin{table}[H]
	\caption{Étude de la faisabilité des trois alternatives pour le système d'entraînement}
	\label{table:S2-8-entrainement-faisabilité}
	\begin{tabularx}{\textwidth}{Xllllll}
		\toprule
		\textbf{Alternative} & \textbf{Physique} & \textbf{Efficacité} & \textbf{Simplicité} & \textbf{Économique} & \textbf{Décision} \\
		\midrule
		Roues mecanum & Oui & Oui & Oui, mais & Oui & Acceptée \\
		Roues holonomes & Oui & Oui & Oui, mais & Oui & Acceptée \\
		\makecell[l]{Entraînement \\ différentiel} & Oui & Oui & Oui & Oui & Acceptée \\
		\bottomrule
	\end{tabularx}
\end{table}


\subsubsection{Décision finale du système d'entraînement} \label{S2-8-choix-systeme-entrainement}

La matrice de décision du tableau~\ref{table:S2-8-roues-matrice-de-decision} fut utilisée pour choisir le système de roues de l'entraînement en fonction du cahier des charges établi au tableau~\ref{table:S2-8-roues-cahier-des-charges}. Afin de faciliter le dressage de la matrice de décision, un numéro fut donné à chacune des alternatives de la section~\ref{S2-8-systeme-entrainement-etude-preliminaire}:

\begin{enumerate}
	\item Système d'entraînement mecanum
	\item Système d'entraînement holonome (\textquote{Kiwi Drive})
	\item Système d'entraînement différentiel
\end{enumerate}

Parmi les trois concepts, le système d'entraînement différentiel fut choisi avec le meilleur score à \SI{95}{\percent}.

\begin{table}[H]
	\caption{Matrice de décision utilisée pour choisir le système de roues}
	\label{table:S2-8-roues-matrice-de-decision}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xccccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pond.} & \textbf{Barème} & \textbf{1} & \textbf{2} & \textbf{3} \\

		\midrule

		\rowgroup{Performance} & $\SI{40}{\percent}$ \\

		\midrule

		Polyvalence & $\SI{35}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & \SI{0}{\percent} & \SI{0}{\percent} & \SI{100}{\percent} \\

		\midrule

		Multidirectionnalité & $\SI{5}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & \SI{100}{\percent} & \SI{100}{\percent} & \SI{0}{\percent} \\

		\midrule

		\rowgroup{Efficacité} & $\SI{40}{\percent}$ \\

		\midrule

		Simplicité & $\SI{25}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & \SI{50}{\percent} & \SI{50}{\percent} & \SI{100}{\percent} \\

		\midrule

		Contrôle & $\SI{15}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & \SI{0}{\percent} & \SI{50}{\percent} & \SI{100}{\percent} \\

		\midrule

		\rowgroup{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{50}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{100}{\$}: \SI{80}{\percent}$ \\
			$\leq \SI{200}{\$}: \SI{60}{\percent}$ \\
			$> \SI{200}{\$}: \SI{0}{\percent}$ \\
		} & \SI{60}{\percent} & \SI{80}{\percent} & \SI{100}{\percent} \\

		\toprule

			\rowgroup{Total} & - & - & \SI{29.5}{\percent} & \SI{41}{\percent} & \SI{95}{\percent} \\

		\bottomrule
	\end{tabularx}
\end{table}


\subsection{Choix des moteurs d'entraînement}

\subsubsection{Cahier des charges}

Les roues choisies à la section~\ref{S2-8-choix-systeme-entrainement} ne seront pas utiles pour déplacer le robot, sans des moteurs pour les actionner. Les moteurs utilisés devraient être rapides, puissants et faciles à commander. De plus, leurs dimensions et leur poids devraient être raisonnables pour des robots mobiles participant à \textit{Eurobot 2020}. Finalement, vu que le temps et le budget du GRUM sont limités, il est très important que les moteurs requièrent un minimum d'entretien.

Le tableau~\ref{table:S2-8-moteurs-cahier-des-charges} ci-dessous représente le cahier des charges du choix des moteurs d'entraînement, en se basant sur les critères mentionnés plus haut.

\begin{table}[H]
	\caption{Cahier des charges utilisé pour le choix des moteurs d'entraînement}
	\label{table:S2-8-moteurs-cahier-des-charges}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xcccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pondération} & \textbf{Barème} & \textbf{Min} & \textbf{Max} \\

		\midrule

		\rowgroup{Performance} & $\SI{40}{\percent}$ \\

		\midrule

		Opération à vitesse élevée & $\SI{25}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Commande électronique & $\SI{15}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Efficacité} & $\SI{40}{\percent}$ \\

		\midrule

		Puissance / Poids & $\SI{30}{\percent}$ & \makecell{
			Élevé $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Bas $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		Entretien & $\SI{10}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & - & - \\

		\midrule

		\rowgroup{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{50}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{100}{\$}: \SI{80}{\percent}$ \\
			$\leq \SI{200}{\$}: \SI{60}{\percent}$ \\
			$> \SI{200}{\$}: \SI{0}{\percent}$ \\
		} & - & - \\

		\bottomrule
	\end{tabularx}
\end{table}

\subsubsection{Étude préliminaire} \label{S2-8-moteurs-etude-preliminaire}

\paragraph{Moteurs C.C. avec balais}

Le moteur électrique à courant continu avec balais est un moteur électrique à commutation interne, fonctionnant à partir d'une source d'alimentation à courant continu. La rotation de l'arbre du moteur à courant continu avec balais est obtenue en faisant traverser un courant électrique dans une  bobine enroulée autour d'un noyau en fer doux ce qui crée une force ascendante sur le côté du pôle positif et crée une force descendante sur l'autre côté. Ces forces créent un effet de rotation sur la bobine qui fait tourner l'arbre du moteur comme montré à la figure~\ref{fig:S2-8-moteur-cc} ci-dessous.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{figures/S2-sous-systèmes/S2-8-moteur-cc-a.png}
	\hfill
	\includegraphics[width=0.3\textwidth]{figures/S2-sous-systèmes/S2-8-moteur-cc-b.png}
	\hfill
	\includegraphics[width=0.3\textwidth]{figures/S2-sous-systèmes/S2-8-moteur-cc-c.png}
	\caption{Rotation de l'arbre d'un moteur à courant continu avec balais~\cite{brushed-1}~\cite{brushed-2}~\cite{brushed-3}}
	\label{fig:S2-8-moteur-cc}
\end{figure}

Il est possible de faire varier la vitesse du moteur en faisant varier la tension de fonctionnement ou la force du champ magnétique. Pour fournir une vitesse constante ou une vitesse inversement proportionnelle à la charge mécanique du moteur, les valeurs de vitesse et de couple du moteur peuvent être modifiées selon les connexions du champ à l'alimentation électrique.

Il est à garder en tête que les balais de ces moteurs se dégradent avec le temps et doivent être remplacés périodiquement afin d'assurer un bon fonctionnement du moteur.


\paragraph{Moteurs C.C. sans balais}

Un moteur sans balais est un moteur électrique qui fait partie de la catégorie des moteurs synchrones, dont le rotor comprend un ou plusieurs aimants permanents et les bobines qui transportent le courant sont fixées sur le stator. Contrairement aux moteurs avec balais, ces moteurs n'ont pas de collecteur tournant et donc aucun balai. Par contre, les moteurs sans balais ont besoin d'un système électronique de commande afin de créer une commutation du courant dans les enroulements du stator. Dans la plupart des cas, le moteur sans balais est contrôlé par un onduleur comme montré à la figure~\ref{fig:S2-8-moteur-bldc-controller} ci-dessous.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-8-moteur-bldc-controller.jpg}
	\caption{Moteur C.C. sans balais avec système de contrôle~\cite{bldc}}
	\label{fig:S2-8-moteur-bldc-controller}
\end{figure}

Pour que le moteur sans balais puisse tourner, l'onduleur envoie un signal carré aux bobines. Lorsqu'un courant est transmis aux bobines, le champ magnétique résultant attire ou repousse l'aimant permanent, soit le rotor dans le cas actuel. En ajustant la grandeur et la direction du courant dans ces bobines, il est possible de contrôler la direction et la vitesse de rotation des rotors. La figure~\ref{fig:S2-8-moteur-bldc-bobines} ci-dessous démontre le fonctionnement des bobines et aimants d'un moteur sans balais.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-8-moteur-bldc-bobines.png}
	\caption{Fonctionnement interne d'un moteur C.C. sans balais~\cite{bldc-bobines}}
	\label{fig:S2-8-moteur-bldc-bobines}
\end{figure}

Les moteurs sans balais ont un rendement puissance-poids élevé, comparativement aux moteur avec balais et peuvent opérer à une vitesse plus élevée. Vu que les moteurs sans balais n'ont pas de balais à changer, ils requièrent moins d'entretien.

Finalement, choisir des moteurs sans balais permettrait non seulement de réutiliser des moteurs utilisés par le GRUM l'année passée, mais il permettrait aussi de réutiliser la même carte de commande que l'année passée, à savoir le \textit{ODrive}. En effet, le groupe possède suffisamment de moteurs et de contrôleurs pour intégrer aux deux robots, tout en ayant quelques pièces de rechange.

Le moteur utilisé l'an dernier est le \techterm{Aerodrive SK3 350KV} de \textit{TURNIGY}~\cite{Aerodrive}. Un aperçu du moteur est donné à la figure~\ref{fig:S2-8-aerodrive}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-8-aerodrive.jpg}
	\caption{\techterm{Aerodrive SK3 350KV} de \textit{TURNIGY}~\cite{Aerodrive}}
	\label{fig:S2-8-aerodrive}
\end{figure}

Un aperçu de la carte de commande des moteurs utilisée l'an dernier, l'\textit{ODrive}, est donné à la figure~\ref{fig:S2-8-odrive}~\cite{ODrive}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-8-odrive.jpg}
	\caption{Contrôleur de moteurs C.C. sans balais \textit{ODrive}~\cite{ODrive}}
	\label{fig:S2-8-odrive}
\end{figure}


\paragraph{Moteur pas à pas}

Un moteur pas à pas est un moteur électrique à courant continu sans balais, où une rotation complète est également divisée en étapes, appelées \techterm{pas}. Le moteur pas à pas contient de multiples électroaimants disposés autour d'une pièce de fer centrale en forme d'engrenage qui est reliée à l'arbre. La disposition circulaire des électroaimants est divisée en groupes appelés \techterm{phases}, où chaque phase a un nombre égal d'électroaimants. Les électroaimants de chaque phase sont entrelacés avec les électroaimants des autres phases afin de créer un motif uniforme. Comme montré à la figure~\ref{fig:S2-8-moteur-stepper}, les phases de ce moteur sont en ordre : phase 1, phase 2, phase 3 et phase 4. L'ordre des phases est donc : 12341234.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/S2-sous-systèmes/S2-8-moteur-stepper.png}
	\caption{Fonctionnement interne d'un moteur pas à pas~\cite{stepper}}
	\label{fig:S2-8-moteur-stepper}
\end{figure}

Les électroaimants de la même phase sont alimentés ensemble, ce qui implique que les moteurs pas à pas ayant plus de phases ont généralement plus de câbles pour contrôler le moteur, vu que chaque phase a son propre fil d'alimentation. Les électroaimants sont alimentés par un circuit de commande externe ou un microcontrôleur qui envoie une série d'impulsions carrées.

Pour faire tourner l'arbre du moteur, un courant est transmis dans la première phase qui attire magnétiquement les dents de l'engrenage vers le premier électroaimant. Lorsque les dents de l'engrenage sont alignées avec l'électroaimant de la première phase, elles sont légèrement décalées par rapport à l'électroaimant suivant. Cela signifie que lorsque le prochain électroaimant est alimenté et qu'il n'y a plus de courant dans le premier, l'engrenage tourne légèrement pour s'aligner avec le suivant. Au fur et à mesure, il est possible d'obtenir des rotations / étapes (pas). Après un nombre entier d'étapes, le moteur fait une rotation complète. Cette caractéristique du moteur pas à pas permet à celui-ci d'être tourné avec un angle précis et la position du moteur peut alors être commandée pour se déplacer et se maintenir à l'une de ces étapes.


\subsubsection{Étude de faisabilité}

Avant de faire la sélection du système de moteurs pour l'entraînement avec une matrice de décision, une étude de faisabilité a été réalisée pour les 3 concepts présentés dans la section précédente. Les options furent évaluées sur les criteres de faisabilité physique, efficacité, simplicité et faisabilité économique, comme présenté au tableau ci-dessous.

Tous les trois concepts ont été acceptés avec l'étude de faisabilité, mais le moteur pas à pas a des restrictions sur l'efficacité, vu le contrôle de ce genre de moteur n'est pas vraiment efficace.

\begin{table}[H]
	\caption{Étude de la faisabilité des trois alternatives pour les moteurs d'entraînement}
	\label{table:S2-8-moteurs-entrainement-faisabilité}
	\begin{tabularx}{\textwidth}{Xllllll}
		\toprule
		\textbf{Alternative} & \textbf{Physique} & \textbf{Efficacité} & \textbf{Simplicité} & \textbf{Économique} & \textbf{Décision} \\
		\midrule
		\makecell[l]{Moteurs C.C. \\ avec balais} & Oui & Oui & Oui & Oui & Acceptée \\
		\makecell[l]{Moteurs C.C. \\ sans balais} & Oui & Oui & Oui & Oui & Acceptée \\
		Moteurs pas à pas & Oui & Oui, mais & Oui & Oui & Acceptée \\
		\bottomrule
	\end{tabularx}
\end{table}


\subsubsection{Décision finale de moteurs d'entraînement} \label{S2-8-choix-moteurs}

La matrice de décision du tableau~\ref{table:S2-8-moteurs-cahier-des-charges} fut utilisée pour choisir les moteurs d'entraînement en fonction du cahier des charges établi au tableau~\ref{table:S2-8-moteurs-matrice-de-decision}. Afin de faciliter le dressage de la matrice de décision, un numéro fut donné à chacun des concepts de la section~\ref{S2-8-moteurs-etude-preliminaire}.

\begin{enumerate}
	\item Moteur C.C. avec balais
	\item Moteur C.C. sans balais
	\item Moteur pas à pas
\end{enumerate}

Parmi les trois concepts, les moteurs C.C. sans balais fut choisi comme moteurs d'entraînement, avec le meilleur score, à \SI{92}{\percent}. Le \textit{ODrive} est ainsi choisi comme contrôleur de moteurs.

\begin{table}[H]
	\caption{Matrice de décision utilisée pour choisir les moteurs d'entraînement}
	\label{table:S2-8-moteurs-matrice-de-decision}
	\centering
	\begin{tabularx}{\textwidth}{>{\quad}Xccccc}
		\toprule
		\rowgroup{Critère} & \textbf{Pond.} & \textbf{Barème} & \textbf{1} & \textbf{2} & \textbf{3} \\

		\midrule

		\rowgroup{Performance} & $\SI{40}{\percent}$ \\

		\midrule

		Opération à vitesse élevée & $\SI{25}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & \SI{0}{\percent} & \SI{100}{\percent} & \SI{0}{\percent} \\

		\midrule

		Commande électronique & $\SI{15}{\percent}$ & \makecell{
			Oui: $\SI{100}{\percent}$ \\
			Non: $\SI{0}{\percent}$ \\
		} & \SI{0}{\percent} & \SI{100}{\percent} & \SI{100}{\percent} \\

		\midrule

		\rowgroup{Efficacité} & $\SI{40}{\percent}$ \\

		\midrule

		Puissance / Poids & $\SI{30}{\percent}$ & \makecell{
			Élevé $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Bas $\SI{0}{\percent}$ \\
		} & \SI{50}{\percent} & \SI{100}{\percent} & \SI{0}{\percent} \\

		\midrule

		Entretien & $\SI{10}{\percent}$ & \makecell{
			Simple: $\SI{100}{\percent}$ \\
			Moyen: $\SI{50}{\percent}$ \\
			Complexe: $\SI{0}{\percent}$ \\
		} & \SI{0}{\percent} & \SI{100}{\percent} & \SI{50}{\percent} \\

		\midrule

		\rowgroup{Prix} & $\SI{20}{\percent}$ & \makecell{
			$\leq \SI{50}{\$}: \SI{100}{\percent}$ \\
			$\leq \SI{100}{\$}: \SI{80}{\percent}$ \\
			$\leq \SI{200}{\$}: \SI{60}{\percent}$ \\
			$> \SI{200}{\$}: \SI{0}{\percent}$ \\
		} & \SI{100}{\percent} & \SI{60}{\percent} & \SI{100}{\percent} \\

		\toprule

		\rowgroup{Total} & - & - & \SI{35}{\percent} & \SI{92}{\percent} & \SI{40}{\percent} \\

		\bottomrule
	\end{tabularx}
\end{table}

\input{sections/S2-Sous-systèmes/S2-8-entrainement/choix-asservissement.tex}

\subsection{Échéancier}

Le diagramme de Gantt de la figure~\ref{table:S2-8-gantt} représente les échéances des tâches du sous-système responsable de l'entraînement. Il est possible de voir que toutes les tâches sont achevées.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-8-echeances.png}
	\caption{Diagramme de Gantt représentant l'échéance des tâches pour l'entraînement}
	\label{table:S2-8-gantt}
\end{figure}

\subsection{Travail accompli}

\subsubsection{Conception mécanique}

La conception mécanique du système d'entraînement fut accomplie par des membres du GRUM en génie mécanique. Ces étudiantes et étudiants ont travaillé avec des logiciels de conception en trois dimensions, notamment \textit{Solid Edge}, afin de créer le système d'entraînement présenté à la figure~\ref{table:S2-8-travail-systeme-dentrainement}. L'équipe PFE était en communication continuelle avec l'équipe mécanique afin de s'assurer que le concept final convienne aux besoins électriques et permette une bonne gestion des câbles.

La conception finale utilise un système d'entraînement différentiel, comme décidé à la section~\ref{S2-8-choix-systeme-entrainement}. De plus, des moteurs à courant continu sans balais sont utilisés, comme décidé à la section~\ref{S2-8-choix-moteurs}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{figures/S2-sous-systèmes/S2-8-travail-systeme-dentrainement.png}
	\caption{Aperçu de la conception mécanique du système d'entraînement}
	\label{table:S2-8-travail-systeme-dentrainement}
\end{figure}

\input{sections/S2-Sous-systèmes/S2-8-entrainement/travail-asservissement.tex}
