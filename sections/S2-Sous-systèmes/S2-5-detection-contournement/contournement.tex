% vim: ft=tex spelllang=fr

\subsubsection{Algorithme de contournement}

Prime abord, les algorithmes de recherche de chemin sur des espaces continus furent recherchés. Plusieurs articles ont été étudiés, notamment \textquote{\textit{Robot Path Planning with Avoiding Obstacles in Known Environment Using Free Segments and Turning Points Algorithm}}~\cite{Hassani2018}. L'article propose un algorithme qui tente de déterminer le chemin le plus court et le plus sécuritaire dans le contexte d'un robot mobile à entraînement différentiel. L'algorithme est intéressant et semble correspondre aux besoins. Pourtant, les chemins générés par cet algorithme ne sont pas optimaux.

Il fut ainsi décidé de développer un algorithme à partir de rien, en s'inspirant de l'article de Hassani, Maalej et Rekik~\cite{Hassani2018}. L'algorithme proposé a deux étapes: il faut d'abord établir une liste de chemins qui ne passent pas par un obstacle, puis il faut trouver le chemin le plus court parmi la liste.

L'organigramme de la figure~\ref{fig:S2-5-contournement-algorithme-chemins-debloques} montre l'algorithme proposé pour trouver les chemins non bloqués. Initialement, la liste de chemins potentiels contient un seul chemin: une ligne droite de l'origine à la destination. Si le chemin ne passe pas par un obstacle, la recherche est finie. Sinon, les chemins qui passent au-dessus et au-dessous de l'obstacle sont considérés et sont ajoutés à la liste de chemins potentiels. L'algorithme exécute le test encore une fois sur les nouveaux chemins et itère de cette façon jusqu'au moment où la liste ne contient que des chemins sans obstacles.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[node distance=2cm]
		\node (start) [startstop] {Début};

		\node (initial) [process, below of=start, yshift=-0.5cm, text width=5cm] {Chemin initial: ligne droite entre origine et destination (séquence de deux points)};
		\draw [arrow] (start) -- (initial);

		\node (blocked) [decision, below of=initial, yshift=-3cm, text width=5cm] {Est-ce que le chemin passe par un obstacle?};
		\draw [arrow] (initial) -- (blocked);

		\node (dupe) [process, below of=blocked, yshift=-2.5cm] {Dupliquer le chemin};
		\draw [arrow] (blocked) -- node [anchor=east] {Oui} (dupe);

		\node (above) [process, below of=dupe, xshift=-3cm, text width=5cm] {Ajouter un point au chemin pour aller au dessus de l'obstacle};
		\draw [arrow] (dupe) -- (above);
		\draw [arrow] (above) -- ++(-3.5,0) -- ++(0,1) -- (blocked);

		\node (below) [process, below of=dupe, xshift=3cm, text width=5cm] {Ajouter un point au chemin pour aller en dessous l'obstacle};
		\draw [arrow] (dupe) -- (below);
		\draw [arrow] (below) -- ++(3.5,0) -- ++(0,1) -- (blocked);

		\node (end) [startstop, right of=blocked, xshift=4cm] {Fin};
		\draw [arrow] (blocked) -- node [anchor=south] {Non} (end);

	\end{tikzpicture}

	\caption{Organigramme d'algorithme proposé pour la recherche des chemins non bloqués}
	\label{fig:S2-5-contournement-algorithme-chemins-debloques}
\end{figure}

Une fois la liste établie, il suffit de suivre le plus court. Cela est déterminé assez facilement en faisant la somme des hypoténuses entre les points.

La duplication des chemins est accomplie avec un appel récursif à l'algorithme de recherche de chemin, comme le montre le pseudo-code de la figure~\ref{fig:S2-5-contournement-pseudocode}, une version simplifiée du code \textit{Python} complet. La fonction \code{developper_chemin} prend un chemin (défini par une liste de points) et une liste d'obstacles et retourne tout simplement le chemin non modifié si aucun obstacle bloque le chemin. Dans le cas contraire, la fonction retourne une liste de deux appels récursifs, un qui contourne le premier obstacle trouvé par le dessus et un qui le contourne par le dessous.

\begin{figure}[H]
	\pythonscript{./scripts/S2-5-contournement-pseudocode.py}
	\mintedspace
	\caption{Pseudo-code de l'algorithme de la figure~\ref{fig:S2-5-contournement-algorithme-chemins-debloques}}
	\label{fig:S2-5-contournement-pseudocode}
\end{figure}

En exécutant la fonction \code{developper_chemin} avec le chemin direct entre l'origine et la destination, une liste de chemins libres est obtenue. Un exemple du résultat de cette fonction entre les points $(1000, 200)$ et $(1000, 2800)$ avec deux obstacles circulaires est présenté à la figure~\ref{fig:S2-5-contournement-avec-deux-cercles}. Il est à noter que le système de coordonnées interne des robots ainsi que celui utilisé pour l'interface graphique de la figure~\ref{fig:S2-5-contournement-avec-deux-cercles} est identique à celui des règles (annexe~\ref{appendix:regles} figure~16). Ainsi, l'axe des x est vers le bas et l'axe des y est vers le droite.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-5-contournement-avec-deux-cercles.png}
	\caption{Exemple de la sortie de l'algorithme de recherche de chemins}
	\label{fig:S2-5-contournement-avec-deux-cercles}
\end{figure}

Il est possible de voir que chaque chemin généré contourne chaque obstacle par le haut ou par le bas. Puisqu'il y a deux obstacles, quatre chemins sont générés.

L'algorithme fut développé pour supporter des obstacles circulaires uniquement. Un cercle de grandeur égal au rayon maximal du robot peut être utilisé pour représenter les robots adverses, peu importe leur orientation. De plus, le cercles peuvent être utilisés pour approximer les éléments de jeu, tels que les taquets de protection des ports (voir annexe~\ref{appendix:regles} section~J.1.f). L'utilisation d'une seule forme pour représenter tous les obstacles permet de réduire le nombre de cas à traiter par l'algorithme. De plus, les cercles sont des formes très simples et facilitent les calculs géométriques effectués par l'algorithme.

Le chemin le plus court peut être déterminé en calculant la longueur totale de chacun. Cette valeur correspond tout simplement à la somme des hypoténuses entre les points du chemin et se calcule grâce au théorème de Pythagore. Le chemin libre de longueur minimale correspond au chemin optimal et est celui que le robot devra suivre. Le chemin le plus court (en rouge) parmi tous les cas considérés (en noir) entre le point $(1200, 200)$ et $(1200, 2800)$ est présenté à l'exemple de la figure~\ref{fig:S2-5-contournement-shortest}.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-5-contournement-shortest.png}
	\caption{Détermination du chemin le plus court}
	\label{fig:S2-5-contournement-shortest}
\end{figure}

Lorsque l'algorithme rencontre un objet, il insère un point dans le chemin pour le contourner. Ce point correspond à l'intersection des tangentes à l'obstacle qui passent par le point précèdent et le point suivant. N'utiliser qu'un seul point pour contourner un obstacle permet de réduire le nombre d'arrêts et de rotations du robot. Par contre, lorsque l'angle entre les tangentes est petit, leur intersection est très éloignée de l'obstacle, ce qui implique que le robot devra se déplacer inutilement.

Puisque le système d'entraînement est capable d'exécuter des mouvements en arcs, il est aussi possible de faire suivre au robot la circonférence de l'obstacle. Par contre, pour ce faire, le robot doit s'arrêter deux fois: au début et à la fin de l'arc.

Un compromis doit donc être fait entre le nombre d'arrêts et la distance à parcourir. Sans connaître la vitesse et l'accélération du robot, il est impossible de développer un algorithme qui est optimal dans tous les cas. Il a été arbitrairement choisi de suivre la circonférence si l'angle entre les tangentes est aigu.

La figure~\ref{fig:S2-5-contournement-arcs} montre un exemple de tels arcs lorsque le chemin contient des angles aigus. Le chemin original sans optimisation est montré avec des traces pointillées alors que le chemin avec des mouvements en forme d'arcs est montré avec traces solides.

\begin{figure}[H]
	\includegraphics[width=\textwidth]{figures/S2-sous-systèmes/S2-5-contournement-arcs.png}
	\caption{Détermination du chemin le plus court}
	\label{fig:S2-5-contournement-arcs}
\end{figure}
