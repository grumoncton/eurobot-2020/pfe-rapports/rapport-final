class Comms:

    temps_dexpiration = 1

    def envoyer_instruction(
            self,
            instruction,
            format,
            donnees,
            **kwargs,
    ):
        # Construire et envoyer la trame dans une méthode privée
        self._envoyer_instruction(
            instruction=instruction,
            format=format,
            donnees=donnees,
            **kwargs,
        )

        try:
            # Essayer de lire la réponse avec un temps d'expiration
            with Expiration(self.temps_dexpiration):
                instruction, donnees = self._lire_reponse(**kwargs)

            # Aussi créer une erreur si NACK reçue
            if instruction == Instruction.NACK:
                raise NAckError()

        # Renvoyer si une erreur s'est produit
        except (TimeoutError, NAckError):
            return self._envoyer_instruction(
                instruction=instruction,
                format=format,
                donnees=donnees,
                **kwargs,
            )

        return instruction, donnees

    def _send_instruction(
            self,
            instruction,
            format,
            donnees,
            **kwargs,
    ):

        # Calculer la somme de contrôle
        crc = crc16((instruction, *donnees))

        # Convertir la somme de contrôle, l'instruction et les
        # données en séquence d'octets
        trame = struct.pack(
            '<HB' + format,
            (crc, instruction, *donnees),
        )

        # Envoyer la trame
        self._envoyer_trame(trame=trame, **kwargs)

    def _lire_reponse(self, **kwargs):

        # Lire la trame
        trame = self._lire_trame(**kwargs)

        # Extraire CRC et instruction
        crc, instruction = struct.unpack('<HB', trame[:3])
        # Extraire données
        donnees = trame[3:]

        # Renvoyer si les sommes de contrôles ne correspondent pas
        if crc != crc16((instruction, *args)):
            return self.send_instruction(
                instruction=Instruction.NACK,
                format='',
                donnees=tuple(),
                **kwargs,
            )

        return instruction, donnees

    def _envoyer_trame(self, trame, **kwargs):
        raise NotImplementedError()

    def _lire_trame(self, **kwargs):
        raise NotImplementedError()
