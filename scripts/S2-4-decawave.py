import struct
import logging
from enum import IntEnum


logger = logging.getLogger(__name__)


class UnknownCommandBrokenFrameError(OSError):
    pass


class InternalError(OSError):
    pass


class InvalidParameterError(OSError):
    pass


class BusyError(OSError):
    pass


class OperationNotPermittedError(OSError):
    pass


class Decawave:
    class _Instruction(IntEnum):
        POS_SET = 0x01
        POS_GET = 0x02
        UPD_RATE_SET = 0x03
        UPD_RATE_GET = 0x04
        CFG_TAG_SET = 0x05
        CFG_ANCHOR_SET = 0x07
        CFG_GET = 0x08
        SLEEP = 0x0A
        RESET = 0x14

    def __init__(self, port, baud_rate=115200):
        from serial import Serial

        self._ser = Serial(port, baud_rate)

    @staticmethod
    def _raise_error(for_code):
        raise {
            1: UnknownCommandBrokenFrameError,
            2: InternalError,
            3: InvalidParameterError,
            4: BusyError,
            5: OperationNotPermittedError,
        }[for_code](errno=for_code)

    def _read_int(self):
        return int.from_bytes(self._ser.read(1), byteorder='little')

    def _tlv_exchange(self, request):

        self._ser.write(request)

        # Jeter "type" et "length"
        self._ser.read(2)

        err_code = self._read_int()
        if err_code:
            self._raise_error(for_code=err_code)

    def _tlv_read(self, instruction, format_string):
        self._tlv_exchange([instruction, 0])

        # Jeter "type"
        self._ser.read(1)

        length = self._read_int()
        frame = self._ser.read(length)

        return struct.unpack('<' + format_string, frame)

    def _tlv_write(self, instruction, format_string, args):
        size = struct.calcsize(format_string) - 2
        frame = struct.pack('<BB' + format_string, [
            instruction,
            size,
            *args,
        ])

        self._tlv_exchange(frame)

    def pos_set(self, x, y, z, quality_factor):
        self._tlv_write(
            instruction=self._Instruction.POS_SET,
            args=(x, y, z, quality_factor),
            format_string='IIIB',
        )

    def pos_get(self):
        return self._tlv_read(
            instruction=self._Instruction.POS_GET,
            format_string='IIIB',
        )

    def upd_rate_set(
            self,
            normal_update_rate,
            stationary_update_rate,
    ):
        self._tlv_write(
            instruction=self._Instruction.UPD_RATE_SET,
            args=(normal_update_rate, stationary_update_rate),
            format_string='HH',
        )

    def upd_rate_get(self):
        return self._tlv_read(
            instruction=self._Instruction.UPD_RATE_GET,
            format_string='HH',
        )

    def cfg_tag_set(
        self,
        uwb_mode,
        fw_update_en,
        ble_en,
        led_en,
        loc_engine_en,
        low_power_en,
        meas_mode,
        stnry_en,
    ):
        value = uwb_mode | (fw_update_en << 2) | (ble_en << 3) | \
            (led_en << 4) | (loc_engine_en << 6) | \
            (low_power_en << 7) | (meas_mode << 8) | \
            (stnry_en << 10)

        self._tlv_write(
            instruction=self._Instruction.CFG_TAG_SET,
            args=(value,),
            format_string='H',
        )

        self.reset()

    def cfg_tag_get(self):
        value = self._tlv_read(
            instruction=self._Instruction.CFG_GET,
            format_string='H',
        )

        return {
            'uwb_mode': value[0] & 0b11,
            'fw_update_en': (value[0] >> 2) & 1,
            'ble_en': (value[0] >> 3) & 1,
            'led_en': (value[0] >> 4) & 1,
            'loc_engine_en': (value[0] >> 5) & 1,
            'low_power_en': (value[0] >> 6) & 1,
            'meas_mode': value[1] & 0b11,
            'accel_en': (value[1] >> 2) & 1,
            'bridge': (value[1] >> 3) & 1,
            'initiator': (value[1] >> 4) & 1,
            'mode': (value[1] >> 5) & 1,
        }

    def cfg_anchor_set(
            self,
            uwb_mode,
            fw_update_en,
            ble_en,
            led_en,
            enc_en,
            bridge,
            initiator,
    ):
        value = uwb_mode | (fw_update_en << 2) | (ble_en << 3) | \
            (led_en << 4) | (bridge << 6) | (initiator << 7)

        self._tlv_write(
            instruction=self._Instruction.CFG_ANCHOR_SET,
            args=(value,),
            format_string='H',
        )
        self.reset()

    def sleep(self):
        self._tlv_write(
            instruction=self._Instruction.SLEEP,
            args=tuple(),
            format_string='',
        )

    def reset(self):
        self._tlv_write(
            instruction=self._Instruction.RESET,
            args=tuple(),
            format_string='',
        )
