class AsyncComms(Comms):
    def attendre_instruction(self, **kwargs):

        instruction, donnees = self._lire_reponse(**kwargs)

        return instruction, donnees

    def _envoyer_trame(self, trame, **kwargs):
        raise NotImplementedError()

    def _lire_trame(self, **kwargs):
        raise NotImplementedError()
